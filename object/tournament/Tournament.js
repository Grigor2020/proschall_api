var querys = require('../../model/querys/querys');
var tournamentQuerys = require('../../model/querys/v1/tournament');


function Tournament(user_id,tournamentId) {
    this.tournamentId = tournamentId;

    this.user_id = user_id;
    this.name = null;
    this.game_id = null;
    this.game_mode_id = null;
    this.members_count = null;
    this.entry = null;
    this.prize = null;
    this.start_date = null;
    this.start_time = null;
    this.check_in_start_date = null;
    this.check_in_start_time = null;

    this.userMoney = null;

    this.result = {};

    var self = 1111;

    // this.selectLeagues = function () {
    //     return new Promise(function (resolve, reject) {
    //         leaguesQuery.checkDefineLeague(
    //             {sportmonks_id:self.leagueId},
    //             ['id','sportmonks_id','name','logo_path','s_country_id']
    //         )
    //             .then(function (data) {
    //                 if(data.length === 0){
    //                     self.getLeagueFromApi(self.leagueId)
    //                         .then(function (data) {
    //                             resolve(data)
    //                         })
    //                         .catch(function (error) {
    //                             resolve({error:'4534534534534',errMsg:error.toString()})
    //                         })
    //                 }else{
    //                     let oGetCountry = new GetCountry(data[0].s_country_id);
    //
    //                     oGetCountry.selectCountry()
    //                         .then(function (selectCountryData) {
    //                             self.result.countryInfo = selectCountryData.countryInfo;
    //                             self.result.leagueInfo = data[0];
    //                             resolve(self.result)
    //                         })
    //                         .catch(function (error) {
    //                             resolve({error:'7532453435434',errMsg:error.toString()})
    //                         })
    //                 }
    //             })
    //             .catch(function (error) {
    //                 resolve({error:'8926598',errMsg:error.toString()})
    //             })
    //     })
    // };
    this.getAllTournaments = function () {
        return new Promise(function (resolve, reject) {
            tournamentQuerys.getAllTournaments()
                .then(function (data){
                    self.result = data;
                    resolve(self.result)
                })
                .catch(function (error) {
                    resolve({error:'8996519864132',errMsg:error.toString()})
                })
        });
    };

    this.joinMember = function () {
        return new Promise(function (resolve, reject) {
            tournamentQuerys.getAllTournaments()
                .then(function (data){
                    self.result = data;
                    resolve(self.result)
                })
                .catch(function (error) {
                    resolve({error:'8996519864132',errMsg:error.toString()})
                })
        });
    };

    this.getUserMoney = function () {
        querys.findByMultyNamePromise('user_money',{user_id : this.user_id},['money'])
            .then(function (money) {
                // console.log('money[0].money',money[0].money)
                self.userMoney = money[0].money
            })
            .catch(function (error) {
                return {error:'8949665456415656',errMsg:error.toString()}
            })
    };
}


module.exports = Tournament;
