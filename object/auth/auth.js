var statics = require('../../static');
var querys = require('../../model/querys/querys');
var log = require("../../logs/log");


const checkValidUser = function (req, res, next) {
    return function(req, res, next) {
        if(typeof req.headers.void !== "undefined"){
            querys.findByMultyNamePromise('users_db',{token:req.headers.void},['id'])
                .then(function (data) {
                    // console.log('data',data)
                    req.userId = data[0].id;
                    next();
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error),"./logs/query2v.txt")
                    res.json({error:true,msg:'9999'})
                })
        }else{
            log.insertLog('req.headers.void === undefined',"./logs/query2v.txt")
            res.json({error:true,msg:'9999'});
            res.end();
        }

    }
}


const checkUserOrGuest = function (req, res, next) {
    return function(req, res, next) {
        if(typeof req.headers.void !== "undefined"){
            querys.findByMultyNamePromise('users_db',{token:req.headers.void},['id'])
                .then(function (data) {
                    // console.log('data',data)
                    req.userId = data[0].id;
                    next();
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error),"./logs/query2v.txt")
                    res.json({error:true,msg:'9999'})
                })
        }else{
            req.userId = null;
            next();
        }

    }
}




module.exports.checkValidUser = checkValidUser;
module.exports.checkUserOrGuest = checkUserOrGuest;


/**
 * [
 *   {
 *     "Email": "sadoyangrigor.dev@gmail.com",
 *     "MessageUUID": "013bc74f-b287-40d2-829e-5bd36d32eea4",
 *     "MessageID": "288230396710090243",
 *     "MessageHref": "https://api.mailjet.com/v3/REST/message/288230396710090243"
 *   }
 * ]
 */