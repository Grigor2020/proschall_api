var statics = require('../static')


function getWinnerPrize(list) {
    for(let i = 0; i < list.length; i++){
        list[i].winner_priz = (2 * parseFloat(list[i].prize)) - ((2 * parseFloat(list[i].prize))*statics.SITE_PERCENT/100);
    }
    return list;
}
function convertTZ(date, tzString) {
    return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));
}

module.exports.getWinnerPrize = getWinnerPrize;
module.exports.convertTZ = convertTZ;