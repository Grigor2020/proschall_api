const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var findChallengeProps = function(langId,gameId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `games`.`url`,`games`.`image`,`games`.`id`,' +
            '`games_language`.`lang_id`,`games_language`.`name` ' +
            'FROM `games` ' +
            'JOIN `games_language` ON `games_language`.`lang_id` = '+db.escape(langId)+' AND `games_language`.`games_id` = `games`.`id`';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findChallengeProps",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error findChallengeProps: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findOneChallenge = function(challengeId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `challenge`.`id` as challenge_id,`challenge`.`prize`,`challenge`.`description`, ' +
            '`game_mode`.`name` as mode_name,`users_db`.`username`,`users_db`.`image` as user_img, ' +
            '`game_usernames`.`name` as username_for_game,`consoles`.`image` as consoles_image,' +
            '`games`.`url` as game_url,`games`.`image` as game_img ' +
            'FROM `challenge` ' +
            'JOIN `game_mode` ON `game_mode`.`id` = `challenge`.`mode_id` ' +
            'JOIN `games` ON `games`.`id` = `challenge`.`game_id` ' +
            'JOIN `users_db` ON `users_db`.`id` = `challenge`.`user_id` ' +
            'JOIN `game_usernames` ON `game_usernames`.`user_id` = `challenge`.`user_id` AND `game_usernames`.`game_id` = `challenge`.`game_id`  ' +
            'JOIN `consoles` ON `consoles`.`id` = `challenge`.`console_id` ' +
            'WHERE `challenge`.`id` = '+db.escape(challengeId);
        // console.log("prepareSql",prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findChallengeProps",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error findOneChallenge: "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < result.length; i++){
                    var imgName = result[i].consoles_image;
                    result[i].console_image = static.API_URL+'/images/consoles/'+imgName;
                    delete result[i].consoles_image;
                }
                resolve(result)
            }
        })
    })
}


var getChallengeList = function(game_id,prize,consolesId,page,userId){
    // prize
    // 1 => max to min
    // 2 => min to max

    var userQuery = "";
    if(userId !== null){
        userQuery = "AND `challenge`.`user_id` <> '"+userId+"'";
    }
    var startInt,limitInt = 4;
    if(page == '1'){
        startInt = 0
    }else{
        startInt = (parseInt(page) - 1) * limitInt
    }

    let where = "WHERE `challenge`.`game_id` = "+db.escape(game_id)+" AND `challenge`.`started` = '0' "+ userQuery;
    if(consolesId !== '0'){
        where = "WHERE `challenge`.`game_id` = "+db.escape(game_id)+" AND `challenge`.`started` = '0' AND `challenge`.`console_id` = "+ db.escape(consolesId) +" "+ userQuery;
    }

    let orderBy = "ORDER BY `challenge`.`id` DESC";
    if(prize == '1'){
        orderBy = "ORDER BY `challenge`.`prize` DESC";
    }else if(prize == '2'){
        orderBy = "ORDER BY `challenge`.`prize` ASC";
    }

    // console.log('limitInt',limitInt)
    // console.log('startInt',startInt)
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `challenge`.`id` as challenge_id,`challenge`.`prize`, ' +
            '`game_mode`.`name` as mode_name,`users_db`.`username`,`users_db`.`image` as user_img, ' +
            '`game_usernames`.`name` as username_for_game,`consoles`.`image` as consoles_image,' +
            '`games`.`image` as game_img ' +
            'FROM `challenge` ' +
            'JOIN `game_mode` ON `game_mode`.`id` = `challenge`.`mode_id` ' +
            'JOIN `users_db` ON `users_db`.`id` = `challenge`.`user_id` ' +
            'JOIN `games` ON `games`.`id` = `challenge`.`game_id` ' +
            ' JOIN `game_usernames` ON `game_usernames`.`user_id` = `challenge`.`user_id`  AND `game_usernames`.`game_id` = `challenge`.`game_id` ' +
            'JOIN `consoles` ON `consoles`.`id` = `challenge`.`console_id` ' +
            where+
            orderBy+' LIMIT '+limitInt +' offset  '+startInt+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error getChallengeList: "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < result.length; i++){
                    var imgName = result[i].consoles_image;
                    result[i].console_image = static.API_URL+'/images/consoles/'+imgName;
                    delete result[i].consoles_image;
                }
                resolve(result)
            }
        })
    })
}
var getAllChallengeList = function(page,prize,consolesId,userId){
    // prize
    // 1 => max to min
    // 2 => min to max

    var userQuery = "";
    if(userId !== null){
        userQuery = " AND `challenge`.`user_id` <> '"+userId+"' ";
    }
    var startInt,limitInt = 4;
    if(page == '1'){
        startInt = 0
    }else{
        startInt = (parseInt(page) - 1) * limitInt
    }

    let where =  " WHERE `challenge`.`started` = '0' "+ userQuery;
    if(consolesId !== '0'){
        where = " WHERE `challenge`.`started` = '0' AND `challenge`.`console_id` = "+ db.escape(consolesId) +" "+ userQuery;
    }

    let orderBy = " ORDER BY `challenge`.`id` DESC ";
    if(prize == '1'){
        orderBy = " ORDER BY `challenge`.`prize` DESC ";
    }else if(prize == '2'){
        orderBy = " ORDER BY `challenge`.`prize` ASC ";
    }
    // console.log('limitInt',limitInt)
    // console.log('startInt',startInt)
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `challenge`.`id` as challenge_id,`challenge`.`prize`, ' +
            '`game_mode`.`name` as mode_name,`users_db`.`username`,`users_db`.`image` as user_img, ' +
            '`game_usernames`.`name` as username_for_game,`consoles`.`image` as consoles_image,' +
            '`games`.`image` as game_img ' +
            'FROM `challenge` ' +
            'JOIN `game_mode` ON `game_mode`.`id` = `challenge`.`mode_id` ' +
            'JOIN `users_db` ON `users_db`.`id` = `challenge`.`user_id` ' +
            'JOIN `games` ON `games`.`id` = `challenge`.`game_id` ' +
            ' JOIN `game_usernames` ON `game_usernames`.`user_id` = `challenge`.`user_id`  AND `game_usernames`.`game_id` = `challenge`.`game_id` ' +
            'JOIN `consoles` ON `consoles`.`id` = `challenge`.`console_id` ' +
            where+
            orderBy+' LIMIT '+limitInt +' offset  '+startInt+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllChallengeList",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error getAllChallengeList: "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < result.length; i++){
                    var imgName = result[i].consoles_image;
                    result[i].console_image = static.API_URL+'/images/consoles/'+imgName;
                    delete result[i].consoles_image;
                }
                resolve(result)
            }
        })
    })
}

var getGameConsoles = function(game_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT DISTINCT  `challenge`.`console_id`, `consoles`.`image` AS consoles_image,`consoles`.`name` ' +
            'FROM `challenge` ' +
            'JOIN `consoles` ON `consoles`.`id` = `challenge`.`console_id` ' +
            'WHERE `challenge`.`game_id` = '+db.escape(game_id);
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error getChallengeList: "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < result.length; i++){
                    var imgName = result[i].consoles_image;
                    result[i].console_image = static.API_URL+'/images/consoles/'+imgName;
                    delete result[i].consoles_image;
                }
                resolve(result)
            }
        })
    })
}


var getAllGameConsoles = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT DISTINCT  `challenge`.`console_id`, `consoles`.`image` AS consoles_image,`consoles`.`name` ' +
            'FROM `challenge` ' +
            'JOIN `consoles` ON `consoles`.`id` = `challenge`.`console_id` ';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllGameConsoles",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error getAllGameConsoles: "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < result.length; i++){
                    var imgName = result[i].consoles_image;
                    result[i].console_image = static.API_URL+'/images/consoles/'+imgName;
                    delete result[i].consoles_image;
                }
                resolve(result)
            }
        })
    })
}
var getGameMinAndMaxPrices = function(game_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT MIN(`challenge`.`prize`) AS min_prize,MAX(`challenge`.`prize`) AS max_prize ' +
            'FROM `challenge` ' +
            'WHERE `challenge`.`game_id` = '+db.escape(game_id);
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error getChallengeList: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getAllGamesMinAndMaxPrices = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT MIN(`challenge`.`prize`) AS min_prize,MAX(`challenge`.`prize`) AS max_prize ' +
            'FROM `challenge` ';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllGamesMinAndMaxPrices",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error getAllGamesMinAndMaxPrices: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getMyChallenges = function(user_id,langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `challenge`.`prize`,`challenge`.`id` as challenge_id,`challenge`.`game_id`,`challenge`.`console_id`,' +
            '`consoles`.`image` as console_image,`consoles`.`name` as console_name,' +
            '`game_mode`.`name` as mode_name, ' +
            '`games`.`image` as game_image, ' +
            '`game_usernames`.`name` as username_for_game, ' +
            '`users_db`.`username`,`users_db`.`image` as user_img, ' +
            '`games_language`.`name` as game_name ' +
            'FROM `challenge` ' +
            'JOIN `users_db` ON `users_db`.`id` = '+db.escape(user_id)+' ' +
            'JOIN `consoles` ON `consoles`.`id` = `challenge`.`console_id` ' +
            'JOIN `game_mode` ON `game_mode`.`id` = `challenge`.`mode_id` ' +
            'JOIN `games` ON `games`.`id` = `challenge`.`game_id` ' +
            'JOIN `games_language` ON `games_language`.`games_id` = `challenge`.`game_id` AND `games_language`.`lang_id` = '+db.escape(langId)+' ' +
            'JOIN `game_usernames` ON `game_usernames`.`user_id` = `challenge`.`user_id`  AND `game_usernames`.`game_id` = `challenge`.`game_id` ' +
            'WHERE `challenge`.`user_id` = '+db.escape(user_id)+' AND `challenge`.`started` = "0" ORDER BY `challenge`.`id` DESC';


        // console.log('asdasd',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getMyChallenges",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error getMyChallenges: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getMyOngoingChallenges = function(user_id,langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `matches`.`challenge_id`' +
            'FROM `matches` ' +
            'WHERE (`matches`.`creator_id` = '+db.escape(user_id)+' OR  `matches`.`joiner_id` = '+db.escape(user_id)+') ' +
            'AND  `matches`.`creator_start` = "1" AND `matches`.`joiner_start` = "1" ' +
            'AND `matches`.`winner_id` IS NULL';


        // console.log('asdasd',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getMyOngoingChallenges",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/challenge.txt")
                reject(new Error("Sql error getMyOngoingChallenges: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.getChallengeList = getChallengeList;
module.exports.getAllChallengeList = getAllChallengeList;
module.exports.getAllGameConsoles = getAllGameConsoles;
module.exports.findChallengeProps = findChallengeProps;
module.exports.findOneChallenge = findOneChallenge;
module.exports.getGameConsoles = getGameConsoles;
module.exports.getGameMinAndMaxPrices = getGameMinAndMaxPrices;
module.exports.getAllGamesMinAndMaxPrices = getAllGamesMinAndMaxPrices;
module.exports.getMyChallenges = getMyChallenges;
module.exports.getMyOngoingChallenges = getMyOngoingChallenges;
