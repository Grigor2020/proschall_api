const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var findAllConsolesByGameId = function(gameId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `consoles`.`name`,`consoles`.`image`,`consoles`.`id` ' +
            'FROM `game_consoles` ' +
            'JOIN `consoles` ON `consoles`.`id` = `game_consoles`.`console_id` ' +
            'WHERE `game_consoles`.`game_id` = '+db.escape(gameId);
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllConsolesByGameId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/console.txt")
                reject(new Error("Sql error findAllConsolesByGameId: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.findAllConsolesByGameId = findAllConsolesByGameId;
