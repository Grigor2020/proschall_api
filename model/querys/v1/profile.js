const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var findUserGameUsernames = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `game_usernames`.`name`,`game_usernames`.`game_id` ' +
            'FROM `game_usernames` ' +
            'WHERE `game_usernames`.`user_id` = '+db.escape(user_id);
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserGameUsernames",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error findUserGameUsernames: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findUserGameResults = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT ' +
            ' `matches`.`winner_id`' +
            ' FROM `matches` ' +
            ' WHERE `matches`.`creator_id` = '+db.escape(user_id)+' OR `matches`.`joiner_id` = '+db.escape(user_id);
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserGameUsernames",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error findUserGameUsernames: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findUserTransactions = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `transactions_history`.* ' +
            'FROM `transactions_history` ' +
            'WHERE `transactions_history`.`user_id` = '+db.escape(user_id)+
            ' ORDER BY `transactions_history`.`id` DESC';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserTransactions",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error findUserTransactions: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getUserMoney = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `transactions_history`.* ' +
            'FROM `transactions_history` ' +
            'WHERE `transactions_history`.`user_id` = '+db.escape(user_id)+
            ' ORDER BY `transactions_history`.`id` DESC';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserTransactions",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error findUserTransactions: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.findUserGameUsernames = findUserGameUsernames;
module.exports.findUserTransactions = findUserTransactions;
module.exports.findUserGameResults = findUserGameResults;
