const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');
const {matchStatuses} = require("../../../constants");

var checkUsersBusy = function(challengeId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT  ' +
            'GROUP_CONCAT(DISTINCT `matches_users`.`user_id` SEPARATOR "||") AS matches_user_id,  ' +
            'GROUP_CONCAT(DISTINCT `matches_users`.`user_accept` SEPARATOR "||") AS matches_user_accept  ' +
            'FROM `challenge` ' +
            'JOIN `matches` ON `matches`.`challenge_id` = `challenge`.`id` ' +
            'JOIN `matches_users` ON `matches_users`.`matches_id` = `matches`.`id` ' +
            'WHERE `challenge`.`id` = '+db.escape(challengeId) +' AND `challenge`.`started` = "0"';

        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllServersByGameId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error checkUsersBusy: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var checkBusyByJoinerId = function(joinerId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `matches`.`id` ' +
            'FROM `matches` ' +
            'WHERE `matches`.`joiner_id` = '+db.escape(joinerId) +' AND `matches`.`creator_accept` = "1" AND `matches`.`joiner_accept` = "1" AND `matches`.`winner_id` IS NULL';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllServersByGameId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error checkUsersBusy: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}


var checkBusyByCreatorId = function(creatorId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `matches`.`id` ' +
            'FROM `matches` ' +
            'WHERE `matches`.`creator_id` = '+db.escape(creatorId) +' AND `matches`.`creator_accept` = "1" AND `matches`.`joiner_accept` = "1"   AND `matches`.`winner_id` IS NULL';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllServersByGameId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error checkUsersBusy: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findByMatchId = function(MatchId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT * ' +
            'FROM `matches` ' +
            'WHERE `matches`.`id` = '+MatchId;
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByMatchId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error checkUsersBusy: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var findUserHistory = function(userId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `matches`.`id`,`matches`.`create_date`,`matches`.`start_time`,`matches`.`winner_id`,' +
            ' `users_db`.`username`,`users_db`.`image` as user_img,' +
            ' `game_usernames`.`name` as username_for_game, ' +
            ' `challenge`.`prize`,`challenge`.`id` as challenge_id, ' +
            ' `game_mode`.`name` as mode_name, ' +
            ' `consoles`.`image` as console_image, ' +
            ' `games`.`image` as game_img ' +
            ' FROM `matches` ' +
            ' JOIN `challenge` ON `challenge`.`id` = `matches`.`challenge_id` ' +
            ' JOIN `games` ON `games`.`id` = `challenge`.`game_id` ' +
            ' JOIN `game_mode` ON `game_mode`.`id` = `challenge`.`mode_id` ' +
            ' JOIN `consoles` ON `consoles`.`id` = `challenge`.`console_id` ' +
            ' JOIN `users_db` ON `users_db`.`id` = '+db.escape(userId)+' ' +
            ' JOIN `game_usernames` ON `game_usernames`.`user_id` = '+db.escape(userId)+' AND `game_usernames`.`game_id` = `challenge`.`game_id` ' +
            ' WHERE `matches`.`creator_id` = '+db.escape(userId)+' OR `matches`.`joiner_id` = '+db.escape(userId);
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserHistory",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error findUserHistory: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var findMatchWithProperties = function(MatchId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `matches`.*,`challenge`.`prize` ' +
            'FROM `matches` ' +
            'JOIN `challenge` ON `challenge`.`id` = `matches`.`challenge_id` ' +
            'WHERE `matches`.`id` = '+MatchId;
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByMatchId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error checkUsersBusy: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var getOponentInfo = function(userId,MatchId,gameId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `matches`.`id`,`matches`.`create_date`,`matches`.`winner_id`,`matches`.`start_date`,' +
            ' `matches`.`creator_id`,`matches`.`joiner_id`,`matches`.`creator_accept`,`matches`.`joiner_accept`,`matches`.`creator_start`,`matches`.`joiner_start`,' +
            '`users_db`.`username`,`users_db`.`image` as user_img,' +
            '`game_usernames`.`name` as username_for_game, ' +
            '`challenge`.`prize`,`challenge`.`id` as challenge_id,`challenge`.`description`, ' +
            '`game_mode`.`name` as mode_name, ' +
            '`consoles`.`image` as console_image, ' +
            '`games`.`image` as game_img,`games`.`url` as game_url ' +
            'FROM `matches` ' +
            'JOIN `games` ON `games`.`id` = '+db.escape(gameId)+' ' +
            'JOIN `challenge` ON `challenge`.`id` = `matches`.`challenge_id` ' +
            'JOIN `game_mode` ON `game_mode`.`id` = `challenge`.`mode_id` ' +
            'JOIN `consoles` ON `consoles`.`id` = `challenge`.`console_id` ' +
            'JOIN `users_db` ON `users_db`.`id` = '+db.escape(userId)+' ' +
            'JOIN `game_usernames` ON `game_usernames`.`user_id` = '+db.escape(userId)+' AND `game_usernames`.`game_id` = '+db.escape(gameId)+' ' +
            'WHERE `matches`.`id` = '+db.escape(MatchId);
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getOponentInfo",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error getOponentInfo: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getOponentInfoTimeing = function(MatchId, timeInterval){
    // console.log('.............MatchId',MatchId)
    // console.log('.............timeInterval',timeInterval)
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT DATE_ADD(`matches`.`start_time`,interval '+timeInterval+' minute) as start_time' +
            ' FROM `matches` ' +
            ' WHERE `matches`.`id` = '+db.escape(MatchId);
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getOponentInfoTimeing",
                        date:new Date()
                    }
                }
                console.log('result',result)
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error getOponentInfoTimeing: "+error.sqlMessage+""))
            }else{
                console.log('result',result)
                resolve(result)
            }
        })
    })
}

var setAcceptFromCreator = function(matchesId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'UPDATE `matches` ' +
            'SET `creator_accept`="1", `status`='+matchStatuses.accepted+' ' +
            'WHERE id='+db.escape(matchesId)+' ';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"setAcceptFromCreator",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error setViewedNotification: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var updateStart = function(updateField,matchesId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'UPDATE `matches` ' +
            'SET `'+updateField+'`="1" ' +
            'WHERE id='+matchesId+' ';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"updateStart",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error setViewedNotification: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var updateStartForTimer = function(matchesId){
    return new Promise(function (resolve, reject) {
        let time = new Date(new Date().toLocaleString("en-US", { timeZone: 'Europe/London' })).getTime()
        var prepareSql = '' +
            'UPDATE `matches` ' +
            'SET `start_date`= CURRENT_TIMESTAMP() ' +
            'WHERE id='+matchesId+' ';
        console.log('QWQQQQQQQQWQWQWQWQWWQWQWQWWQprepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"updateStart",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error setViewedNotification: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findJoinerByChallengeId = function(userId,challengeId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `matches`.`id` ' +
            'FROM `matches` ' +
            'WHERE `matches`.`challenge_id` = '+db.escape(challengeId)+" AND `matches`.`joiner_id` = "+db.escape(userId)+"";
        // console.log("prepareSql",prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findJoinerByChallengeId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/match.txt")
                reject(new Error("Sql error findJoinerByChallengeId: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.checkUsersBusy = checkUsersBusy;
module.exports.findUserHistory = findUserHistory;
module.exports.checkBusyByJoinerId = checkBusyByJoinerId;
module.exports.checkBusyByCreatorId = checkBusyByCreatorId;
module.exports.setAcceptFromCreator = setAcceptFromCreator;
module.exports.findByMatchId = findByMatchId;
module.exports.updateStart = updateStart;
module.exports.getOponentInfo = getOponentInfo;
module.exports.findMatchWithProperties = findMatchWithProperties;
module.exports.findJoinerByChallengeId = findJoinerByChallengeId;
module.exports.updateStartForTimer = updateStartForTimer;
module.exports.getOponentInfoTimeing = getOponentInfoTimeing;

