const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var findAllServersByGameId = function(gameId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `game_server`.`name`,`game_server`.`id` ' +
            'FROM `game_server` ' +
            'WHERE `game_server`.`game_id` = '+db.escape(gameId);
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllServersByGameId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/server.txt")
                reject(new Error("Sql error findAllServersByGameId: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.findAllServersByGameId = findAllServersByGameId;
