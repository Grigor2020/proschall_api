const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var addNotification = function(params){
    // console.log('params',params)
    return new Promise(function (resolve, reject) {
        // var insertParams = {
        //     user_id : typeof params.creatorId !== "undefined" ? params.creatorId : null,
        //     s_user_id :typeof params.joinerId !== "undefined" ? params.joinerId : null,
        //     type : params.type,
        //     view : "0",
        // }
        var prepareSql = 'INSERT INTO notifications SET ?';
        var query = db.query(prepareSql, params, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"addNotification",
                        table:"notifications",
                        params:params,
                        date:new Date()
                    }
                }
                // console.log('errorData',errorData)
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error addNotification"))
            }else{
                resolve(results)
            }
        });
    })
}


var findUserNotifications = function(user_id){
    // console.log('asdasd',user_id)
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `notifications`.`user_id`,`notifications`.`id` as notificationId,`notifications`.`money`, ' +
            '`notifications`.`s_user_id`,`notifications`.`view`,`notifications`.`type`,`notifications`.`challenge_id`, ' +
            '`users_db`.`username` as s_user_username,`matches`.`id` as matchesId  ' +
            'FROM `notifications` ' +
            'LEFT JOIN `users_db` ON `users_db`.`id` = `notifications`.`s_user_id` ' +
            'LEFT JOIN `matches` ON ' +
            ' `matches`.`challenge_id` = `notifications`.`challenge_id` AND  ' +
            ' (`matches`.`creator_id` = '+db.escape(user_id)+' OR ' +
            ' `matches`.`joiner_id` = '+db.escape(user_id)+') ' +
            ' WHERE `notifications`.`user_id` = '+db.escape(user_id)+' ORDER BY  `notifications`.`id` DESC';
        // console.log('prepareSql',prepareSql)

        // var prepareSql = '' +
        //     'SELECT `notifications`.`user_id`,`notifications`.`id` as notificationId, ' +
        //     '`notifications`.`s_user_id`,`notifications`.`view`,`notifications`.`type`,`notifications`.`challenge_id`, ' +
        //     '`users_db`.`username` as s_user_username ' +
        //     'FROM `notifications` ' +
        //     'JOIN `users_db` ON `users_db`.`id` = `notifications`.`s_user_id` ' +
        //     'WHERE `notifications`.`user_id` = '+db.escape(user_id)+' ';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserNotofications",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error findByUrl: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findUserUnreadCount = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT  COUNT(`notifications`.`id`) as counts ' +
            'FROM `notifications` ' +
            'WHERE `notifications`.`user_id` = '+db.escape(user_id)+' AND `notifications`.`view` = "0"';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserNotofications",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error findByUrl: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}


var setViewedNotification = function(notificationId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'UPDATE `notifications` ' +
            'SET view="1" ' +
            'WHERE id='+db.escape(notificationId)+' ';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"setViewedNotification",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error setViewedNotification: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var deleteNotification = function(params){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in params) {
            if (params[key] !== "NULL") {
                val.push("`" + key + "`=" + db.escape(params[key]));
            } else {
                val.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'DELETE FROM `notifications` WHERE ' + val.join(' AND ') + '';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            // console.log('error',error)
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deleteNoification",
                        params: params,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}


module.exports.addNotification = addNotification;
module.exports.findUserNotifications = findUserNotifications;
module.exports.setViewedNotification = setViewedNotification;
module.exports.findUserUnreadCount = findUserUnreadCount;
module.exports.deleteNotification = deleteNotification;
