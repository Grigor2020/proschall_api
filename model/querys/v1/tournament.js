const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var getAllTournaments = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `tournament`.`id` as tournamentId,`tournament`.`name` as tournamentName,`tournament`.`image` as tournamentImage, ' +
            ' `tournament`.`entry` as tournamentEntry,`tournament`.`prize_1` as tournamentPrize_1,`tournament`.`prize_2` as tournamentPrize_2, ' +
            ' `tournament`.`start_datetime` as tournamentStartDate, ' +
            ' `tournament`.`check_in_datetime` as tournamentCheckInStartDate, ' +
            ' `games_language`.`name` as gameName, ' +
            ' COUNT(`tournament_members`.`id`) as registeredUsersCount ' +
            ' FROM `tournament` ' +
            ' JOIN `games_language` ON `games_language`.`games_id` = `tournament`.`game_id` AND `games_language`.`lang_id` = "1" '+
            ' LEFT JOIN `tournament_members` ON `tournament_members`.`tournament_id` = `tournament`.`id` ' +
            '  WHERE  `tournament`.`start_datetime` > NOW() GROUP BY `tournament`.`id`,`games_language`.`name` ORDER BY  `tournament`.`start_datetime` ASC';
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllTournaments",
                        table:"tournament",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/tournament.txt")
                reject(new Error("Sql error getAllTournaments"))
            }else{
                // console.log('results',results)
                resolve(results)
            }
        });
    })
}
var getTournamentById = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `tournament`.`id` as tournamentId,`tournament`.`name` as tournamentName,`tournament`.`entry` as tournamentEntry, ' +
            ' `tournament`.`entry` as tournamentEntry,`tournament`.`prize_1` as tournamentPrize_1,`tournament`.`prize_2` as tournamentPrize_2, ' +
            ' `tournament`.`start_datetime` as tournamentStartDate, ' +
            ' `tournament`.`check_in_datetime` as tournamentCheckInStartDate, ' +
            ' `tournament`.`create_data` as tournamentCreateData, ' +
            ' `consoles`.`name` as consolesName, ' +
            ' `game_mode`.`name` as gameModeName, ' +
            ' `games_language`.`name` as gameName, ' +
            ' COUNT(`tournament_members`.`id`) as registeredUsersCount, ' +
            ' GROUP_CONCAT(DISTINCT `tournament_members`.`user_id`  ORDER BY `tournament_members`.`id` ASC SEPARATOR "||") AS users_ids '+
            ' FROM `tournament` ' +
            ' JOIN `consoles` ON `consoles`.`id` = `tournament`.`consoles_id` '+
            ' JOIN `game_mode` ON `game_mode`.`id` = `tournament`.`game_mode_id` '+
            ' JOIN `games_language` ON `games_language`.`games_id` = `tournament`.`game_id` AND `games_language`.`lang_id` = "1" '+
            ' LEFT JOIN `tournament_members` ON `tournament_members`.`tournament_id` = `tournament`.`id` ' +
            '  WHERE `tournament`.`id` = '+db.escape(id)+' GROUP BY `tournament`.`id`,`games_language`.`name`';
            // console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getTournamentById",
                        table:"tournament",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/tournament.txt")
                reject(new Error("Sql error getTournamentById"))
            }else{
                // console.log('results',results)
                resolve(results)
            }
        });
    })
}


var getTournamentRounds = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `tournament_round`.`id` as tournament_round_id,' +
            '`tournament_round`.`type` as tournament_round_type, ' +
            '`tournament_round`.`start_date` as tournament_round_start_date' +
            ' FROM `tournament_round` ' +
            '  WHERE `tournament_round`.`tournament_id` = '+db.escape(id);
            // console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getTournamentRounds",
                        table:"tournament",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/tournament.txt")
                reject(new Error("Sql error getTournamentRounds"))
            }else{
                // console.log('results',results)
                resolve(results)
            }
        });
    })
}
var getTournamentGroups = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `tournament_group`.`id` as tournament_group_id, `tournament_group`.`tournament_round_id` as tournament_round_id ' +
            ' FROM `tournament_group` ' +
            ' WHERE `tournament_group`.`tournament_id` = '+db.escape(id);
            // console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getTournamentGroups",
                        table:"tournament",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/tournament.txt")
                reject(new Error("Sql error getTournamentGroups"))
            }else{
                // console.log('results',results)
                resolve(results)
            }
        });
    })
}

var getTournamentGames = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `tournament_games`.`id` as tournament_games_id, `tournament_games`.`tournament_group_id` as tournament_group_id, ' +
            ' GROUP_CONCAT(DISTINCT `tournament_games_members`.`tournament_members_id`  ORDER BY `tournament_games_members`.`id` ASC SEPARATOR "||") AS members_id, '+
            ' GROUP_CONCAT(DISTINCT `tournament_games_members`.`winner` ORDER BY `tournament_games_members`.`id` ASC SEPARATOR "||") AS winner, '+
            ' GROUP_CONCAT(DISTINCT `tournament_members`.`user_id` ORDER BY `tournament_games_members`.`id` ASC SEPARATOR "||") AS user_id, '+
            ' GROUP_CONCAT(DISTINCT `users_db`.`username` ORDER BY `tournament_games_members`.`id` ASC SEPARATOR "||") AS usernames '+
            ' FROM `tournament_games` ' +
            ' JOIN `tournament_games_members` ON `tournament_games_members`.`tournament_games_id` =  `tournament_games`.`id`' +
            ' JOIN `tournament_members` ON `tournament_members`.`id` =  `tournament_games_members`.`tournament_members_id`' +
            ' JOIN `users_db` ON `users_db`.`id` =  `tournament_members`.`user_id`' +
            ' WHERE `tournament_games`.`tournament_id` = '+db.escape(id)+' GROUP BY `tournament_games`.`id`';
            // console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getTournamentGames",
                        table:"tournament",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/tournament.txt")
                reject(new Error("Sql error getTournamentGames"))
            }else{
                // console.log('results',results)
                resolve(results)
            }
        });
    })
}

module.exports.getAllTournaments = getAllTournaments;
module.exports.getTournamentById = getTournamentById;
module.exports.getTournamentRounds = getTournamentRounds;
module.exports.getTournamentGroups = getTournamentGroups;
module.exports.getTournamentGames = getTournamentGames;




var resultIng = {
    quarterFinals : {
        round_id : 789,
        groups :  [
            {
                id : 13,
                group_id : 55,
                games : [
                    {
                        game_id : 41,
                        members_id : '7||2',
                        winner_id : null
                    },
                    {
                        game_id : 44,
                        members_id : '5||8',
                        winner_id : null
                    }
                ]
            },
            {
                id : 13,
                group_id : 55,
                games : [
                    {
                        game_id : 41,
                        members_id : '7||2',
                        winner_id : null
                    },
                    {
                        game_id : 44,
                        members_id : '5||8',
                        winner_id : null
                    }
                ]
            }
        ]
    },
    semiFinals : [],
    finals : []
}