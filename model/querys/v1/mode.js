const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var findAllModesByGameId = function(gameId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `game_mode`.`name`,`game_mode`.`id` ' +
            'FROM `game_mode` ' +
            'WHERE `game_mode`.`game_id` = '+db.escape(gameId);
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllModesByGameId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/mode.txt")
                reject(new Error("Sql error findAllModesByGameId: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.findAllModesByGameId = findAllModesByGameId;
