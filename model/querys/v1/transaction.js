const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var addTransaction = function(params){
    console.log('params',params)
    return new Promise(function (resolve, reject) {
        // var insertParams = {
        //     user_id : typeof params.creatorId !== "undefined" ? params.creatorId : null,
        //     s_user_id :typeof params.joinerId !== "undefined" ? params.joinerId : null,
        //     type : params.type,
        //     view : "0",
        // }
        var prepareSql = 'INSERT INTO transactions_history SET ?';
        var query = db.query(prepareSql, params, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"addNotification",
                        table:"notifications",
                        params:params,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}

module.exports.addTransaction = addTransaction;
