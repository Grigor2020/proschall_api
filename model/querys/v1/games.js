const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var findAllGames = function(langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `games`.`url`,`games`.`image`,`games`.`id`,`games`.`show_server`,' +
            '`games_language`.`lang_id`,`games_language`.`name` ' +
            'FROM `games` ' +
            'JOIN `games_language` ON `games_language`.`lang_id` = '+db.escape(langId)+' AND `games_language`.`games_id` = `games`.`id`';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/games.txt")
                reject(new Error("Sql error findAllByFildsPromise: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var findByUrl = function(url){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `games`.`id` ' +
            'FROM `games` ' +
            'WHERE `games`.`url` = '+db.escape(url)+' ';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        date:new Date()
                    }
                }
                // console.log('errorData',errorData)
                log.insertLog(JSON.stringify(errorData),"./logs/v1/games.txt")
                reject(new Error("Sql error findByUrl: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.findAllGames = findAllGames;
module.exports.findByUrl = findByUrl;
