const db = require('../connection');
var log = require("../../logs/log");
const static = require('../../static');

var findAllByFilds = function(table,filds,cb){
    var queryFild = "";
    if(filds == "*"){
        queryFild = filds;
    }else{
        queryFild = filds.join(' , ')
    }
    var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+'';
    db.query(prepareSql, function (error, result, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findAllByFilds",
                    table:table,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    })
}



/**
 *
 * @param table
 * @param params {fild_name:value}
 * @param filds [fild1,fild2]
 * @param cb function
 */
var findByMultyName = function (table,params,filds,cb) {
    var whereParams = [];
    for(var key in params){
        if(params[key] !== "NULL") {
            whereParams.push("`" + key + "`=" + db.escape(params[key]));
        }else{
            whereParams.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';

    db.query(prepareSql, function (error, rows, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findByMultyName2v",
                    table:table,
                    params:params,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}

/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var insert2v = function(table,post,cb){
    var prepareSql = 'INSERT INTO '+table+' SET ?';
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var findAllByFildsPromise = function(table,filds){
    return new Promise(function (resolve, reject) {
        var queryFild = "";
        if(filds == "*"){
            queryFild = filds;
        }else{
            queryFild = filds.join(' , ')
        }
        var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:table,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query1v.txt")
                reject(new Error("Sql error findAllByFildsPromise: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var findAllByFildsOrderedPromise = function(table,filds,order){
    return new Promise(function (resolve, reject) {
        var queryFild = "";
        if(filds == "*"){
            queryFild = filds;
        }else{
            queryFild = filds.join(' , ')
        }
        var orderString = '';
        if(order !== null){
            orderString = ' ORDER BY '+order;
        }
        var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+''+orderString;
        // console.log(prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:table,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findAllByFildsPromise: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findByMultyNamePromise = function(table,params,filds){
    return new Promise(function (resolve, reject) {
        var whereParams = [];
        for(var key in params){
            if(params[key] !== "NULL") {
                whereParams.push("`" + key + "`=" + db.escape(params[key]));
            }else{
                whereParams.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByMultyNamePromise",
                        table:table,
                        params:params,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findByMultyNamePromise"))
            }else{
                // console.log('rows',rows)
                resolve(rows)
            }
        });
    })
}
var findUsersByIds = function(params){
    return new Promise(function (resolve, reject) {
        if(params.length === 0){
            resolve([])
        }else {
            var prepareSql = '' +
                ' SELECT `users_db`.`username`,`users_db`.`id` ' +
                ' FROM `users_db` ' +
                ' WHERE `users_db`.`id` IN(' + params.join(',') + ')';
            // console.log('prepareSql', prepareSql)
            db.query(prepareSql, function (error, rows, fields) {
                if (error) {
                    var errorData = {
                        code: error.code,
                        errno: error.errno,
                        sqlMessage: error.sqlMessage,
                        devData: {
                            queryFunction: "findByMultyNamePromise",
                            table: 'users_db',
                            params: params,
                            date: new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                    reject(new Error("Sql error findByMultyNamePromise"))
                } else {
                    // console.log('rows',rows)
                    resolve(rows)
                }
            });
        }
    })
}

var findTournamentMembersInfo = function(tournamentId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `tournament_members`.`id` as tournament_members_id' +
            // '`users_db`.`lastName`,`users_db`.`firstName` ' +
            ' FROM `tournament_members` ' +
            // ' JOIN `users_db` ON `users_db`.`id` = `tournament_members`.`user_id` ' +
            ' WHERE `tournament_members`.`tournament_id` = '+db.escape(tournamentId)+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findTournamentMembersInfo",
                        table:'users_db',
                        params:tournamentId,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findByMultyNamePromise"))
            }else{
                // console.log('rows',rows)
                resolve(rows)
            }
        });
    })
}


function insert2vPromise(table,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO '+table+' SET ?';
        // console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insert2vPromise",
                        table:table,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}

var insertLoopPromise = function(table,filds,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO ' + table + ' (' + filds.join(',') + ') VALUES  ?';
        var query = db.query(prepareSql, [post], function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insertLoopPromise",
                        table:table,
                        filds:filds,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error insertLoopPromise"))
            }else{
                resolve(results)
            }
        });
    })
}


function findByToken(token){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT  `users_db`.`id`,`users_db`.`email`,`users_db`.`firstName`,`users_db`.`lastName`, ' +
            '`users_db`.`username`,`users_db`.`image` as user_image,`users_db`.`token`,`users_db`.`bio`,`users_db`.`e_verify` as email_verify, ' +
            '`user_money`.`money` as userMoney ' +
            'FROM  `users_db` ' +
            'JOIN `user_money` ON `user_money`.`user_id` = `users_db`.`id` ' +
            'WHERE `users_db`.`token`=' + db.escape(token) + ' ' +
            'LIMIT 1';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByToken",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function checkUserMoney(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT  `user_money`.`money` ' +
            'FROM  `user_money` ' +
            'WHERE `user_money`.`user_id`=' + db.escape(user_id);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"checkUserMoney",
                        table:'users_money',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}


function findByGuestToken(token){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `guest` WHERE `token`=' + db.escape(token) + ' LIMIT 1';
        db.query(prepareSql, function (error, rows, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "findByGuestToken",
                        table: 'guest',
                        params: {token:token},
                        filds: "*",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/checkUser.txt")
                reject(new Error("Sql error findByGuestToken"))
            } else {
                resolve(rows)
            }
        })
    })
}

var update2vPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post.values) {
            val.push("`" + key + "`=" + db.escape(post.values[key]));
        }
        var whereVal = [];
        for (var key in post.where) {
            whereVal.push("`" + key + "`" + "=" + db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";
        // console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "update2vPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}

function updatePromise(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for(var key in post.values){
            val.push("`"+key +"`="+db.escape(post.values[key]));
        }
        var whereVal = [];
        for(var key in post.where){
            whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "updatePromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
                reject(new Error("Sql error updatePromise"));
            }else{
                resolve(results)
            }
        });
    })
}


function deleteMultyRowBy(table,params,cb) {
    var where = [];
    for(var i=0;i<params.length;i++){
        where.push("`"+params[i]['key'] +"`="+db.escape(params[i]['value']));
    }
    var prepareSql = "DELETE FROM  "+table+" WHERE "+where.join(" OR ")+" ";
    var query = db.query(prepareSql, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "deleteMultyRowBy",
                    table: table,
                    params: params,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
            cb({error:true})
        }else{
            cb({error:false})
        }
    });
}

var update2v = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }
    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "update2v",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var deletes = function(table,post,cb){
    var val = [];
    for(var key in post){
        if(post[key] !== "NULL") {
            val.push("`" + key + "`=" + db.escape(post[key]));
        }else{
            val.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'DELETE FROM ' + table + ' WHERE '+val.join(' AND ')+'';
    db.query(prepareSql,post,function(error,result,fields){
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "deletes",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    });
}

var deletesPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post) {
            if (post[key] !== "NULL") {
                val.push("`" + key + "`=" + db.escape(post[key]));
            } else {
                val.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'DELETE FROM ' + table + ' WHERE ' + val.join(' AND ') + '';

        db.query(prepareSql, post, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletesPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}


function findByEmail(email){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `users_db` WHERE `email`=' + db.escape(email);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByEmail",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}
function findByUsername(username){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `users_db` WHERE `username`=' + db.escape(username);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByEmail",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}
function findForLogin(params){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `users_db`.`token` ' +
            'FROM  `users_db` ' +
            'WHERE (`users_db`.`email`=' + db.escape(params.login)+' AND `users_db`.`password` = '+db.escape(params.password)+' ) ' +
            ' OR (`users_db`.`username`=' + db.escape(params.login)+' AND `users_db`.`password` = '+db.escape(params.password)+' )';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findForLogin",
                        table:'users_db',
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}
function checkUserForMsgShow(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `matches`.`id` ' +
            ' FROM  `matches` ' +
            ' WHERE (`matches`.`winner_id` IS NULL AND `matches`.`joiner_start` = "1" AND `matches`.`creator_start` = "1" AND `matches`.`creator_id` = "'+user_id+'") OR ' +
            ' (`matches`.`winner_id` IS NULL AND `matches`.`joiner_start` = "1" AND `matches`.`creator_start` = "1" AND `matches`.`joiner_id` = "'+user_id+'") ';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"checkUserForMsgShow",
                        table:'matches',
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}
function getUserMessages(user_id,matches_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `messages`.`msg`,`messages`.`view`,`messages`.`date_time`, IF(`messages`.`user_id` = '+user_id+', "1", "0") as self_msg ' +
            ' FROM  `messages` ' +
            ' WHERE `messages`.`matches_id` = '+matches_id+' ' +
            ' ORDER BY `messages`.`date_time` ASC';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserMessages",
                        table:'matches',
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function setUserMessagesViewed(user_id,matches_id){
    return new Promise(function (resolve, reject) {
        var prepareSql =
            'UPDATE `messages` ' +
            'SET view="1" ' +
            'WHERE `matches_id`='+(matches_id)+'  AND user_id <> '+user_id;
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserMessages",
                        table:'matches',
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function findByEmailAndCodeForForgotPassword(queryParams){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `forgot_password_key` WHERE `user_id`=' + db.escape(queryParams.user_id)+' AND `ver_code`='+db.escape(queryParams.code);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByEmailAndCodeForForgotPassword",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}
function findByCodeId(codeId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `user_id` FROM  `forgot_password_key` WHERE `id`=' + db.escape(codeId);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByCodeId",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}





function findByUser(userid){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id`,`email`,`f_name`,`l_name`,`m_name` FROM  `users_db` WHERE `id`=' + db.escape(userid);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByUser",
                        table:'users_db',
                        params:{userid:userid},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/findUser.txt");
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}


var findUserUsernames = function(user_id,game_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `game_usernames`.`id`,`game_usernames`.`name` ' +
            'FROM `games` ' +
            'JOIN `game_usernames` ON `game_usernames`.`game_id` = '+db.escape(game_id)+' AND `game_usernames`.`user_id` = '+db.escape(user_id);
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserUsernames",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./log/checkUser.txt")
                reject(new Error("Sql error findChallengeProps: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.findUserUsernames = findUserUsernames;
module.exports.findByUser = findByUser;


module.exports.getUserMessages = getUserMessages;
module.exports.setUserMessagesViewed = setUserMessagesViewed;
module.exports.checkUserForMsgShow = checkUserForMsgShow;
module.exports.findByEmail = findByEmail;
module.exports.findByUsername = findByUsername;
module.exports.findForLogin = findForLogin;
module.exports.findByEmailAndCodeForForgotPassword = findByEmailAndCodeForForgotPassword;
module.exports.findByCodeId = findByCodeId;

module.exports.findAllByFilds = findAllByFilds;
module.exports.findAllByFildsPromise = findAllByFildsPromise;
module.exports.findAllByFildsOrderedPromise = findAllByFildsOrderedPromise;
module.exports.findByMultyName = findByMultyName;
module.exports.findByMultyNamePromise = findByMultyNamePromise;
module.exports.insert2v = insert2v;
module.exports.update2v = update2v;
module.exports.updatePromise = updatePromise;
module.exports.update2vPromise = update2vPromise;
module.exports.deletes = deletes;
module.exports.deletesPromise = deletesPromise;
module.exports.insert2vPromise = insert2vPromise;
module.exports.insertLoopPromise = insertLoopPromise;
module.exports.findByToken = findByToken;
module.exports.findByGuestToken = findByGuestToken;
module.exports.checkUserMoney = checkUserMoney;
module.exports.findUsersByIds = findUsersByIds;
module.exports.findTournamentMembersInfo = findTournamentMembersInfo;
