let express = require('express');
let router = express.Router();
let log = require("../../logs/log");
var checkUser = require('../../object/auth/auth');
var profileQuerys = require('../../model/querys/v1/profile');
var tournamentQuerys = require('../../model/querys/v1/tournament');
var querys = require('../../model/querys/querys');
var statics = require('../../static')
var notificationQuerys = require('../../model/querys/v1/notification')
var sendmail = require('../../mailer/sendmail');



router.post('/all', function(req, res,next) {
    tournamentQuerys.getAllTournaments()
        .then(function (data) {



            // var startDate = new Date(data[0].tournamentStartDate);
            // var checkInDate = new Date(data[0].tournamentCheckInStartDate);
            // console.log('startDate',startDate)
            console.log('data',data)
            console.log('...........................................')

            // let startDateUtc =
            //     new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startDate.getHours(), startDate.getMinutes(), startDate.getSeconds());
            // let checkInDateUtc =
            //     new Date(checkInDate.getFullYear(), checkInDate.getMonth(), checkInDate.getDate(), checkInDate.getHours(), checkInDate.getMinutes(), checkInDate.getSeconds());
            // console.log('Given IST startDateUtc: ' + startDateUtc);
            // console.log('Given IST checkInDateUtc: ' + checkInDateUtc);
            // console.log('...........................................')
            // let clientStartTime =
            //     startDateUtc.toLocaleString("en-US", {
            //         timeZone: req.body.timezone
            //     });
            // let clientCheckInTime =
            //     checkInDateUtc.toLocaleString("en-US", {
            //         timeZone: req.body.timezone
            //     });
            // console.log('client start: ' + clientStartTime);
            // console.log('client check-in: ' + clientCheckInTime);


            res.json({error:false,data:data});
            res.end();
        })
        .catch(function (error) {
            log.insertLog(JSON.stringify(error) +", errorSelfCode:'85400'","./logs/v1/tournament.txt");
            res.json({error:true,msg:"85400"})
            res.end();
        })
});

router.post('/send-users-checkin-emails', function(req, res,next) {
    console.log('********************************************************/send-users-checkin-emails.')
    console.log('req.body.', req.body.tournament_id)
    let tournamentId = req.body.tournament_id;
    Promise.all([
        tournamentQuerys.getTournamentById(tournamentId),
        // querys.findByMultyNamePromise('tournament_members',{tournament_id : tournamentId},['user_id']),
    ])
        .then(function ([thisTournament]) {
            // console.log('thisTournament',thisTournament[0]);
            // console.log('thisTournament[0].users_ids',thisTournament[0].users_ids);
            var tournament = thisTournament[0];
            let thisTournamentMembers = tournament.users_ids === null ? [] : tournament.users_ids.split('||')
            console.log('/////////thisTournamentMembers////////',thisTournamentMembers)
            let allQuery = [
                createRoundsGroupsAndGames(tournamentId),
            ];
            thisTournamentMembers.forEach((member)=>{
                var params = {
                    user_id : '',
                    s_user_id : '',
                    type:'4',
                    challenge_id : '',
                    view : "0",
                }
                allQuery.push(notificationQuerys.addNotification(params))
            })

            Promise.all(allQuery)
                .then(([data])=>{

                })
                .catch((error)=>{

                })
        });
});

router.post('/send-tournament-stats-to-admin', function(req, res,next) {
    console.log('********************************************************/send-tournament-stats-to-admin.')

    console.log('req.body.', req.body.tournament_id)
    let tournamentId = req.body.tournament_id;
    querys.findByMultyNamePromise('tournament_members',{tournament_id : tournamentId},['user_id'])
        .then((data)=>{
            console.log('This is Count of members for sending email to admin, members are : '+data.length+'')
            // Stex Adminin email enq uxarkum te qani mard a masnakcum
            console.log('data.length',data.length)
            sendmail.sendMailAuth(tournamentId,data.length)
                .then((data)=>{
                    console.log('data',data)
                    res.json({error : false})
                    res.end();
                })
                .catch((error)=>{
                    console.log('error',error)
                    res.json({error : true})
                    res.end();
                })

        })
        .catch((error)=>{
            console.log('error',error)
            res.end();
        })
});



router.post('/user/join',checkUser.checkValidUser(), function(req, res) {
    let tournamentId = req.body.tournament_id;
    // console.log('tournament_id',tournamentId)
    Promise.all([
        querys.findByMultyNamePromise('user_money',{user_id : req.userId},['money']),
        querys.findByMultyNamePromise('tournament',{id : tournamentId},['members_count','entry','start_datetime','check_in_datetime']),
        querys.findByMultyNamePromise('tournament_members',{tournament_id : tournamentId},['user_id']),
        querys.findByMultyNamePromise('tournament_members',{tournament_id : tournamentId,user_id : req.userId},['user_id'])
    ])
        .then(function ([money,thisTournament,thisTournamentMembers,userFind]) {
            // console.log('thisTournamentMembers',thisTournamentMembers)
            // var Now = new Date();
            // var checkInStartDate = new Date(thisTournament[0].check_in_datetime);
            var Now = new Date("2020-12-31T19:30");
            var checkInStartDate = new Date("2020-12-31T22:30");

            // console.log('Now',Now)
            // console.log('checkInStartDate',checkInStartDate)
            // console.log('checkInStartDate - Now',checkInStartDate - Now)
            if(thisTournamentMembers.length < 8){
                if(checkInStartDate > Now){
                    // if((checkInStartDate - Now) > statics.TOURNAMENT_CHECK_IN_TILL){

                        let userMoney = money[0].money;
                        // console.log('userMoney',userMoney)
                        // userFind -> man enq gali te et usere kpac a et tournament-in te voch
                        if(userFind.length == 0){
                            let tournamentEntry = thisTournament[0].entry;
                            let membersLimit = thisTournament[0].members_count;
                            let membersCount = thisTournament.length;
                            // membersLimit > membersCount -> stugum enq tex ka grancvelu te che
                            if(membersLimit > membersCount){
                                // tournamentEntry > userMoney -> stugum enq usere etqan pox uni te che
                                if(userMoney > tournamentEntry){
                                    let userUewMoney = userMoney - tournamentEntry;
                                    let updatePost = {
                                        where : {user_id : req.userId},
                                        values : {money : userUewMoney}
                                    };
                                    Promise.all([
                                        querys.insert2vPromise('transactions_history',{
                                            user_id : req.userId,
                                            money : tournamentEntry,
                                            tournament_id : tournamentId,
                                            type : '1',
                                            way : '2'
                                        }),
                                        querys.update2vPromise('user_money',updatePost)
                                    ])
                                        .then(function ([transactionInsert,moneyUpdateResult]) {
                                            console.log('moneyUpdateResult',moneyUpdateResult)
                                            if(moneyUpdateResult.affectedRows == 1){
                                                let insertTournamentMemberPost = {
                                                    tournament_id : tournamentId,
                                                    user_id : req.userId
                                                }
                                                // let insertTournamentRoundPost = {
                                                //     tournament_id : tournamentId
                                                // }
                                                Promise.all([
                                                    // querys.insert2vPromise('tournament_round',insertTournamentMemberPost),
                                                    querys.insert2vPromise('tournament_members',insertTournamentMemberPost)
                                                ])
                                                    .then(function ([insertTournamentMemberData]) {
                                                        if((membersCount+1) === 8 ){
                                                            // createRoundsGroupsAndGames(tournamentId)
                                                            //     .then((data)=>{
                                                            //
                                                            //     })
                                                            //     .catch((error)=>{
                                                            //
                                                            //     })
                                                        }else {
                                                            res.json({error: false})
                                                        }
                                                    })
                                                    .catch(function (error) {
                                                        log.insertLog(JSON.stringify(error) +" ,errorSelfCode:'42004'","./logs/v1/tournament.txt");
                                                        res.json({error:true,msg:"42004"})
                                                        res.end();
                                                    })
                                            }else{
                                                log.insertLog("errorSelfCode:'42006'","./logs/v1/tournament.txt");
                                                res.json({error:true,msg:"42006"})
                                                res.end();
                                            }
                                        })
                                        .catch(function (error) {
                                            log.insertLog(JSON.stringify(error) +", errorSelfCode:'42003'","./logs/v1/tournament.txt");
                                            res.json({error:true,msg:"42003"})
                                            res.end();
                                        })
                                }else{
                                    log.insertLog("errorSelfCode:'42005'","./logs/v1/tournament.txt");
                                    res.json({error:true,msg:"42005"})
                                    res.end();
                                }
                            }else{
                                log.insertLog("errorSelfCode:'42002'","./logs/v1/tournament.txt");
                                res.json({error:true,msg:"42002"})
                                res.end();
                            }
                        }else {
                            log.insertLog("errorSelfCode:'42001'","./logs/v1/tournament.txt");
                            res.json({error:true,msg:"42001"})
                            res.end();
                        }
                    // }else{
                    //     res.json({error:true,msg:'2 jamic qich a nmacel.'})
                    // }
                }else{
                    res.json({error:true,msg:'ancel a tournament grancvelu jame'})
                }
            }else{
                // createRoundsGroupsAndGames(tournamentId)
                //     .then((data)=>{
                //
                //     })
                //     .catch((error)=>{
                //
                //     })
                res.json({error:true,msg:'arden 8 hogi grancvac a'})
            }

        })
        .catch(function (error) {
            // log.insertLog(JSON.stringify(error) +" ,errorSelfCode:'42007'","./logs/v1/tournament.txt");
            // res.json({error:true,msg:"42007"})
            // res.end();
        })
});

router.post('/get-one-tournament', function(req, res) {
    let tournamentId = req.body.tournament_id;
    Promise.all([
        tournamentQuerys.getTournamentById(tournamentId),
        // querys.findByMultyNamePromise('tournament_members',{tournament_id : tournamentId},['user_id']),
    ])
        .then(function ([thisTournament]) {
            console.log('thisTournament[0].tournamentCheckInStartDate',thisTournament[0].tournamentCheckInStartDate)
            console.log('thisTournament[0].tournamentStartDate',thisTournament[0].tournamentStartDate)
            const checkInDate = new Date(thisTournament[0].tournamentCheckInStartDate).toUTCString();
            const startDate = new Date(thisTournament[0].tournamentStartDate).toUTCString();
            console.log('checkInDate',checkInDate)
            console.log('startDate',startDate)

            const now = new Date().toUTCString();
            console.log('now',now)
            let actionType = null;
            if(now < checkInDate){
                // available for join
                actionType = 1;
            }else if(now > checkInDate && now < startDate){
                // check in time is over , users waiting for start
                actionType = 2;
            }else if(now > startDate){
                // tournament is started
                actionType = 3;
            }
            console.log('actionType',actionType)
            // console.log('thisTournament',thisTournament[0]);
            // console.log('thisTournament[0].users_ids',thisTournament[0].users_ids);
            var tournament = thisTournament[0];
            console.log('tournament',tournament)
            let thisTournamentMembers = tournament.users_ids === null ? [] : tournament.users_ids.split('||')
            console.log('thisTournamentMembers',thisTournamentMembers);
            // if(thisTournamentMembers.length >0 && thisTournamentMembers.length !== 8){
            //     querys.findUsersByIds(thisTournamentMembers)
            //         .then(function (datas) {
            //             console.log('datas',datas);
            //             res.json({error:false,data:{tournament : thisTournament[0],tournamentMembers : thisTournamentMembers, regUsers : datas , rounds : []}})
            //             res.end();
            //         })
            //         .catch(function (error) {
            //             log.insertLog(JSON.stringify(error) +" ,errorSelfCode:'42011'","./logs/v1/tournament.txt");
            //             res.json({error:true,msg:"42011"})
            //             res.end();
            //         })
            // }else if(thisTournamentMembers.length === 8 || thisTournamentMembers.length === 6 || thisTournamentMembers.length === 4){
                    var resultIng = {
                        quarterFinals : { round_id : null, groups : [] },
                        semiFinals : { round_id : null, groups : [] },
                        finals : { round_id : null, groups : [] }
                    }
            console.log('resultIng',resultIng)
                    Promise.all([
                        querys.findUsersByIds(thisTournamentMembers),
                        tournamentQuerys.getTournamentRounds(tournamentId),
                        tournamentQuerys.getTournamentGroups(tournamentId),
                        tournamentQuerys.getTournamentGames(tournamentId)
                    ])
                        .then(([tMembers,rounds,groups,games])=>{
                            console.log('tMembers',tMembers)
                            console.log('rounds',rounds);
                            console.log('groups',groups);
                            console.log('games',games);

                            let roundType = 0;
                            if(rounds.length === 1 && rounds[0].tournament_round_type === 1){
                                roundType = 1;
                            }else if(rounds.length === 2 && rounds[0].tournament_round_type === 1 && rounds[1].tournament_round_type === 2){
                                roundType = 2;
                            }else if(rounds.length === 3 && rounds[0].tournament_round_type === 1 && rounds[1].tournament_round_type === 2 && rounds[2].tournament_round_type === 3){
                                roundType = 3;
                            }
                            for(let i = 0; i < rounds.length; i++){
                                if(rounds[i].tournament_round_type == '1'){
                                    resultIng.quarterFinals.round_id = rounds[i].tournament_round_id;
                                    for(var j = 0; j < groups.length; j++){
                                        if(groups[j].tournament_round_id == rounds[i].tournament_round_id){
                                            var groupObj = {}
                                            groupObj.group_id = groups[j].tournament_group_id
                                            var gamesArr = [];
                                            for(var k = 0; k < games.length; k++){
                                                if(games[k].tournament_group_id == groups[j].tournament_group_id){
                                                    gamesArr.push({
                                                        game_id :games[k].tournament_games_id,
                                                        members_id :games[k].members_id,
                                                        usernames :games[k].usernames,
                                                        winner :games[k].winner
                                                    })
                                                }
                                            }
                                            groupObj.games = gamesArr
                                        }
                                        resultIng.quarterFinals.groups.push(groupObj)
                                    }
                                }else if(rounds[i].tournament_round_type == '2'){

                                }else if(rounds[i].tournament_round_type == '3'){

                                }
                            }

                            // console.log('tMembers',tMembers)
                            // console.log('resultIng',resultIng.quarterFinals.groups.games)
                            res.json({
                                error:false,
                                data:{
                                    tournament : thisTournament[0],
                                    tournamentMembers : tMembers,
                                    actionType : actionType,
                                    roundType : roundType,
                                    regUsers : [] ,
                                    data : resultIng
                                }
                            })
                            res.end();
                        })
                        .catch((error)=>{
                            log.insertLog(JSON.stringify(error) +" ,errorSelfCode:'42011'","./logs/v1/tournament.txt");
                            res.json({error:true,msg:"42011"})
                            res.end();
                        })
            // }else{
            //     res.json({error:false,data:{tournament : thisTournament[0],tournamentMembers : thisTournamentMembers,  regUsers : [], rounds : [] }})
            //     res.end();
            // }
        })
        .catch(function (error) {
            log.insertLog(JSON.stringify(error) +" ,errorSelfCode:'42010'","./logs/v1/tournament.txt");
            res.json({error:true,msg:"42010"})
            res.end();
        })
});

module.exports = router;


function createQuarterFinals(tournamentId) {
    return new Promise((resolve, reject) => {
        querys.findTournamentMembersInfo(tournamentId)
            .then(function (members) {
                console.log('members',members);

                querys.insert2vPromise('tournament_round',{tournament_id :tournamentId, type : 1 })
                    .then(function (RoundInsertResult) {
                        if(members.length === 8) {
                            Promise.all([
                                querys.insert2vPromise('tournament_group', {
                                    tournament_id: tournamentId,
                                    tournament_round_id: RoundInsertResult.insertId
                                }),
                                querys.insert2vPromise('tournament_group', {
                                    tournament_id: tournamentId,
                                    tournament_round_id: RoundInsertResult.insertId
                                })
                            ])
                                .then(function ([group1, group2]) {
                                    let membersArr = [];
                                    members.forEach((item) => {
                                        membersArr.push(item.tournament_members_id)
                                    })
                                    console.log('membersArr', membersArr);
                                    let result = [];
                                    for (let i = 0; i < 4; i++) {
                                        let sArr = []
                                        for (let j = 0; j < 2; j++) {
                                            let item = membersArr[Math.floor(Math.random() * membersArr.length)];
                                            // console.log('item',item)
                                            sArr.push(item)
                                            membersArr.splice(membersArr.indexOf(item), 1)

                                            // console.log(membersArr)
                                        }
                                        result.push(sArr)
                                    }
                                    console.log('group1', group1);
                                    console.log('group2', group2);

                                    Promise.all([
                                        querys.insert2vPromise('tournament_games', {
                                            tournament_id: tournamentId,
                                            tournament_group_id: group1.insertId
                                        }),
                                        querys.insert2vPromise('tournament_games', {
                                            tournament_id: tournamentId,
                                            tournament_group_id: group1.insertId
                                        }),
                                        querys.insert2vPromise('tournament_games', {
                                            tournament_id: tournamentId,
                                            tournament_group_id: group2.insertId
                                        }),
                                        querys.insert2vPromise('tournament_games', {
                                            tournament_id: tournamentId,
                                            tournament_group_id: group2.insertId
                                        }),
                                    ])
                                        .then(function (games) {
                                            console.log('games', games);
                                            // console.log('game2',game2);
                                            // console.log('game3',game3);
                                            // console.log('game4',game4);
                                            let allGamesInsertQuerys = [];
                                            // console.log('result',result);
                                            for (let i = 0; i < result.length; i++) {
                                                for (let j = 0; j < result[i].length; j++) {
                                                    allGamesInsertQuerys.push(
                                                        querys.insert2vPromise('tournament_games_members', {
                                                            tournament_games_id: games[i].insertId,
                                                            tournament_members_id: result[i][j]
                                                        })
                                                    )
                                                }
                                            }
                                            Promise.all(allGamesInsertQuerys)
                                                .then(function (allGamesInsertResult) {
                                                    console.log('allGamesInsertResult', allGamesInsertResult);
                                                })
                                                .catch(function (error) {
                                                    console.log('error', error);
                                                })
                                        })
                                        .catch(function (error) {
                                            console.log('error', error);
                                        })

                                })
                                .catch(function (error) {
                                    console.log('error', error);
                                })
                        }else if(members.length === 6){
                            Promise.all([
                                querys.insert2vPromise('tournament_group', {
                                    tournament_id: tournamentId,
                                    tournament_round_id: RoundInsertResult.insertId
                                }),
                                querys.insert2vPromise('tournament_group', {
                                    tournament_id: tournamentId,
                                    tournament_round_id: RoundInsertResult.insertId
                                })
                            ])
                                .then(function ([group1, group2]) {
                                    let membersArr = [];
                                    members.forEach((item) => {
                                        membersArr.push(item.tournament_members_id)
                                    })
                                    console.log('membersArr', membersArr);
                                    let result = [];
                                    for (let i = 0; i < 3; i++) {
                                        let sArr = []
                                        for (let j = 0; j < 2; j++) {
                                            let item = membersArr[Math.floor(Math.random() * membersArr.length)];
                                            // console.log('item',item)
                                            sArr.push(item)
                                            membersArr.splice(membersArr.indexOf(item), 1)

                                            // console.log(membersArr)
                                        }
                                        result.push(sArr)
                                    }
                                    console.log('group1', group1);
                                    console.log('group2', group2);

                                    Promise.all([
                                        querys.insert2vPromise('tournament_games', {
                                            tournament_id: tournamentId,
                                            tournament_group_id: group1.insertId
                                        }),
                                        querys.insert2vPromise('tournament_games', {
                                            tournament_id: tournamentId,
                                            tournament_group_id: group1.insertId
                                        }),
                                        querys.insert2vPromise('tournament_games', {
                                            tournament_id: tournamentId,
                                            tournament_group_id: group2.insertId
                                        })
                                    ])
                                        .then(function (games) {
                                            console.log('games', games);
                                            // console.log('game2',game2);
                                            // console.log('game3',game3);
                                            // console.log('game4',game4);
                                            let allGamesInsertQuerys = [];
                                            // console.log('result',result);
                                            for (let i = 0; i < result.length; i++) {
                                                for (let j = 0; j < result[i].length; j++) {
                                                    allGamesInsertQuerys.push(
                                                        querys.insert2vPromise('tournament_games_members', {
                                                            tournament_games_id: games[i].insertId,
                                                            tournament_members_id: result[i][j]
                                                        })
                                                    )
                                                }
                                            }
                                            Promise.all(allGamesInsertQuerys)
                                                .then(function (allGamesInsertResult) {
                                                    console.log('allGamesInsertResult', allGamesInsertResult);
                                                })
                                                .catch(function (error) {
                                                    console.log('error', error);
                                                })
                                        })
                                        .catch(function (error) {
                                            console.log('error', error);
                                        })
                                });
                        }else if(members.length === 4){

                        }
                    })
                    .catch(function (error) {
                        console.log('error',error);
                    })

            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error) +" ,errorSelfCode:'43001'","./logs/v1/tournament.txt");
                reject({error:true,msg:"43001"})
            })
    })
}

function createRoundsGroupsAndGames(tournamentId) {
    return new Promise((resolve, reject) => {
        querys.findByMultyNamePromise('tournament_round',{tournament_id  : tournamentId},['id'])
            .then(function (checkRound) {
                console.log('checkRound',checkRound);
                if(checkRound.length === 0){
                    createQuarterFinals(tournamentId)
                }else{

                }
            })
            .catch(function (error) {
                console.log('error',error);
            })

        //************************************

        // Promise.all([
        //     querys.findTournamentMembersInfo(tournamentId),
        //     querys.findByMultyNamePromise('tournament_round',{tournament_id  : tournamentId},['id'])
        // ])
        //     .then(function ([members,checkRound]) {
        //         console.log('members',members);
        //         // let allQuerys = [];
        //         let membersArr = [];
        //         members.forEach((item)=>{
        //             membersArr.push(item.user_id)
        //         })
        //         let result = [];
        //         for(let i = 0; i < 4; i++ ){
        //             let sArr = []
        //             for(let j = 0; j < 2; j++ ){
        //                 let item = membersArr[Math.floor(Math.random() * membersArr.length)];
        //                 // console.log('item',item)
        //                 sArr.push({user_id : item,tournament_id : tournamentId})
        //                 membersArr.splice(membersArr.indexOf(item),1)
        //
        //                 // console.log(membersArr)
        //             }
        //             result.push(sArr)
        //         }
        //         if(checkRound.length === 4){
        //             resolve(result)
        //         }else{
        //             console.log('result',result)
        //             // roundsAndGames(0,result,tournamentId, function () {
        //             //     console.log('resultss',result)
        //             //     resolve(result)
        //             // })
        //         }
                //
                // console.log('tournamentId',tournamentId)

            // })
            // .catch(function (error) {
            //     log.insertLog(JSON.stringify(error) +" ,errorSelfCode:'43001'","./logs/v1/tournament.txt");
            //     reject({error:true,msg:"43001"})
            // })
    })
}



function roundsAndGames(num,result,tournamentId,cb) {
    querys.insert2vPromise('tournament_round',{tournament_id :tournamentId, type : 1 })
        .then(function (data) {
            // console.log('data',data)
            var allQureys = [];
            for(var i = 0; i < result[num].length; i++){
                allQureys.push(
                    querys.insert2vPromise('tournament_games',{
                        tournament_id : tournamentId,
                        tournament_round_id : data.insertId,
                        tournament_members_id :  result[num][i].user_id
                    })
                )
            }
            Promise.all([allQureys])
                .then(function (res) {
                    if(num === result.length - 1){
                        cb()
                    }else{
                        num++;
                        roundsAndGames(num,result,tournamentId,cb)
                    }
                })
                .catch(function (error) {
                    console.log('22 error',error)
                })

        })
        .catch(function (error) {
            console.log('11 error',error)
        })
    // return 's'
}


// function checkUserMembership(userId,thisTournamentMembers) {
//     var checking = false;
//     for(var i = 0; i < thisTournamentMembers.length;i++){
//         if(thisTournamentMembers[i] == userId){
//             checking = true
//         }
//     }
//     return checking
// }


