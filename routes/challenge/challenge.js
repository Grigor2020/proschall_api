const express = require('express');
const router = express.Router();
const log = require("../../logs/log");
const challengeQuerys = require('../../model/querys/v1/challenge')
const matchQuerys = require('../../model/querys/v1/match')
const querys = require('../../model/querys/querys');
const gamesQuerys = require('../../model/querys/v1/games')
const modeQuerys = require('../../model/querys/v1/mode')
const serverQuerys = require('../../model/querys/v1/server')
const consoleQuerys = require('../../model/querys/v1/console')
const statics = require('../../static');
const checkUser = require('../../object/auth/auth');
const staticMethods = require('../../model/staticMethods');
const {schedulerService} = require("../../service/scheduler.service");

router.post('/get-list',checkUser.checkUserOrGuest(), function(req, res) {

    const page = typeof req.body.page === "undefined" ? 1 : req.body.page
    if(typeof req.body.game_url !== "undefined"){
        const game_url = req.body.game_url;
        const reqConsole = req.body.console;
        const reqPrize = req.body.prize;
        if(game_url === 'all'){
            Promise.all([
                challengeQuerys.getAllChallengeList(page,reqPrize,reqConsole,req.userId),
                challengeQuerys.getAllGameConsoles(),
                challengeQuerys.getAllGamesMinAndMaxPrices()
            ])
                .then(function ([list,consoles,prices]) {
                    for(let i = 0; i < list.length; i++){
                        list[i].winner_priz = (2 * parseFloat(list[i].prize)) - ((2 * parseFloat(list[i].prize))*statics.SITE_PERCENT/100);
                    }
                    res.json({error:false, data:{list:list,consoles:consoles,prices:prices[0]}})
                    res.end();
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error)+" errorSelfCode:'18589'","./logs/v1/challenge.txt");
                    res.json({error:true,msg:"18589"});
                    res.end();
                })
        }else{
            gamesQuerys.findByUrl(game_url)
                .then(function (game) {
                    const game_id = game[0].id;
                    Promise.all([
                        challengeQuerys.getChallengeList(game_id,reqPrize,reqConsole,page,req.userId),
                        challengeQuerys.getGameConsoles(game_id),
                        challengeQuerys.getGameMinAndMaxPrices(game_id)
                    ])
                        .then(function ([list,consoles,prices]) {
                            let resList = staticMethods.getWinnerPrize(list)
                            res.json({error:false, data:{list:resList,consoles:consoles,prices:prices[0]}})
                            res.end();
                        })
                        .catch(function (error) {
                            log.insertLog(JSON.stringify(error)+" errorSelfCode:'18589'","./logs/v1/challenge.txt");
                            res.json({error:true,msg:"18589"});
                            res.end();
                        })
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error)+" errorSelfCode:'18588'","./logs/v1/challenge.txt");
                    res.json({error:true,msg:"18588"});
                    res.end();
                })
        }
    }else {
        log.insertLog(" errorSelfCode:'18586'","./logs/v1/challenge.txt");
        res.json({error:true,msg:"18586"});
        res.end();
    }

});

router.post('/get_game_info', function(req, res) {
    if(typeof req.body.gameId !== "undefined"){
        // var langId = typeof req.body.langId !== "undefined" ? req.body.langId : 1;
        Promise.all([
            modeQuerys.findAllModesByGameId(req.body.gameId),
            serverQuerys.findAllServersByGameId(req.body.gameId),
            consoleQuerys.findAllConsolesByGameId(req.body.gameId)
        ])
            .then(function ([mode,server,console]) {
                res.json({error:false,data:{mode:mode,server:server,console:console}})
                res.end();
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+" , errorSelfCode:'15698'","./logs/v1/challenge.txt");
                res.json({error:true,data:[]});
                res.end();
            })
    }else {
        log.insertLog("errorSelfCode:'486513'","./logs/v1/challenge.txt");
        res.json({error:true});
        res.end();
    }

});

router.post('/get-my',checkUser.checkValidUser(), function(req, res) {
    const langId = typeof req.body.langId !== "undefined" ? req.body.langId : 1;
    const userId = req.userId;
    Promise.all([
        challengeQuerys.getMyChallenges(userId,langId),
        challengeQuerys.getMyOngoingChallenges(userId,langId)
    ])

        .then(function ([data,ongoing]) {
            let ongoingIds = [];
            for(let t = 0; t < ongoing.length; t++){
                ongoingIds.push(ongoing[t].challenge_id);
            }


            let availableGames = [];
            let availableConsoles = [];

            for(let i = 0; i < data.length; i++){
                data[i].is_ongoing = ongoingIds.indexOf(data[i].challenge_id) !== -1;


                data[i].console_img = statics.API_URL+'/images/consoles/'+data[i].console_image;
                delete data[i].console_image;

                data[i].winner_priz = (2 * parseFloat(data[i].prize)) - ((2 * parseFloat(data[i].prize))*statics.SITE_PERCENT/100);

                const obj1 = {
                    id : data[i].game_id,
                    game_image : data[i].game_image,
                    name : data[i].game_name
                };
                let existsGame = false;
                for(let j = 0 ; j < availableGames.length; j++){
                    if(availableGames[j].id === obj1.id){
                        existsGame = true;
                    }
                }
                if(!existsGame){
                    availableGames.push(obj1);
                }

                const obj2 = {
                    id : data[i].console_id,
                    name : data[i].console_name,
                    image : data[i].console_image
                };
                let existsConsoles = false;
                for(let k = 0 ; k < availableConsoles.length; k++){
                    if(availableConsoles[k].id === obj2.id){
                        existsConsoles = true;
                    }
                }
                if(!existsConsoles){
                    availableConsoles.push(obj2);
                }
            }

            res.json(
                    {
                        error:false,
                        ongoing:ongoing,
                        games:availableGames,
                        consoles:availableConsoles,
                        data:data
                    }
                );
            res.end();
        })
        .catch(function (error) {
            log.insertLog(JSON.stringify(error)+" , errorSelfCode:'41579'","./logs/v1/challenge.txt");
            res.json({error:true,errMsg:'41579'});
            res.end();
        })
});

router.post('/get-one-challenge',checkUser.checkUserOrGuest(), function(req, res) {
    if(typeof req.body.challenge_id !== "undefined"){
        // console.log('req.body.challenge_id',req.body.challenge_id)
        challengeQuerys.findOneChallenge(req.body.challenge_id)
            .then(function (data) {
                data[0].description = data[0].description.replace(/(?:\r\n|\r|\n)/g, '<br>')
                let resList = staticMethods.getWinnerPrize(data)
                if(req.userId !== null){
                    matchQuerys.findJoinerByChallengeId(req.userId,req.body.challenge_id)
                        .then((data)=>{
                            if(data.length > 0){
                                res.json({error:false,data:resList, joining : true});
                                res.end();
                            }else{
                                res.json({error:false,data:resList, joining : false});
                                res.end();
                            }
                        })
                        .catch((error)=>{
                            log.insertLog(JSON.stringify(error)+" , errorSelfCode:'486791'","./logs/v1/challenge.txt");
                            res.json({error:true,data:[]});
                            res.end();
                        })
                }else{
                    res.json({error:false,data:resList, joining : false});
                    res.end();
                }
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+" , errorSelfCode:'486691'","./logs/v1/challenge.txt");
                res.json({error:true,data:[]});
                res.end();
            })
    }else {
        log.insertLog("errorSelfCode:'486689'", "./logs/v1/challenge.txt");
        res.json({error: true});
        res.end();
    }

});

router.post('/add-game-username',checkUser.checkValidUser(), function(req, res) {
    // console.log('req.body',req.body);
    // console.log()
    if(typeof req.body.game_id !== "undefined" &&  typeof req.body.userName !== "undefined"){
        querys.findUserUsernames(req.userId,req.body.game_id)
            .then(function (data) {
                if (data.length === 0) {
                    let postData = {
                        game_id : req.body.game_id,
                        user_id : req.userId,
                        name : req.body.userName
                    }
                    querys.insert2vPromise('game_usernames',postData)
                        .then(function (data) {
                            res.json({error:false,msg:"",data:data});
                            res.end();
                        })
                        .catch(function (error) {
                            log.insertLog(JSON.parse(error)+" errorSelfCode:'33668'","./logs/v1/challenge.txt");
                            res.json({error:true,msg:"33668"});
                            res.end();
                        })
                }else {
                    res.json({error:false,msg:"31863"});
                    res.end();
                }
            })
            .catch(function (error) {
                log.insertLog(JSON.parse(error)+" errorSelfCode:'33660'","./logs/v1/challenge.txt");
                res.json({error:true,msg:"28976"});
                res.end();
            })
    }else{
        log.insertLog(" errorSelfCode:'36579'","./logs/v1/challenge.txt");
        res.json({error:true, msg:"36579"});
        res.end();
    }
});

router.post('/new',checkUser.checkValidUser(), async function(req, res) {
    try {
        if(
            typeof req.body.game_id !== "undefined" &&
            typeof req.body.description !== "undefined" &&
            typeof req.body.mode_id !== "undefined" &&
            typeof req.body.prize !== "undefined" &&
            typeof req.body.server_id !== "undefined" &&
            typeof req.body.console_id !== "undefined"
        ){
            querys.findByMultyNamePromise('challenge',{user_id:req.userId,started:'1'},['id'])
            .then(function(busyData){
                if(busyData.length === 0){
                    if(parseFloat(req.body.prize) >= 1) {
                        let checkArr = [
                            querys.findByMultyNamePromise('games', {id: req.body.game_id}, ['id']),
                            querys.findByMultyNamePromise('game_mode', {id: req.body.mode_id}, ['id']),
                            querys.findByMultyNamePromise('game_consoles', {id: req.body.console_id}, ['id'])
                        ];
                        Promise.all(checkArr)
                            .then(function (datas) {
                                // console.log('datas',datas)
                                let sxal = false;
                                for (let i = 0; i < datas.length; i++) {
                                    if (datas[i].length === 0) {
                                        sxal = true;
                                    }
                                }
                                if (!sxal) {
                                    querys.findUserUsernames(req.userId, req.body.game_id)
                                        .then(function (data) {
                                            if (data.length === 0) {
                                                res.json({error: false, msg: "33659"});
                                                res.end();
                                            } else {
                                                let isNum = /.\d+$/.test(parseFloat(req.body.prize).toFixed(2));
                                                // console.log('isNum', isNum)
                                                // console.log('req.body.prize', typeof req.body.prize)
                                                // console.log('req.body.prize.length', req.body.prize.length)
                                                if (isNum && req.body.prize.length < 15) {
                                                    querys.checkUserMoney(req.userId)
                                                        .then(function (checkMoney) {
                                                            // console.log('checkMoney', checkMoney)
                                                            const userMoney = checkMoney[0].money;
                                                            if (parseFloat(userMoney) < parseFloat(req.body.prize)) {
                                                                log.insertLog("user hs no enough money -> errorSelfCode:'24444'", "./logs/v1/challenge.txt");
                                                                res.json({error: false, msg: "24444"});
                                                                res.end();
                                                            } else {
                                                                let insertInfo = {
                                                                    user_id: req.userId,
                                                                    game_id: req.body.game_id,
                                                                    mode_id: req.body.mode_id,
                                                                    description: req.body.description,
                                                                    prize: parseFloat(req.body.prize).toFixed(2),
                                                                    console_id: req.body.console_id
                                                                }
                                                                if (req.body.server_id !== 'null')
                                                                    insertInfo.server_id = req.body.server_id

                                                                querys.insert2vPromise('challenge', insertInfo)
                                                                    .then(async function () {
                                                                        res.json({error: false, msg: '0000'});
                                                                        res.end();
                                                                    })
                                                                    .catch(function (error) {
                                                                        log.insertLog(JSON.stringify(error) + " errorSelfCode:'28976'", "./logs/v1/challenge.txt");
                                                                        res.json({error: true, msg: "28976"});
                                                                        res.end();
                                                                    })
                                                            }

                                                        })
                                                        .catch(function (error) {
                                                            log.insertLog(JSON.parse(error) + " errorSelfCode:'33999'", "./logs/v1/challenge.txt");
                                                            res.json({error: true, msg: "33999"});
                                                            res.end();
                                                        })
                                                } else {
                                                    log.insertLog("errorSelfCode:'28893'", "./logs/v1/challenge.txt");
                                                    res.json({error: true, msg: "28893"});
                                                    res.end();
                                                }
                                            }
                                        })
                                        .catch(function (error) {
                                            log.insertLog(JSON.parse(error) + " errorSelfCode:'33660'", "./logs/v1/challenge.txt");
                                            res.json({error: true, msg: "28976"});
                                            res.end();
                                        })
                                } else {
                                    log.insertLog(" errorSelfCode:'29999'", "./logs/v1/challenge.txt");
                                    res.json({error: true, msg: "29999"});
                                    res.end();
                                }
                            })
                            .catch(function (errors) {
                                log.insertLog(JSON.parse(errors) + " errorSelfCode:'29999'", "./logs/v1/challenge.txt");
                                res.json({error: true, msg: "29999"});
                                res.end();
                            })
                        // console.log('req.body.server_id',typeof req.body.server_id)
                    }else{
                        log.insertLog("prize is less then 1$ -> errorSelfCode:'24555'", "./logs/v1/challenge.txt");
                        res.json({error: false, msg: "24555"});
                        res.end();
                    }
                }else{
                    log.insertLog(" errorSelfCode:'28902'","./logs/v1/challenge.txt");
                    res.json({error:true, msg:"28902"});
                    res.end();
                }
            })
            .catch(function(){
                log.insertLog(" errorSelfCode:'28901'","./logs/v1/challenge.txt");
                res.json({error:true, msg:"28901"});
                res.end();
            })


        }else{
            log.insertLog(" errorSelfCode:'2148'","./logs/v1/challenge.txt");
            res.json({error:true, msg:"2148"});
            res.end();
        }
    } catch (err) {
        res.json(err);
        res.end();
    }
});

module.exports = router;
