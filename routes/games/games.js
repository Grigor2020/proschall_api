var express = require('express');
var router = express.Router();
var log = require("../../logs/log");
var gamesQuerys = require('../../model/querys/v1/games')
var statics = require('../../static');

/* GET games listing. */
router.get('/all/:langId', function(req, res, next) {
    var langId = typeof req.params.langId !== "undefined" ? req.params.langId : 1
    gamesQuerys.findAllGames(langId)
        .then(function (data) {
            res.json({error:false,data:data})
        })
        .catch(function (error) {
            log.insertLog(JSON.stringify(error)+" , errorSelfCode:'15698'","./logs/v1/games.txt");
            res.json({error:false,data:[]});
        })
});

module.exports = router;
