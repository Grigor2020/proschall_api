var express = require('express');
var router = express.Router();
var log = require("../../logs/log");
var challengeQuerys = require('../../model/querys/v1/challenge')
var notificationQuerys = require('../../model/querys/v1/notification')
var querys = require('../../model/querys/querys');
var gamesQuerys = require('../../model/querys/v1/games')
var modeQuerys = require('../../model/querys/v1/mode')
var serverQuerys = require('../../model/querys/v1/server')
var consoleQuerys = require('../../model/querys/v1/console')
var statics = require('../../static');
var checkUser = require('../../object/auth/auth');


router.post('/viewed',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.notificationId !== "undefined"){
        notificationQuerys.setViewedNotification(req.body.notificationId)
            .then(function (data) {
                res.json({error:false,msg:"0000"})
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+" errorSelfCode:'66598'","./logs/v1/notification.txt");
                res.json({error:true,msg:"66598"})
                res.end();
            })
    }else{
        log.insertLog(" errorSelfCode:'66589'","./logs/v1/notification.txt");
        res.json({error:true,msg:"66589"})
        res.end();
    }

});

router.post('/get-all',checkUser.checkValidUser(), function(req, res, next) {
    Promise.all([
        notificationQuerys.findUserNotifications(req.userId),
        notificationQuerys.findUserUnreadCount(req.userId),
    ])
        .then(function ([notes,unread]) {
            // console.log('notes',notes)
            // console.log('unread',unread)
            res.json(
                {
                    error: false,
                    msg: "1000",
                    notes : notes,
                    unreadCount : unread[0].counts
                });
            res.end();
        })
        .catch(function (error) {
            console.log('error',error)
        })
        // log.insertLog(" errorSelfCode:'66589'","./logs/v1/notification.txt");
        // res.json({error:true,msg:"66589"})
        // res.end();
});

module.exports = router;
