var express = require('express');
var router = express.Router();
var log = require("../../logs/log");
var gamesQuerys = require('../../model/querys/v1/games')
var statics = require('../../static');
var sendMailAuth = require('../../mailer/sendmail');
var querys = require('../../model/querys/querys');
var sha1 = require('sha1');


router.post('/check', function(req, res, next) {
    if(typeof req.body.email !== "undefined") {
        querys.findByMultyNamePromise('users_db',{email:req.body.email},['*'])
            .then(function (userData) {
                if(userData.length === 1){
                    var userId = userData[0].id;
                    var sendCode = makeid(6);
                    querys.findByMultyNamePromise('email_codes',{user_id :userId},['*'])
                        .then(function (checkEmail) {
                            if(checkEmail.length > 0){
                                querys.update2vPromise('email_codes',{values:{code:sendCode},where:{user_id:userId}})
                                    .then(function (data) {
                                        var x = sendMailAuth(userData[0].email, "",{user_name:userData[0].username,ver_code:sendCode},'forgot_password');
                                        res.json({error: false,email:userData[0].email});
                                    })
                                    .catch(function (error) {
                                        log.insertLog(JSON.stringify(error)+ " errorSelfCode:'80005'", "./logs/v1/forgot.txt");
                                        res.json({error: true,msg:'80005'});
                                        res.end();
                                    })
                            }else {
                                querys.insert2vPromise('email_codes',{user_id:userId,code:sendCode})
                                    .then(function (data) {
                                        var x = sendMailAuth(userData[0].email, "",{user_name:userData[0].username,ver_code:sendCode},'forgot_password');
                                        res.json({error: false,email:userData[0].email});
                                    })
                                    .catch(function (error) {
                                        log.insertLog(JSON.stringify(error)+ " errorSelfCode:'80004'", "./logs/v1/forgot.txt");
                                        res.json({error: true,msg:'80004'});
                                        res.end();
                                    })
                            }
                        })
                        .catch(function () {

                        })


                }else{
                    log.insertLog(" errorSelfCode:'80003'", "./logs/v1/forgot.txt");
                    res.json({error: true,msg:'80003'});
                    res.end();
                }

            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+ " errorSelfCode:'80002'", "./logs/v1/forgot.txt");
                res.json({error: true,msg:'80002'});
                res.end();
            })
    }else{
        log.insertLog(" errorSelfCode:'80001'", "./logs/v1/forgot.txt");
        res.json({error: true,msg:'80001'});
        res.end();
    }
});

router.post('/code', function(req, res, next) {
    // console.log('userData',userData)
    if(typeof req.body.email !== "undefined" && typeof req.body.code !== "undefined" ) {
        querys.findByMultyNamePromise('users_db',{email:req.body.email},['*'])
            .then(function (userData) {
                // console.log('userData',userData)
                if(userData.length === 1){
                    var userId =  userData[0].id;
                    querys.findByMultyNamePromise('email_codes',{user_id :userId,code :req.body.code},['*'])
                        .then(function (codeData) {
                            if(codeData.length === 1){
                                querys.deletesPromise('email_codes',{user_id :userId,code :req.body.code})
                                    .then(function (data) {
                                        res.json({error:false,token:userData[0].token})
                                    })
                                    .catch(function (error) {
                                        log.insertLog(" errorSelfCode:'80106'", "./logs/v1/forgot.txt");
                                        res.json({error: true,msg:'80106'});
                                        res.end();
                                    })
                            }else{
                                log.insertLog(" errorSelfCode:'80105'", "./logs/v1/forgot.txt");
                                res.json({error: true,msg:'80105'});
                                res.end();
                            }
                        })
                        .catch(function (error) {
                            log.insertLog(JSON.stringify(error) + " errorSelfCode:'80104'", "./logs/v1/forgot.txt");
                            res.json({error: true,msg:'80104'});
                            res.end();
                        })
                }else{
                    log.insertLog(" errorSelfCode:'80103'", "./logs/v1/forgot.txt");
                    res.json({error: true,msg:'80103'});
                    res.end();
                }
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error) + " errorSelfCode:'80102'", "./logs/v1/forgot.txt");
                res.json({error: true,msg:'80102'});
                res.end();
            })
    }else{
        log.insertLog(" errorSelfCode:'80101'", "./logs/v1/forgot.txt");
        res.json({error: true,msg:'80101'});
        res.end();
    }
});

router.post('/set-password', function(req, res, next) {
    if(typeof req.body.password !== "undefined" && typeof req.body.repassword !== "undefined" && typeof req.body.token !== "undefined"){
        if(req.body.password === req.body.repassword){
            if(req.body.password.length >= 8){
                querys.findByToken(req.body.token)
                    .then(function (userData) {
                        var newPass = sha1(req.body.password);
                        var token= sha1(sha1(userData[0].email) + "" + new Date().getTime());
                        querys.update2vPromise('users_db',{where:{token:req.body.token},values:{password:newPass}})
                            .then(function (data) {
                                res.json({error: false});
                            })
                            .catch(function (error) {
                                log.insertLog(" errorSelfCode:'80205'", "./logs/v1/forgot.txt");
                                res.json({error: true,msg:'80205'});
                                res.end();
                            })
                    })
                    .catch(function () {
                        log.insertLog(" errorSelfCode:'80204'", "./logs/v1/forgot.txt");
                        res.json({error: true,msg:'80204'});
                        res.end();
                    })
            }else{
                log.insertLog(" errorSelfCode:'80203'", "./logs/v1/forgot.txt");
                res.json({error: true,msg:'80203'});
                res.end();
            }

        }else{
            log.insertLog(" errorSelfCode:'80202'", "./logs/v1/forgot.txt");
            res.json({error: true,msg:'80202'});
            res.end();
        }
    }else{
        log.insertLog(" errorSelfCode:'80201'", "./logs/v1/forgot.txt");
        res.json({error: true,msg:'80201'});
        res.end();
    }
});


module.exports = router;


function makeid(length) {
    var result           = '';
    var characters       = '0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
