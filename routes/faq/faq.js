var express = require('express');
var router = express.Router();
var log = require("../../logs/log");
var gamesQuerys = require('../../model/querys/v1/games')
var statics = require('../../static');
var sendMailAuth = require('../../mailer/sendmail');
var querys = require('../../model/querys/querys');
var sha1 = require('sha1');


router.post('/all', function(req, res, next) {
    if(typeof req.body.lang_id !== "undefined") {
        querys.findByMultyNamePromise('faq_lang',{lang_id : req.body.lang_id},['question','answer'])
            .then(function (data){
                res.json({error: false,data:data});
            })
            .catch(function (error) {
                log.insertLog(" errorSelfCode:'64002'", "./logs/all-logs.txt");
                res.json({error: true,msg:'64002'});
                res.end();
            })
    }else{
        log.insertLog(" errorSelfCode:'64001'", "./logs/all-logs.txt");
        res.json({error: true,msg:'64001'});
        res.end();
    }

});



module.exports = router;
