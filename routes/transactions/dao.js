const db = require('../../model/connection');

module.exports.transactionsDao = {

    async getByUserIdAndMatchId(user_id, matchId) {
        return new Promise((resolve, reject) => {
            const query = 'SELECT * FROM  `transactions_history` WHERE `user_id`=' + db.escape(user_id) + ' AND `matches_id` = ' + db.escape(matchId);
            db.query(query, function (error, rows) {
                if (error){
                    reject(new Error("Sql error:  "+error.sqlMessage+""))
                }else{
                    resolve(rows[0])
                }
            });
        })
    }

}