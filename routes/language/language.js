var express = require('express');
var router = express.Router();
var log = require("../../logs/log");
var querys = require('../../model/querys/querys')
var statics = require('../../static');

/* GET games listing. */
router.get('/all', function(req, res, next) {
    querys.findAllByFildsPromise('language','*')
        .then(function (data) {
            for(var i = 0; i < data.length; i++){
                var imgName = data[i].lang_img;
                data[i].img = statics.MAIN_URL+"/images/lang/"+imgName;
                delete data[i].lang_img;
            }
            res.json({error:false,data:data})
        })
        .catch(function (error) {
            log.insertLog(JSON.stringify(error)+" , errorSelfCode:'0147'","./logs/query1v.txt");
            res.json({error:false,data:[]});
        })
});

module.exports = router;
