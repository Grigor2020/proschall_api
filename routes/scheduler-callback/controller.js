const schedulerCallbackService = require('./service').schedulerCallbackService

module.exports.schedulerCallbackController = {

    async handeCallback(req, res) {
        try {
            schedulerCallbackService.checkMatchStatus(req.params.matchId)
                .then(data => {
                    console.log('data', data)
                    res.json(data).status(200)
                })
                .catch(err => {
                    console.log('err', err)
                    res.json().status(400)
                })

        } catch (err) {
            res.json(err)
        }
    }

}