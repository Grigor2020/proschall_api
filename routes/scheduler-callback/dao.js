const db = require('../../model/connection');

module.exports.schedulerCallbackDao = {

    getMatchById(matchId) {
        return new Promise((resolve, reject) => {
            const query = 'SELECT * FROM  `matches` WHERE `id`=' + db.escape(matchId);
            db.query(query, function (error, rows) {
                if (error){
                    reject(new Error("Sql error:  "+error.sqlMessage+""))
                }else{
                    resolve(rows[0])
                }
            });
        })
    }

}