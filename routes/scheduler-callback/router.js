const express = require('express');
const router = express.Router();
const {schedulerCallbackController} = require("./controller");

router.get('/:matchId', schedulerCallbackController.handeCallback);

module.exports = router;