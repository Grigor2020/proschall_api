const {matchStatuses} = require("../../constants");
const dbQuery = require("../../model/querys/querys");
const {transactionsDao} = require("../transactions/dao");
const schedulerCallbackDao = require('./dao').schedulerCallbackDao;

module.exports.schedulerCallbackService = {

    async checkMatchStatus(matchId) {
        const match = await schedulerCallbackDao.getMatchById(matchId)
        if(!match) {
            throw 'Match Not Found'
        }
        //TODO: need to check for the first is match ended or not, then check started or not, and then check accpeted or not
        // this.checkIdEnded();
        // this.checkIdStarted();
        return this.checkIsAccepted(match);
    },

    checkIdEnded() {

    },

    checkIdStarted() {

    },

    async checkIsAccepted(match) {
        if (match.status === matchStatuses.accepted) {
            await dbQuery.deletesPromise('matches',{id:match.id});
            await dbQuery.update2vPromise('challenge',
                {
                    where:{
                        id : match.challenge_id
                    },
                    values : {
                        started : '0'
                    }
                }
            )

            return await this.returnMoneyWhenSomeoneAccepted(match);
        }
    },

    async returnMoneyWhenSomeoneAccepted(match) {
        let creatorChecked = false;
        let joinerChecked = false;

        if(match.joiner_start === '1') {
            const transaction = await transactionsDao.getByUserIdAndMatchId(match.joiner_id, match.id)
            const userMoney = await dbQuery.findByMultyNamePromise(
                'user_money',
                {user_id: match.joiner_id},
                ['user_id', 'money', 'id']
            )
            let moneyToUpdate = (parseFloat(userMoney[0].money) + parseFloat(transaction.money)).toFixed(2);

            await dbQuery.update2vPromise('user_money',
                {
                    where:{
                        user_id : match.joiner_id
                    },
                    values : {
                        money : moneyToUpdate
                    }
                }
            )
            joinerChecked = true
        }

        if (match.creator_start === '1') {
            const transaction = await transactionsDao.getByUserIdAndMatchId(match.creator_id, match.id)
            const userMoney = await dbQuery.findByMultyNamePromise(
                'user_money',
                {user_id: match.creator_id},
                ['user_id', 'money', 'id']
            )
            let moneyToUpdate = (parseFloat(userMoney[0].money) + parseFloat(transaction.money)).toFixed(2);

            await dbQuery.update2vPromise('user_money',
                {
                    where:{
                        user_id : match.creator_id
                    },
                    values : {
                        money : moneyToUpdate
                    }
                }
            )

            creatorChecked = true
        }

        if (match.creator_accept === '1' && !creatorChecked) {
            const transaction = await transactionsDao.getByUserIdAndMatchId(match.creator_id, match.id)
            const userMoney = await dbQuery.findByMultyNamePromise(
                'user_money',
                {user_id: match.creator_id},
                ['user_id', 'money', 'id']
            )
            let moneyToUpdate = (parseFloat(userMoney[0].money) + parseFloat(transaction.money)).toFixed(2);

            await dbQuery.update2vPromise('user_money',
                {
                    where:{
                        user_id : match.creator_id
                    },
                    values : {
                        money : moneyToUpdate
                    }
                }
            )
        }
    }
}