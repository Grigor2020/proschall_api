var express = require('express');
var router = express.Router();
var log = require("../../logs/log");
var profileQuerys = require('../../model/querys/v1/profile')
var challengeQuerys = require('../../model/querys/v1/challenge')
var notificationQuerys = require('../../model/querys/v1/notification')
var transactionQuerys = require('../../model/querys/v1/transaction')
var querys = require('../../model/querys/querys');
var gamesQuerys = require('../../model/querys/v1/games')
var modeQuerys = require('../../model/querys/v1/mode')
var serverQuerys = require('../../model/querys/v1/server')
var consoleQuerys = require('../../model/querys/v1/console')
var statics = require('../../static');
var checkUser = require('../../object/auth/auth');
var sha1 = require('sha1');
var sendmail = require('../../mailer/sendmail');

router.get('/get-info',checkUser.checkValidUser(), function(req, res, next) {
    // console.log('rsadsdf',req.userId)

    Promise.all([
        profileQuerys.findUserGameUsernames(req.userId),
        profileQuerys.findUserGameResults(req.userId),
    ])
        .then(function ([gameUsernames,gameResults]) {
            var resResults = {total : 0, win : 0, lost : 0 };
            for(var i = 0; i < gameResults.length; i++){
                resResults.total++;
                if(gameResults[i].winner_id == req.userId){
                    resResults.win++;
                }else{
                    resResults.lost++;
                }
            }
            res.json({error:false,data:{gameUsernames:gameUsernames,gameResults:resResults}})
        })
        .catch(function (error) {
            log.insertLog(JSON.stringify(error) +", errorSelfCode:'75863'","./logs/v1/profile.txt");
            res.json({error:true,msg:"66589"})
            res.end();
        })
    // if(typeof req.body.notificationId !== "undefined"){
    //     notificationQuerys.setViewedNotification(req.body.notificationId)
    //         .then(function (data) {
    //             res.json({error:false,msg:"0000"})
    //         })
    //         .catch(function (error) {
    //             log.insertLog(JSON.stringify(error)+" errorSelfCode:'66598'","./logs/v1/notification.txt");
    //             res.json({error:true,msg:"66598"})
    //             res.end();
    //         })
    // }else{

    // }

});

router.post('/deposit-add/bitcoin',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.amount !== "undefined" && typeof req.body.wallet_address !== "undefined"){
        let wallet_address = req.body.wallet_address;
        let amount = parseFloat(req.body.amount).toFixed(2);
        querys.insert2vPromise('deposit',{id:null})
            .then(function (depositData) {
                var depositPost = {
                    deposit_id : depositData.insertId,
                    user_id : req.userId,
                    wallet_address : wallet_address,
                    amount : amount,
                    type : '2',
                    admin_check : '0',
                    admin_accept : '0'
                }
                querys.insert2vPromise('deposit_bitcoin',depositPost)
                    .then(function (data) {
                        var params = {
                            user_id : req.userId,
                            type:'16',
                            view : "0",
                        }
                        var transactionParams = {
                            user_id : req.userId,
                            type:'2',
                            way : "1",
                            deposite_id : data.insertId,
                            money : amount,
                        }
                        Promise.all([
                            notificationQuerys.addNotification(params),
                            transactionQuerys.addTransaction(transactionParams)
                        ])

                            .then(function (data) {
                                res.json({error:false,msg:"0000"})
                            })
                            .catch(function (error) {
                                log.insertLog(JSON.stringify(error) +" errorSelfCode:'66547'","./logs/v1/notification.txt");
                                res.json({error:true,msg:"66547"});
                                res.end();
                            })
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error)+" errorSelfCode:'75006'","./logs/v1/profile.txt");
                        res.json({error:true,msg:"75006"})
                        res.end();
                    })
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+" errorSelfCode:'75005'","./logs/v1/profile.txt");
                res.json({error:true,msg:"75005"})
                res.end();
            })
    }else {
        log.insertLog(" errorSelfCode:'75004'","./logs/v1/profile.txt");
        res.json({error:true,msg:"75004"})
        res.end();
    }
});

router.post('/deposit-add/c-2-c',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.card_number !== "undefined" && typeof req.body.amount !== "undefined"){
        let cardNumber = req.body.card_number;
        let amount = parseFloat(req.body.amount).toFixed(2);
        querys.insert2vPromise('deposit',{id:null})
            .then(function (depositData) {
                var depositPost = {
                    deposit_id : depositData.insertId,
                    user_id : req.userId,
                    card_number : cardNumber,
                    amount : amount,
                    type : '1',
                    admin_check : '0',
                    admin_accept : '0'
                }
                querys.insert2vPromise('deposit_c2c',depositPost)
                    .then(function (data) {
                        var params = {
                            user_id : req.userId,
                            type:'16',
                            view : "0",
                        }
                        var transactionParams = {
                            user_id : req.userId,
                            type:'2',
                            way : "1",
                            deposite_id : data.insertId,
                            money : amount,
                        }
                        Promise.all([
                            notificationQuerys.addNotification(params),
                            transactionQuerys.addTransaction(transactionParams)
                        ])
                            .then(function (data) {
                                res.json({error:false,msg:"0000"})
                            })
                            .catch(function (error) {
                                log.insertLog(JSON.stringify(error) +" errorSelfCode:'66547'","./logs/v1/notification.txt");
                                res.json({error:true,msg:"66547"});
                                res.end();
                            })
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error)+" errorSelfCode:'75003'","./logs/v1/profile.txt");
                        res.json({error:true,msg:"75003"})
                        res.end();
                    })
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+" errorSelfCode:'75002'","./logs/v1/profile.txt");
                res.json({error:true,msg:"75002"})
                res.end();
            })
    }else {
        log.insertLog(" errorSelfCode:'75001'","./logs/v1/profile.txt");
        res.json({error:true,msg:"75001"})
        res.end();
    }
});
router.post('/deposit-add/perfect-money',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.e_voucher !== "undefined" && typeof req.body.activation_code !== "undefined"){
        let e_voucher = req.body.e_voucher;
        let activation_code = req.body.activation_code;
        // console.log('cardNumber',cardNumber)
        // console.log('amount',amount)
        querys.insert2vPromise('deposit',{id:null})
            .then(function (depositData) {
                var depositPost = {
                    deposit_id : depositData.insertId,
                    user_id : req.userId,
                    e_voucher : e_voucher,
                    activation_code : activation_code,
                    type : '3',
                    admin_check : '0',
                    admin_accept : '0'
                }
                querys.insert2vPromise('deposit_pm',depositPost)
                    .then(function (data) {
                        var params = {
                            user_id : req.userId,
                            type:'16',
                            view : "0",
                        }
                        notificationQuerys.addNotification(params)
                            .then(function (data) {
                                res.json({error:false,msg:"0000"})
                            })
                            .catch(function (error) {
                                log.insertLog(JSON.stringify(error) +" errorSelfCode:'66547'","./logs/v1/notification.txt");
                                res.json({error:true,msg:"66547"});
                                res.end();
                            })
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error)+" errorSelfCode:'75009'","./logs/v1/profile.txt");
                        res.json({error:true,msg:"75009"})
                        res.end();
                    })
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+" errorSelfCode:'75008'","./logs/v1/profile.txt");
                res.json({error:true,msg:"75008"})
                res.end();
            })
    }else {
        log.insertLog(" errorSelfCode:'75007'","./logs/v1/profile.txt");
        res.json({error:true,msg:"75007"})
        res.end();
    }
});

router.post('/update-bio',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.bio !== "undefined"){
        querys.update2vPromise('users_db',{where:{id:req.userId},values:{bio:req.body.bio}})
            .then(function (data) {
                res.json({error:false,msg:'0000'})
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+" errorSelfCode:'75102'","./logs/v1/profile.txt");
                res.json({error:true,msg:"75102"})
                res.end();
            })
    }else {
        log.insertLog(" errorSelfCode:'75101'","./logs/v1/profile.txt");
        res.json({error:true,msg:"75101"})
        res.end();
    }
});
router.post('/email-verify',checkUser.checkValidUser(), function(req, res, next) {
    querys.findByMultyNamePromise('users_db',{id:req.userId},['id','email','e_verify'])
        .then(function (userData) {
            if(userData[0].e_verify == '0'){
                var sendingToken =sha1(makeid(10)) +'-'+ sha1(makeid(10));
                let emailParams = {
                    backUrl : statics.MAIN_URL+'/profile/email-verification?t='+sendingToken,
                    // backUrl : http://192.168.2.191:3000/profile/email-verification?t=f5c23a0657541097867fbcc6f419c072fa1964d2-fafa6e2bb6e702cba6bdc01b576472b84a4312ee,
                    email : userData[0].email
                }
                querys.findByMultyNamePromise('verify_tokens',{user_id:req.userId},['*'])
                    .then(function (verTokData) {
                        if(verTokData.length === 0) {
                            querys.insert2vPromise('verify_tokens', {user_id: userData[0].id, v_token: sendingToken})
                                .then(function (data) {
                                    sendmail.sendMailVerification(emailParams)
                                    res.json({error: false})
                                })
                                .catch(function (error) {
                                    log.insertLog(JSON.stringify(error) + " errorSelfCode:'75105'", "./logs/v1/profile.txt");
                                    res.json({error: true, msg: "75105"})
                                    res.end();
                                })
                        }else {
                            querys.update2vPromise('verify_tokens',{where:{id:verTokData[0].id},values:{v_token:sendingToken}})
                                .then(function (updateData) {
                                    sendmail.sendMailVerification(emailParams)
                                    res.json({error: false})
                                })
                                .catch(function (error) {
                                    log.insertLog(JSON.stringify(error) + " errorSelfCode:'75107'","./logs/v1/profile.txt");
                                    res.json({error:true,msg:"75107"})
                                    res.end();
                                })
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error) + " errorSelfCode:'75106'","./logs/v1/profile.txt");
                        res.json({error:true,msg:"75106"})
                        res.end();
                    })


            }else{
                log.insertLog(" errorSelfCode:'75104'","./logs/v1/profile.txt");
                res.json({error:true,msg:"75104"})
                res.end();
            }

        })
        .catch(function (error) {
            log.insertLog(JSON.stringify(error) + " errorSelfCode:'75103'","./logs/v1/profile.txt");
            res.json({error:true,msg:"75103"})
            res.end();
        })

});

router.post('/e_verify/accept',checkUser.checkValidUser(),async function (req,res,next){
    console.log('req.body',req.body.token)
    console.log('req.userId',req.userId)

    try {
        const data = await querys.findByMultyNamePromise('verify_tokens',{user_id : req.userId,v_token : req.body.token},['*'])
        if(data.length !== 1){
            // error
        }

        await querys.update2vPromise('users_db',{where:{id: req.userId},values:{e_verify:'1'}})
        await querys.deletesPromise('verify_tokens',{user_id : req.userId,v_token : req.body.token})

        res.json({error:false})
        res.end();
    } catch (err) {
        res.json({error:true,msg:"76103"})
        res.end();
    }
})

router.post('/withdraw',checkUser.checkValidUser(), function(req, res, next) {
    if(
        typeof req.body.bank_name !== "undefined" &&
        typeof req.body.bank_account_number !== "undefined" &&
        typeof req.body.card_number !== "undefined" &&
        typeof req.body.amount !== "undefined"
    ){
        var userId = req.userId;
        querys.findByMultyNamePromise('user_money',{user_id:userId},['*'])
            .then(function (moneyData) {
                if(parseFloat(moneyData[0].money) > parseFloat(req.body.amount)){
                    var upgradeableMoney = parseFloat(moneyData[0].money) - parseFloat(req.body.amount);
                    querys.update2vPromise('user_money',{where:{id:moneyData[0].id},values:{money:upgradeableMoney.toFixed(2)}})
                        .then(function (updateData) {
                            let insertData = {
                                user_id : userId,
                                bank_name : req.body.bank_name,
                                bank_account_number : req.body.bank_account_number,
                                card_number : req.body.card_number,
                                amount : req.body.amount
                            }
                            querys.insert2vPromise('withdraws',insertData)
                                .then(function (data) {
                                    var noteParams = {
                                        user_id : userId,
                                        type:'14',
                                        view : "0",
                                    }
                                    var transactionParams = {
                                        user_id : userId,
                                        type:'3',
                                        way : "2",
                                        withdraw_id : data.insertId,
                                        money : parseFloat(req.body.amount).toFixed(2),
                                    }

                                    // var updateableMoney =

                                    Promise.all([
                                        notificationQuerys.addNotification(noteParams),
                                        transactionQuerys.addTransaction(transactionParams)
                                    ])
                                        .then(function () {
                                            res.json({error:false,msg:"0000"})
                                        })
                                        .catch(function (error) {
                                            log.insertLog(JSON.stringify(error) +" errorSelfCode:'75204'","./logs/v1/notification.txt");
                                            res.json({error:true,msg:"75205"});
                                            res.end();
                                        })
                                })
                                .catch(function (error) {
                                    log.insertLog(JSON.stringify(error) + " errorSelfCode:'75204'","./logs/v1/profile.txt");
                                    res.json({error:true,msg:"75204"})
                                    res.end();
                                })
                        })
                        .catch(function (error) {
                            log.insertLog(" errorSelfCode:'75206'","./logs/v1/profile.txt");
                            res.json({error:true,msg:"75206"})
                            res.end();
                        })
                }else{
                    var noteParams = {
                        user_id : userId,
                        type:'18',
                        view : "0",
                    }
                    notificationQuerys.addNotification(noteParams)
                        .then(function (data) {
                            log.insertLog(" errorSelfCode:'75203'","./logs/v1/profile.txt");
                            res.json({error:true,msg:"75203"})
                            res.end();
                        })
                        .catch(function (error) {
                            log.insertLog(" errorSelfCode:'75203-1'","./logs/v1/profile.txt");
                            res.json({error:true,msg:"75203-1"})
                            res.end();
                        })
                }
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error) + " errorSelfCode:'75202'","./logs/v1/profile.txt");
                res.json({error:true,msg:"75202"})
                res.end();
            })
    }else{
        log.insertLog(" errorSelfCode:'75201'","./logs/v1/profile.txt");
        res.json({error:true,msg:"75201"})
        res.end();
    }
});

router.post('/transaction-history',checkUser.checkValidUser(), function(req, res, next) {
    var userId = req.userId;
    console.log('userId',userId)
    profileQuerys.findUserTransactions(userId)
        .then(function (data){
            res.json({error:false,data:data,type:'1 - gaming 2 - deposit 3 - withdraw',way:" 0 - out 1 - in 2 - freeze"})
        })
        .catch(function (error){
            res.json({error:true})
        })
});

router.post('/change-password',checkUser.checkValidUser(), function(req, res, next) {
    const userId = req.userId;
    querys.findByMultyNamePromise('users_db', {id : userId, password : sha1(req.body.oldPassword)}, ['id'])
        .then((data)=>{
            if(data.length === 1){
                querys.update2vPromise('users_db',{where : {id : userId}, values : {password : sha1(req.body.newPassword)}})
                    .then((data)=>{
                        res.json({error:false})
                    })
                    .catch(function (error){
                        console.log('........error',error)
                        res.json({error:true})
                    })
            }else{
                res.json({error:true})
            }
        })
        .catch(function (error){
            console.log('........error',error)
            res.json({error:true})
        })
});


module.exports = router;


function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
