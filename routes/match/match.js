var express = require('express');
var router = express.Router();
var log = require("../../logs/log");
var challengeQuerys = require('../../model/querys/v1/challenge')
var querys = require('../../model/querys/querys');
var gamesQuerys = require('../../model/querys/v1/games')
var matchQuerys = require('../../model/querys/v1/match')
var notificationQuerys = require('../../model/querys/v1/notification')
var transactionQuerys = require('../../model/querys/v1/transaction')
var modeQuerys = require('../../model/querys/v1/mode')
var serverQuerys = require('../../model/querys/v1/server')
var consoleQuerys = require('../../model/querys/v1/console')
var statics = require('../../static');
var staticMethods = require('../../model/staticMethods');
var checkUser = require('../../object/auth/auth');
const {schedulerService} = require("../../service/scheduler.service");

router.post('/join',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.challengeId !== "undefined"){
        var challengeId = req.body.challengeId;
        querys.findByMultyNamePromise('challenge',{id:challengeId}, ['id','user_id','game_id','prize'])
            .then(function (challenge) {
                var challengeUserId = challenge[0].user_id;
                var gameId =  challenge[0].game_id;
                var creatorId = challenge[0].user_id;
                var joinerId = req.userId;
                var challengePrize = challenge[0].prize;
                matchQuerys.checkBusyByJoinerId(joinerId)
                // ***** check user is playing or no ****
                    .then(function (data) {
                        if(data.length  === 0){
                            querys.findByMultyNamePromise('game_usernames',{game_id : gameId, user_id : joinerId},['id'])
                                // ***** check joner has userName or no ****
                                .then(function (usernameFind) {
                                    if(usernameFind.length !== 0){
                                        let params = {
                                            challenge_id : challengeId,
                                            creator_id : creatorId,
                                            joiner_id : joinerId,
                                            game_id : gameId,
                                        }
                                        querys.findByMultyNamePromise('matches',params,['id'])
                                            // check uSer is joined to this challenge or not
                                            .then(function (data) {
                                                if(data.length  === 0){
                                                    querys.findByMultyNamePromise('user_money',{user_id : req.userId},['id','money'])
                                                        // check Joiner has enough money or not
                                                        .then(function (user_money) {
                                                            var joinerMoney = user_money[0].money;
                                                            // console.log('user_money',user_money)
                                                            // console.log('joinerMoney',joinerMoney)
                                                            // console.log('challengePrize',challengePrize)
                                                            if(parseFloat(joinerMoney) >= parseFloat(challengePrize)){
                                                                let matchPost = {
                                                                    challenge_id : challengeId,
                                                                    creator_id : creatorId,
                                                                    joiner_id : joinerId,
                                                                    joiner_accept : "1",
                                                                    game_id :  challenge[0].game_id
                                                                }
                                                                querys.insert2vPromise('matches',matchPost)
                                                                    .then(function (matches) {
                                                                        var params = {
                                                                            user_id : creatorId,
                                                                            s_user_id : joinerId,
                                                                            type:'4',
                                                                            challenge_id : challengeId,
                                                                            view : "0",
                                                                        }
                                                                        notificationQuerys.addNotification(params)
                                                                            .then(function (data) {
                                                                                res.json({error:false,msg:"0000"});
                                                                            })
                                                                            .catch(function (error) {
                                                                                log.insertLog(JSON.stringify(error) +" errorSelfCode:'66547'","./logs/v1/notification.txt");
                                                                                res.json({error:true,msg:"66547"});
                                                                                res.end();
                                                                            })
                                                                    })
                                                                    .catch(function (error) {
                                                                        log.insertLog(JSON.stringify(error) +" errorSelfCode:'58008'","./logs/v1/match.txt");
                                                                        res.json({error:true,msg:"58008"});
                                                                        res.end();
                                                                    })
                                                            }else {
                                                                log.insertLog(" errorSelfCode:'58183'","./logs/v1/match.txt");
                                                                res.json({error:true,msg:"58183" });
                                                                res.end();
                                                            }
                                                        })
                                                        .catch(function (error) {
                                                            log.insertLog(JSON.stringify(error) +" errorSelfCode:'58182'","./logs/v1/match.txt");
                                                            res.json({error:true,msg:"58182"});
                                                            res.end();
                                                        })
                                                }else{
                                                    log.insertLog(" errorSelfCode:'58103'","./logs/v1/match.txt");
                                                    res.json({error:true,msg:"58103"});
                                                    res.end();
                                                }
                                            })
                                            .catch(function (error) {
                                                log.insertLog(JSON.stringify(error) +" errorSelfCode:'58104'","./logs/v1/match.txt");
                                                res.json({error:true,msg:"58104"});
                                                res.end();
                                            })
                                    }else{
                                        log.insertLog(" errorSelfCode:'58129'","./logs/v1/match.txt");
                                        res.json({error:true,msg:"58129"});
                                        res.end();
                                    }

                                })
                                .catch(function (error) {
                                    log.insertLog(" errorSelfCode:'58128'","./logs/v1/match.txt");
                                    res.json({error:true,msg:"58128"});
                                    res.end();
                                })

                        }else {
                            log.insertLog(" errorSelfCode:'58108'","./logs/v1/match.txt");
                            res.json({error:true,msg:"58108"});
                            res.end();
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error) +" errorSelfCode:'58107'","./logs/v1/match.txt");
                        res.json({error:true,msg:"58107"});
                        res.end();
                    })
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error) +" errorSelfCode:'58007'","./logs/v1/match.txt");
                res.json({error:true,msg:"58007"});
                res.end();
            })

    }else {
        log.insertLog(" errorSelfCode:'58691'","./logs/v1/match.txt");
        res.json({error:true,msg:"58691"});
        res.end();
    }
});

router.post('/accept',checkUser.checkValidUser(), async function(req, res, next) {
    try {
        const healthCheck = await schedulerService.healthCheck();
        if (healthCheck.status !== 200) {
            throw new Error("2367148")
        }
        if(typeof req.body.matchesId !== "undefined") {
            let creatorId = req.userId;
            let matchesId = req.body.matchesId;
            // ***** check user is playing no ****
            matchQuerys.checkBusyByCreatorId(creatorId)
                // ***** check user is playing or no ****
                .then(function (user) {
                    if (user.length === 0) {
                        matchQuerys.findByMatchId(matchesId)
                            // get this match by matchID
                            .then(function (matchInfo) {
                                var joinerId = matchInfo[0].joiner_id;
                                matchQuerys.checkBusyByJoinerId(joinerId)
                                    // check Joiner busy or not
                                    .then(function (joinerBusyData) {
                                        if (joinerBusyData.length === 0) {
                                            querys.findByMultyNamePromise('challenge',{id:matchInfo[0].challenge_id}, ['prize'])
                                                // get this challenge info
                                                .then(function (challengeInfo) {
                                                    var challengePrize = challengeInfo[0].prize;
                                                    querys.findByMultyNamePromise('user_money',{user_id : creatorId},['id','money'])
                                                        // check Creator has enough money or not
                                                        .then(function (user_money) {
                                                            var creatorMoney = user_money[0].money;
                                                            var updateableUserId = creatorId;
                                                            if(parseFloat(creatorMoney) >= parseFloat(challengePrize)) {

                                                                let updateableMoney = (parseFloat(creatorMoney) - parseFloat(challengePrize)).toFixed(2);
                                                                querys.update2vPromise('user_money',{
                                                                    values:{money:updateableMoney},
                                                                    where:{user_id:updateableUserId}
                                                                })
                                                                    // update Creator money
                                                                    .then(function (updateUserMoneyData) {
                                                                        var trParams = {
                                                                            type : '1',
                                                                            way  : '2',
                                                                            matches_id  :matchesId,
                                                                            user_id : creatorId,
                                                                            money  :parseFloat(challengePrize).toFixed(2)
                                                                        }
                                                                        transactionQuerys.addTransaction(trParams)
                                                                            // freeze creator money in transactions
                                                                            .then(function () {
                                                                                matchQuerys.setAcceptFromCreator(matchesId)
                                                                                    .then(function (data) {
                                                                                        querys.update2vPromise('challenge',
                                                                                            {
                                                                                                where : {id : matchInfo[0].challenge_id},
                                                                                                values : {started : '1'}
                                                                                            }
                                                                                        )
                                                                                            .then(function () {
                                                                                                var params = [
                                                                                                    {
                                                                                                        user_id: matchInfo[0].joiner_id,
                                                                                                        s_user_id: matchInfo[0].creator_id,
                                                                                                        type: '0',
                                                                                                        challenge_id: matchInfo[0].challenge_id,
                                                                                                        view: "0",
                                                                                                    },
                                                                                                    {
                                                                                                        user_id: matchInfo[0].joiner_id,
                                                                                                        s_user_id: matchInfo[0].creator_id,
                                                                                                        type: '7',
                                                                                                        challenge_id: matchInfo[0].challenge_id,
                                                                                                        view: "0",
                                                                                                    },
                                                                                                    {
                                                                                                        user_id: matchInfo[0].creator_id,
                                                                                                        s_user_id: matchInfo[0].joiner_id,
                                                                                                        type: '7',
                                                                                                        challenge_id: matchInfo[0].challenge_id,
                                                                                                        view: "0",
                                                                                                    }
                                                                                                ];
                                                                                                var allQuerys = [];
                                                                                                for (var i = 0; i < params.length; i++) {
                                                                                                    allQuerys.push(notificationQuerys.addNotification(params[i]))
                                                                                                }
                                                                                                let deleteParams = {
                                                                                                    challenge_id: matchInfo[0].challenge_id,
                                                                                                    s_user_id: joinerId,
                                                                                                    type : '4'
                                                                                                };
                                                                                                // console.log('deleteParams',deleteParams)
                                                                                                allQuerys.push(notificationQuerys.deleteNotification(deleteParams))
                                                                                                allQuerys.push(matchQuerys.updateStartForTimer(matchesId))

                                                                                                Promise.all(allQuerys)
                                                                                                    .then(async function () {
                                                                                                        const minutesForCallback = 15;
                                                                                                        const scheduled = await schedulerService.create(
                                                                                                            matchesId,
                                                                                                            new Date(new Date().getTime() + minutesForCallback*60000),
                                                                                                            statics.API_URL + "/v1/scheduler-callback/"+matchesId
                                                                                                        )
                                                                                                        if(!scheduled) {
                                                                                                            throw new Error('5754658768')
                                                                                                        }
                                                                                                        await querys.update2vPromise('matches', {
                                                                                                            where: { id: matchesId },
                                                                                                            values: { scheduleId: scheduled.data.scheduleId }
                                                                                                        })
                                                                                                        res.json({error: false, msg: "0000"});
                                                                                                    })
                                                                                                    .catch(function (error) {
                                                                                                        log.insertLog(JSON.stringify(error) + " errorSelfCode:'66547'", "./logs/v1/notification.txt");
                                                                                                        res.json({error: true, msg: "66547"});
                                                                                                        res.end();
                                                                                                    })
                                                                                            })
                                                                                            .catch(function (error) {
                                                                                                log.insertLog(JSON.stringify(error) + " errorSelfCode:'58247'", "./logs/v1/match.txt");
                                                                                                res.json({error: true, msg: "58247"});
                                                                                                res.end();
                                                                                            })
                                                                                    })
                                                                                    .catch(function (error) {
                                                                                        log.insertLog(JSON.stringify(error) + " errorSelfCode:'58246'", "./logs/v1/match.txt");
                                                                                        res.json({error: true, msg: "58246"});
                                                                                        res.end();
                                                                                    })
                                                                            })
                                                                            .catch(function (error) {
                                                                                log.insertLog(JSON.stringify(error) + ", errorSelfCode:'58417'","./logs/v1/match.txt");
                                                                                res.json({error:true, errCode :"58417" });
                                                                                res.end();
                                                                            })
                                                                    })
                                                                    .catch(function (error) {
                                                                        log.insertLog(JSON.stringify(error) + ", errorSelfCode:'58416'","./logs/v1/match.txt");
                                                                        res.json({error:true, errCode :"58416" });
                                                                        res.end();
                                                                    })
                                                            }else{
                                                                log.insertLog(" errorSelfCode:'58414'","./logs/v1/match.txt");
                                                                res.json({error:true,msg:"No enough money", errCode :"58414" });
                                                                res.end();
                                                            }
                                                        })
                                                        .catch(function (error) {
                                                            log.insertLog(JSON.stringify(error) + " errorSelfCode:'58413'", "./logs/v1/challenge.txt");
                                                            res.json({error: true, msg: "58413"});
                                                            res.end();
                                                        })
                                                })
                                                .catch(function (error) {
                                                    log.insertLog(JSON.stringify(error) + " errorSelfCode:'58412'", "./logs/v1/challenge.txt");
                                                    res.json({error: true, msg: "58412"});
                                                    res.end();
                                                })

                                        }else{
                                            log.insertLog(JSON.stringify(error) + " errorSelfCode:'58279'", "./logs/v1/match.txt");
                                            res.json({error: true, msg: "58279"});
                                            res.end();
                                        }
                                    })
                                    .catch(function (error) {
                                        log.insertLog(JSON.stringify(error) + " errorSelfCode:'58278'", "./logs/v1/match.txt");
                                        res.json({error: true, msg: "58278"});
                                        res.end();
                                    })
                            })
                            .catch(function (error) {
                                log.insertLog(JSON.stringify(error) + " errorSelfCode:'58241'", "./logs/v1/match.txt");
                                res.json({error: true, msg: "58241"});
                                res.end();
                            })
                    } else {
                        log.insertLog(" errorSelfCode:'58249'", "./logs/v1/match.txt");
                        res.json({error: true, msg: "58249"});
                        res.end();
                    }
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error) + " errorSelfCode:'58248'", "./logs/v1/match.txt");
                    res.json({error: true, msg: "58248"});
                    res.end();
                })
        }else{
            log.insertLog("errorSelfCode:'58200'", "./logs/v1/match.txt");
            res.json({error: true, msg: "58200"});
            res.end();
        }
    } catch (err) {
        res.json(err);
        res.end();
    }

});

router.post('/start',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.matchesId !== "undefined") {
        let userId = req.userId;
        let matchesId = req.body.matchesId;
        matchQuerys.findByMatchId(matchesId)
            .then(function (data) {
                // console.log('data', data)
                var updateField = "";
                if(data[0].creator_id == userId){
                    updateField = 'creator_start';
                }else if(data[0].joiner_id == userId) {
                    updateField = 'joiner_start';
                }
                matchQuerys.updateStart(updateField,matchesId)
                    .then(function (updateResult) {
                        matchQuerys.findByMatchId(matchesId)
                            .then(function (secondFindResult) {
                                // console.log('secondFindResult', secondFindResult)
                                var challengeId = data[0].challenge_id;
                                var joinerId = data[0].joiner_id;
                                if(updateField === 'joiner_start'){

                                    Promise.all([
                                        querys.findByMultyNamePromise('challenge',{id:challengeId}, ['prize']),
                                        querys.findByMultyNamePromise('user_money',{user_id : joinerId},['id','money'])
                                    ])
                                        .then(function ([challengeData,userMoneyData]) {
                                            var challengePrize = challengeData[0].prize;
                                            var joinerMoney = userMoneyData[0].money;
                                            if(parseFloat(joinerMoney) >= parseFloat(challengePrize)) {

                                                let updateableMoney = (parseFloat(joinerMoney) - parseFloat(challengePrize)).toFixed(2);
                                                querys.update2vPromise('user_money', {
                                                    values: {money: updateableMoney},
                                                    where: {user_id: joinerId}
                                                })
                                                    .then(function () {
                                                        var trParams = {
                                                            type : '1',
                                                            way  : '2',
                                                            user_id : joinerId,
                                                            matches_id  :matchesId,
                                                            money  :parseFloat(challengePrize).toFixed(2)
                                                        }
                                                        transactionQuerys.addTransaction(trParams)
                                                            .then(function () {
                                                                // next()
                                                            })
                                                            .catch(function (error) {
                                                                log.insertLog(JSON.stringify(error) +",errorSelfCode:'59813'", "./logs/v1/match.txt");
                                                                res.json({error: true, msg: "59813"});
                                                                res.end();
                                                            })
                                                    })
                                                    .catch(function (error) {
                                                        log.insertLog(JSON.stringify(error) +",errorSelfCode:'59812'", "./logs/v1/match.txt");
                                                        res.json({error: true, msg: "59812"});
                                                        res.end();
                                                    })
                                            }else{
                                                log.insertLog(",errorSelfCode:'59811'", "./logs/v1/match.txt");
                                                res.json({error: true, msg: "59811"});
                                                res.end();
                                            }
                                        })
                                        .catch(function (error) {
                                            log.insertLog(JSON.stringify(error) + ",errorSelfCode:'59810'", "./logs/v1/match.txt");
                                            res.json({error: true, msg: "59810"});
                                            res.end();
                                        })
                                }

                                var allNote = []
                                if(updateField === 'creator_start'){
                                    notificationQuerys.addNotification({
                                        user_id : data[0].creator_id,
                                        s_user_id :  data[0].joiner_id,
                                        type : '19',
                                        challenge_id : challengeId
                                    })
                                    allNote.push(notificationQuerys.deleteNotification({
                                        user_id : data[0].creator_id,
                                        s_user_id : data[0].joiner_id,
                                        type : '7',
                                        challenge_id : challengeId
                                    }))
                                }
                                if(updateField === 'joiner_start'){
                                    notificationQuerys.addNotification({
                                        user_id : data[0].joiner_id,
                                        s_user_id : data[0].creator_id ,
                                        type : '19',
                                        challenge_id : challengeId
                                    })
                                    allNote.push(notificationQuerys.deleteNotification({
                                        user_id : data[0].joiner_id,
                                        type : '0',
                                        challenge_id : challengeId
                                    }))
                                    allNote.push(notificationQuerys.deleteNotification({
                                        user_id : data[0].joiner_id,
                                        type : '7',
                                        challenge_id : challengeId
                                    }))
                                }
                                Promise.all(allNote)
                                    .then(async ()=>{
                                        // console.log('secondFindResult',secondFindResult)
                                        if(secondFindResult[0].creator_start === '1' && secondFindResult[0].joiner_start === '1'){
                                            /**
                                             * 1. delete last schedule for both start - 15 minutes
                                             * 2. set new schedule timer for 30 minutes for game playinge
                                             */

                                            const destroyedScheduleJob = await schedulerService.destroy(
                                                secondFindResult[0].scheduleId
                                            )
                                            if(!destroyedScheduleJob) {
                                                throw new Error('57546586543534')
                                            }

                                            const minutesForCallback = 30;
                                            const scheduled = await schedulerService.create(
                                                matchesId,
                                                new Date(new Date().getTime() + minutesForCallback*60000),
                                                statics.API_URL + "/v1/scheduler-callback/"+matchesId
                                            )
                                            if(!scheduled) {
                                                throw new Error('57546543534')
                                            }
                                            await querys.update2vPromise('matches', {
                                                where: { id: matchesId },
                                                values: { scheduleId: scheduled.data.scheduleId }
                                            })

                                            //end scheduler logic

                                            let paramsF = {user_id : secondFindResult[0].creator_id, type:'8', view : "0", challenge_id : challengeId}
                                            let paramsS = {user_id : secondFindResult[0].joiner_id, type:'8', view : "0", challenge_id : challengeId}
                                            let paramsDelF = {user_id : secondFindResult[0].creator_id, type:'19'}
                                            let paramsDelS = {user_id : secondFindResult[0].joiner_id, type:'19'}

                                            Promise.all([
                                                notificationQuerys.addNotification(paramsF),
                                                notificationQuerys.addNotification(paramsS),
                                                notificationQuerys.deleteNotification(paramsDelF),
                                                notificationQuerys.deleteNotification(paramsDelS)
                                                ]
                                            )
                                                .then(function () {
                                                    // console.log('::::::::::')
                                                    res.json({error:false,msg:"0000"});
                                                    res.end();
                                                })
                                                .catch(function (error) {
                                                    log.insertLog(JSON.stringify(error) +" errorSelfCode:'66547'","./logs/v1/notification.txt");
                                                    res.json({error:true,msg:"66547"});
                                                    res.end();
                                                })
                                        }else{
                                            // querys.update2vPromise('matches',{ where : { id:matchesId },values : { start_time : new Date().toISOString().slice(0, 19).replace('T', ' ') }})
                                            matchQuerys.updateStartForTimer(matchesId)
                                                .then(function (data) {
                                                    res.json({error:false,msg:"0000"});
                                                    res.end();
                                                })
                                                .catch(function (error) {
                                                    log.insertLog(JSON.stringify(error) +",errorSelfCode:'59820'", "./logs/v1/match.txt");
                                                    res.json({error: true, msg: "59820"});
                                                    res.end();
                                                })
                                        }
                                    })
                                    .catch(()=>{
                                        log.insertLog(JSON.stringify(error) +",errorSelfCode:'59817'", "./logs/v1/match.txt");
                                        res.json({error: true, msg: "59817"});
                                        res.end();
                                    })


                            })
                            .catch(function (error) {
                                log.insertLog(JSON.stringify(error) + ",errorSelfCode:'58115'", "./logs/v1/match.txt");
                                res.json({error: true, msg: "58115"});
                                res.end();
                            })
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error) + ",errorSelfCode:'58114'", "./logs/v1/match.txt");
                        res.json({error: true, msg: "58114"});
                        res.end();
                    })
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error) + ",errorSelfCode:'58113'", "./logs/v1/match.txt");
                res.json({error: true, msg: "58113"});
                res.end();
            })
    }else{
        log.insertLog("errorSelfCode:'58111'", "./logs/v1/match.txt");
        res.json({error: true, msg: "58111"});
        res.end();
    }
});

router.post('/decline',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.matchesId !== "undefined") {
        querys.findByMultyNamePromise('matches',{id:req.body.matchesId},['*'])
            .then(function (matchData) {
                if(matchData[0].creator_start === '1' && matchData[0].joiner_start === '1') {
                    log.insertLog("errorSelfCode:'59321'", "./logs/v1/match.txt");
                    res.json({error: true, msg: "59321"});
                    res.end();
                } else {
                    var userType = "";
                    if(matchData[0].creator_id == req.userId){
                        userType = 'creator';
                    }else if(matchData[0].joiner_id == req.userId){
                        userType = 'joiner';
                    }
                    var creatorId = matchData[0].creator_id;
                    if(userType === 'creator'){
                        if(matchData[0].creator_accept === '0'){
                            Promise.all([
                                querys.deletesPromise('matches',{id:req.body.matchesId}),
                                querys.deletesPromise('notifications',{user_id:creatorId,s_user_id:creatorId,challenge_id:matchData[0].challenge_id})
                            ])
                                .then(function ([data]) {
                                    res.json({error: false});
                                })
                                .catch(function () {
                                    log.insertLog("errorSelfCode:'59003'", "./logs/v1/match.txt");
                                    res.json({error: true, msg: "59003"});
                                    res.end();
                                })

                        }else if(matchData[0].creator_accept === '1'){
                            if(matchData[0].joiner_start === '1'){
                                querys.findByMultyNamePromise('transactions_history',{user_id:matchData[0].joiner_id,matches_id:req.body.matchesId,type:'1'},['*'])
                                    .then(function (transactionData) {
                                        var plusMoney = parseFloat(transactionData[0].money);
                                        querys.findByMultyNamePromise('user_money',{user_id:matchData[0].joiner_id},['*'])
                                            .then(function (userMoneyData) {
                                                var userMoney = parseFloat(userMoneyData[0].money);
                                                var updateMoney = (plusMoney + userMoney).toFixed(2)
                                                querys.update2vPromise('user_money',{
                                                    where : {user_id : matchData[0].joiner_id},
                                                    values : {money:updateMoney}
                                                })
                                                    .then(function (result) {
                                                        next();
                                                    })
                                                    .catch(function (error) {
                                                        console.log('error', error)
                                                    })
                                            })
                                            .catch(function (error) {
                                                console.log('error', error)
                                            })
                                    })
                                    .catch(function (error) {
                                        console.log('error', error)
                                    })
                            }
                            querys.findByMultyNamePromise('transactions_history',{user_id:matchData[0].creator_id,matches_id:req.body.matchesId,type:'1'},['*'])
                                .then(function (transactionData) {
                                    var plusMoney = parseFloat(transactionData[0].money);
                                    querys.findByMultyNamePromise('user_money',{user_id:matchData[0].creator_id},['*'])
                                        .then(function (userMoneyData) {
                                            var userMoney = parseFloat(userMoneyData[0].money);
                                            var updateMoney = (plusMoney + userMoney).toFixed(2)
                                            querys.update2vPromise('user_money',{
                                                where : {user_id : matchData[0].creator_id},
                                                values : {money:updateMoney}
                                            })
                                                .then(function (result) {
                                                    Promise.all([
                                                        querys.deletesPromise('matches',{id:req.body.matchesId}),
                                                        querys.deletesPromise('notifications',{user_id:creatorId,s_user_id:creatorId,challenge_id:matchData[0].challenge_id})
                                                    ])
                                                        .then(function ([data]) {
                                                            var noteParamsF = {
                                                                user_id : matchData[0].creator_id,
                                                                type : '5'
                                                            }
                                                            var noteParamsS = {
                                                                user_id : matchData[0].joiner_id,
                                                                type : '5'
                                                            }
                                                            Promise.all([
                                                                notificationQuerys.addNotification(noteParamsF),
                                                                notificationQuerys.addNotification(noteParamsS),
                                                                querys.update2vPromise('challenge',{
                                                                    where : { id : matchData[0].challenge_id },
                                                                    values : {started: '0'}
                                                                }),
                                                                schedulerService.destroy(matchData[0].scheduleId)
                                                            ])
                                                                .then(function () {
                                                                    res.json({error: false});
                                                                })
                                                                .catch(function () {
                                                                    log.insertLog("errorSelfCode:'59004'", "./logs/v1/match.txt");
                                                                    res.json({error: true, msg: "59004"});
                                                                    res.end();
                                                                })
                                                        })
                                                        .catch(function () {
                                                            log.insertLog("errorSelfCode:'59003'", "./logs/v1/match.txt");
                                                            res.json({error: true, msg: "59003"});
                                                            res.end();
                                                        })
                                                })
                                                .catch(function (error) {

                                                })
                                        })
                                        .catch(function (error) {

                                        })
                                })
                                .catch(function (error) {

                                })
                        }
                    }else if(userType === 'joiner'){
                        if(matchData[0].creator_accept === '1' || matchData[0].creator_start === '1'){
                            querys.findByMultyNamePromise('transactions_history',{user_id:matchData[0].creator_id,matches_id:req.body.matchesId,type:'1'},['*'])
                                .then(function (transactionData) {
                                    var plusMoney = parseFloat(transactionData[0].money);
                                    querys.findByMultyNamePromise('user_money',{user_id:matchData[0].creator_id},['*'])
                                        .then(function (userMoneyData) {
                                            var userMoney = parseFloat(userMoneyData[0].money);
                                            var updateMoney = (plusMoney + userMoney).toFixed(2)
                                            querys.update2vPromise('user_money',{
                                                where : {user_id : matchData[0].creator_id},
                                                values : {money:updateMoney}
                                            })
                                                .then(function (result) {
                                                    next();
                                                })
                                                .catch(function (error) {
                                                    console.log('error',error)
                                                })
                                        })
                                        .catch(function (error) {
                                            console.log('error',error)
                                        })
                                })
                                .catch(function (error) {
                                    console.log('error',error)
                                })
                        }
                        if(matchData[0].joiner_start === '1') {
                            querys.findByMultyNamePromise('transactions_history', {
                                user_id: matchData[0].joiner_id,
                                matches_id: req.body.matchesId,
                                type: '1'
                            }, ['*'])
                                .then(function (transactionData) {
                                    var plusMoney = parseFloat(transactionData[0].money);
                                    querys.findByMultyNamePromise('user_money', {user_id: matchData[0].joiner_id}, ['*'])
                                        .then(function (userMoneyData) {
                                            var userMoney = parseFloat(userMoneyData[0].money);
                                            var updateMoney = (plusMoney + userMoney).toFixed(2)
                                            querys.update2vPromise('user_money', {
                                                where: {user_id: matchData[0].joiner_id},
                                                values: {money: updateMoney}
                                            })
                                                .then(function (result) {
                                                    Promise.all([
                                                        querys.deletesPromise('matches', {id: req.body.matchesId}),
                                                        querys.deletesPromise('notifications', {
                                                            user_id: creatorId,
                                                            s_user_id: creatorId,
                                                            challenge_id: matchData[0].challenge_id
                                                        }),
                                                        querys.update2vPromise('challenge',{
                                                            where : { id : matchData[0].challenge_id },
                                                            values : {started: '0'}
                                                        }),
                                                        schedulerService.destroy(matchData[0].scheduleId)
                                                    ])
                                                        .then(function ([data]) {
                                                            var noteParamsF = {
                                                                user_id: matchData[0].creator_id,
                                                                type: '5'
                                                            }
                                                            var noteParamsS = {
                                                                user_id: matchData[0].joiner_id,
                                                                type: '5'
                                                            }
                                                            Promise.all([
                                                                notificationQuerys.addNotification(noteParamsF),
                                                                notificationQuerys.addNotification(noteParamsS)
                                                            ])

                                                                .then(function () {
                                                                    res.json({error: false});
                                                                })
                                                                .catch(function () {
                                                                    log.insertLog("errorSelfCode:'59004'", "./logs/v1/match.txt");
                                                                    res.json({error: true, msg: "59004"});
                                                                    res.end();
                                                                })
                                                        })
                                                        .catch(function () {
                                                            log.insertLog("errorSelfCode:'59003'", "./logs/v1/match.txt");
                                                            res.json({error: true, msg: "59003"});
                                                            res.end();
                                                        })
                                                })
                                                .catch(function (error) {

                                                })
                                        })
                                        .catch(function (error) {

                                        })
                                })
                                .catch(function (error) {

                                })
                        }
                    }
                }
            })
            .catch(function (error) {

            })

    }else{
        log.insertLog("errorSelfCode:'59001'", "./logs/v1/match.txt");
        res.json({error: true, msg: "59001"});
        res.end();
    }
});

router.post('/get-this',checkUser.checkValidUser(), async function(req, res, next) {
    // console.log('req.body.matchesId',req.body.matchesId)
    if(typeof req.body.matchesId !== "undefined") {
        querys.findByMultyNamePromise('matches',{id:req.body.matchesId},['*'])
            .then(async function (matchData) {
                let scheduledDate = null;
                if(matchData[0].creator_result === '0' || matchData[0].joiner_result === '0') {
                    const scheduled = await schedulerService.get(matchData[0].scheduleId);
                    if (!scheduled) {
                        throw new Error('57546587232')
                    }
                    scheduledDate = scheduled.data.date
                }
                let selectUserId;
                let userType = "";
                if(matchData[0].creator_id == req.userId){
                    selectUserId = matchData[0].joiner_id;
                    userType = 'creator'
                }
                if(matchData[0].joiner_id == req.userId){
                    selectUserId = matchData[0].creator_id;
                    userType = 'joiner'
                }
                matchQuerys.getOponentInfo(selectUserId,req.body.matchesId,matchData[0].game_id)
                    .then(async function (data) {
                        let returnData = {};
                        data[0].description = data[0].description.replace(/(?:\r\n|\r|\n)/g, '<br>')
                        data[0].console_image = statics.API_URL+'/images/consoles/'+data[0].console_image;
                        returnData.action_status = checkStatus(userType,matchData[0])
                        const resData = staticMethods.getWinnerPrize(data)
                        returnData.opponent_info = resData[0]
                        if(returnData.action_status.option === '2' || returnData.action_status.option === '3' || returnData.action_status.option === '4'){
                            returnData.opponent_info.distance = Math.floor((new Date(scheduledDate).getTime() - new Date().getTime()) / 1000)
                            res.json({error: false, data: returnData});
                        }else{
                            res.json({error: false, data: returnData});
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error)+ "errorSelfCode:'59053'", "./logs/v1/match.txt");
                        res.json({error: true, msg: "59053"});
                        res.end();
                    })
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+ "errorSelfCode:'59052'", "./logs/v1/match.txt");
                res.json({error: true, msg: "59052"});
                res.end();
            })
    }else{
        log.insertLog("errorSelfCode:'59051'", "./logs/v1/match.txt");
        res.json({error: true, msg: "59051"});
        res.end();
    }
});

router.post('/check-winner',checkUser.checkValidUser(), async function(req, res, next) {
    try {
        const matches = await querys.findByMultyNamePromise('matches',{id:req.body.matchesId},['*'])
        if(matches.length === 0) {
            throw new Error('43534543543543')
        }
        const match = matches[0];
        if(match.winner_id === null) {
            await actionCreator(match)
            await actionJoiner(match)
        } else {

        }

    } catch (errCode) {
        log.insertLog(JSON.stringify(error)+ "errorSelfCode:'59070'", "./logs/v1/match.txt");
        res.json({error: true, msg: errCode});
        res.end();
    }
}); // no usage

router.post('/set-result',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.matchesId !== "undefined" && typeof req.body.result !== "undefined") {
        querys.findByMultyNamePromise('matches',{id:req.body.matchesId},['*'])
            .then(function (matchData) {
                var updateValueObj;
                var userFiled,userFiledResult,opponentResult;
                if(matchData[0].creator_id == req.userId){
                    opponentResult = 'joiner_result';
                    userFiled = 'creator'
                    userFiledResult = 'creator_result'
                    updateValueObj = {creator_result : req.body.result}
                }
                if(matchData[0].joiner_id == req.userId){
                    opponentResult = 'creator_result';
                    userFiled = 'joiner'
                    userFiledResult = 'joiner_result';
                    updateValueObj = {joiner_result : req.body.result}
                }
                if(matchData[0][userFiledResult] == '0'){
                    // if(matchData[0][opponentResult] !== req.body.result || matchData[0][opponentResult] == '0'){
                    if(matchData[0][opponentResult] !== req.body.result){
                        querys.update2vPromise('matches',{where:{id:req.body.matchesId},values:updateValueObj})
                            .then(function (data){
                                matchQuerys.findMatchWithProperties( req.body.matchesId)
                                    .then(function (updatedMatch){
                                        console.log('updatedMatch',updatedMatch)
                                        if(updatedMatch[0].creator_result !== '0' && updatedMatch[0].joiner_result !== '0'){
                                            var winnerId,looserId;

                                            // var challengePrize = updatedMatch[0].prize.toFixed(2)
                                            var matchPrize = parseFloat(updatedMatch[0].prize) * 2;
                                            if(updatedMatch[0].creator_result == '1'){
                                                winnerId = updatedMatch[0].creator_id
                                                looserId = updatedMatch[0].joiner_id
                                            }else if(updatedMatch[0].joiner_result == '1'){
                                                winnerId = updatedMatch[0].joiner_id
                                                looserId = updatedMatch[0].creator_id
                                            }else{
                                                log.insertLog( "errorSelfCode:'53009'", "./logs/v1/match.txt");
                                                res.json({error: true, msg: "53009"});
                                                res.end();
                                            }

                                            var ourMoney = (matchPrize * statics.SITE_PERCENT / 100).toFixed(2);
                                            var winnerMoney = (matchPrize - ourMoney).toFixed(2);
                                            querys.findByMultyNamePromise('user_money',{user_id:winnerId},['*'])
                                                .then(function (userMoney){
                                                    var userNewMoney = parseFloat(userMoney[0].money) + parseFloat(winnerMoney);
                                                    Promise.all([
                                                        querys.insert2vPromise('self_earnings',{
                                                            match_id:parseInt(updatedMatch[0].id),
                                                            site_percent: statics.SITE_PERCENT,
                                                            money: ourMoney
                                                        }),
                                                        querys.update2vPromise('challenge',{where:{id:updatedMatch[0].challenge_id},values:{started:'2'}}),
                                                        querys.update2vPromise('matches',{where:{id:updatedMatch[0].id},values:{winner_id:winnerId}}),
                                                        querys.update2vPromise('user_money',{where:{user_id:winnerId},values:{money:userNewMoney}}),
                                                        querys.update2vPromise('transactions_history',{where:{user_id:winnerId,matches_id:updatedMatch[0].id,way:'2'},values:{way:'0'}}),
                                                        querys.update2vPromise('transactions_history',{where:{user_id:looserId,matches_id:updatedMatch[0].id,way:'2'},values:{way:'0'}}),
                                                        querys.insert2vPromise('transactions_history',{user_id:winnerId,type:'1',way:'1',matches_id:updatedMatch[0].id,money:winnerMoney}),
                                                        notificationQuerys.addNotification({user_id : winnerId,type:'9', view : "0"}),
                                                        notificationQuerys.addNotification({user_id : winnerId,type:'13', view : "0",money:winnerMoney}),
                                                        notificationQuerys.addNotification({user_id : looserId,type:'17', view : "0"}),
                                                        querys.insert2vPromise('transactions_history',{user_id:'0',type:'1',way:'1',matches_id:updatedMatch[0].id,money:ourMoney}),
                                                        schedulerService.destroy(updatedMatch[0].scheduleId)
                                                    ])
                                                        .then(function (datas){
                                                            res.json({error:false})
                                                        })
                                                        .catch(function (error){
                                                            log.insertLog(JSON.stringify(error)+ "errorSelfCode:'53008'", "./logs/v1/match.txt");
                                                            res.json({error: true, msg: "53008"});
                                                            res.end();
                                                        })
                                                })
                                                .catch(function (error){
                                                    log.insertLog(JSON.stringify(error)+ "errorSelfCode:'53007'", "./logs/v1/match.txt");
                                                    res.json({error: true, msg: "53007"});
                                                    res.end();
                                                })
                                        }else{
                                            res.json({error:false})
                                        }
                                    })
                                    .catch(function (error){
                                        log.insertLog(JSON.stringify(error)+ "errorSelfCode:'53006'", "./logs/v1/match.txt");
                                        res.json({error: true, msg: "53006"});
                                        res.end();
                                    })

                            })
                            .catch(function (error){
                                log.insertLog(JSON.stringify(error)+ "errorSelfCode:'53003'", "./logs/v1/match.txt");
                                res.json({error: true, msg: "53003"});
                                res.end();
                            })
                    }else{
                        log.insertLog("errorSelfCode:'5353005005'", "./logs/v1/match.txt");
                        res.json({error: true, msg: "53005"});
                        res.end();
                    }
                }else{
                    log.insertLog("errorSelfCode:'53004'", "./logs/v1/match.txt");
                    res.json({error: true, msg: "53004"});
                    res.end();
                }

            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error)+ "errorSelfCode:'53002'", "./logs/v1/match.txt");
                res.json({error: true, msg: "53002"});
                res.end();
            })
    }else{
        log.insertLog("errorSelfCode:'53001'", "./logs/v1/match.txt");
        res.json({error: true, msg: "53001"});
        res.end();
    }
});

router.post('/dispute-add',checkUser.checkValidUser(), function(req, res, next) {
    if(
        typeof req.body.matchesId !== "undefined" &&
        typeof req.body.message !== "undefined" &&
        typeof req.body.description !== "undefined" &&
        typeof req.body.fileName !== "undefined"
    ){
        var insertPost = {
            matches_id : req.body.matchesId,
            user_id : req.userId,
            filename : req.body.fileName,
            message : req.body.message,
            description : req.body.description
        }
        querys.insert2vPromise('matches_dispute',insertPost)
            .then(function (data){
                var params = {
                    user_id : req.userId,
                    type:'11',
                    view : "0",
                }
                notificationQuerys.addNotification(params)
                    .then(function (data) {
                        res.json({error:false});
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error) +" errorSelfCode:'66547'","./logs/v1/notification.txt");
                        res.json({error:true,msg:"66547"});
                        res.end();
                    })
                res.json({error:false})
            })
            .catch(function (error){
                log.insertLog(" errorSelfCode:'75302'","./logs/v1/profile.txt");
                res.json({error:true,msg:"75302"})
                res.end();
            })
    }else{
        log.insertLog(" errorSelfCode:'75301'","./logs/v1/profile.txt");
        res.json({error:true,msg:"75301"})
        res.end();
    }
});

router.post('/get-my-history',checkUser.checkValidUser(), function(req, res, next) {
    console.log('req.userId',req.userId)
    matchQuerys.findUserHistory(req.userId)
        .then(function (data){
            console.log('data',data)
            for(var i = 0; i < data.length; i++){
                data[i].win = false
                if(data[i].winner_id == req.userId){
                    data[i].win = true
                }
                data[i].console_img = statics.API_URL+'/images/consoles/'+data[i].console_image;
                delete  data[i].console_image;
                data[i].winner_priz = (2 * parseFloat(data[i].prize)) - ((2 * parseFloat(data[i].prize))*statics.SITE_PERCENT/100);

            }
            res.json({error:false,data:data})
        })
        .catch(function (error){
            res.json({error:true})
        })
});

module.exports = router;



function checkStatus(userType,data){
    console.log('qwewqeqwewq')
    var status = {
        option : null,
        winningwinning : null
    };
    if (userType === 'creator') {
            if (data.creator_accept === '0') {
                status.option = '1'//'accept button or decline button'
            } else if (data.creator_start === '1' && data.joiner_start === '1') {
                status.option = '2' //'the match is started'
                if (data.creator_result === '0' && data.joiner_result === '0') {
                    status.winning = '1'//'show won and lost buttons'
                } else if (data.joiner_result === '0' && data.creator_result === '1') {
                    status.winning = '4'//'waiting for oponent'
                } else if (data.joiner_result === '0' && data.creator_result === '2') {
                    status.winning = '4'//'waiting for oponent'
                } else if (data.joiner_result === '1' && data.creator_result === '2') {
                    status.winning = '5'//you lost this game
                } else if (data.joiner_result === '2' && data.creator_result === '1') {
                    status.winning = '6'//'you win this game
                } else if (data.joiner_result === '1') {
                    status.winning = '2'//'show lost button'
                } else if (data.joiner_result === '2') {
                    status.winning = '3'//'show won button'
                }
            } else if (data.creator_start === '1' && data.joiner_start === '0') {
                status.option = '4'//'waiting for oponent to start'
            } else if (data.creator_accept === '1') {
                status.option = '3'//'start button'
            }
        }

    if (userType === 'joiner') {
            if (data.joiner_accept === '1' && data.creator_accept === '0') {
                status.option = '5'//'waiting for oponent to accept'
            } else if (data.joiner_start === '0') {
                status.option = '3'//start button'
            } else if (data.joiner_start === '1' && data.creator_start === '0') {
                status.option = '4'//'waiting for oponent to start'
            } else if (data.joiner_start === '1' && data.creator_start === '1') {
                status.option = '2'//'the match is started'
                if (data.creator_result === '0' && data.joiner_result === '0') {
                    status.winning = '1'//'show won and lost buttons'
                } else if (data.creator_result === '0' && data.joiner_result === '1') {
                    status.winning = '4'//'waiting for oponent'
                } else if (data.creator_result === '0' && data.joiner_result === '2') {
                    status.winning = '4'//'waiting for oponent'
                } else if (data.creator_result === '1' && data.joiner_result === '2') {
                    status.winning = '5'//you lost this game
                } else if (data.creator_result === '2' && data.joiner_result === '1') {
                    status.winning = '6'//you win this game
                } else if (data.creator_result === '2') {
                    status.winning = '3'//'show won button'
                } else if (data.creator_result === '1') {
                    status.winning = '2'//'show lost button'
                }
            }
        }
    return status;
}


actionCreator = (matchInfo) => {
    return new Promise((resolve, reject) => {
        console.log('matchInfo',matchInfo)

        querys.findByMultyNamePromise('transactions_history',{user_id:matchInfo.creator_id,matches_id:matchInfo.id,type:'1'},['*'])
            .then((transactionData)=>{
                console.log('transactionData',transactionData)
                var plusMoney = parseFloat(transactionData[0].money);
                querys.findByMultyNamePromise('user_money',{user_id:matchInfo.creator_id},['*'])
                    .then(function (userMoneyData) {
                        var userMoney = parseFloat(userMoneyData[0].money);
                        var updateMoney = (plusMoney + userMoney).toFixed(2)
                        querys.update2vPromise('user_money',{
                            where : {user_id : matchInfo.creator_id},
                            values : {money:updateMoney}
                        })
                            .then(function (result) {
                                Promise.all([
                                    querys.update2vPromise('matches',{
                                        where : {id : matchInfo.id},
                                        values : {
                                            winner_id:0,
                                            creator_start:'1',
                                            joiner_start:'1',
                                            creator_result:'0',
                                            joiner_result:'0'
                                        }
                                    }),
                                    querys.insert2vPromise('transactions_history',{
                                        user_id: matchInfo.creator_id,
                                        type:'1',
                                        way:'1',
                                        matches_id:matchInfo.id,
                                        money:plusMoney
                                    }),
                                ])
                                    .then(()=>{
                                        resolve({error:false})
                                    })
                                    .catch((error)=>{
                                        log.insertLog(JSON.stringify(error) +" errorSelfCode:'59083'","./logs/v1/match.txt");
                                        resolve({error:true,msg : '59083'})
                                    })
                            })
                            .catch(function (error) {
                                log.insertLog(JSON.stringify(error) +" errorSelfCode:'59081'","./logs/v1/match.txt");
                                resolve({error:true,msg : '59081'})
                            })
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error) +" errorSelfCode:'59082'","./logs/v1/match.txt");
                        resolve({error:true,msg : '59082'})
                    })
            })
            .catch((error)=>{
                log.insertLog(JSON.stringify(error) +" errorSelfCode:'59080'","./logs/v1/match.txt");
                resolve({error:true,msg : '59080'})
            })
    })
} // no usage
actionJoiner = (matchInfo) => {
    return new Promise((resolve, reject) => {
        console.log('matchInfo',matchInfo)

        querys.findByMultyNamePromise('transactions_history',{user_id:matchInfo.joiner_id,matches_id:matchInfo.id,type:'1'},['*'])
            .then((transactionData)=>{
                console.log('transactionData',transactionData)
                var plusMoney = parseFloat(transactionData[0].money);
                querys.findByMultyNamePromise('user_money',{user_id:matchInfo.joiner_id},['*'])
                    .then(function (userMoneyData) {
                        var userMoney = parseFloat(userMoneyData[0].money);
                        var updateMoney = (plusMoney + userMoney).toFixed(2)
                        querys.update2vPromise('user_money',{
                            where : {user_id : matchInfo.joiner_id},
                            values : {money:updateMoney}
                        })
                            .then(function (result) {
                                Promise.all([
                                    querys.update2vPromise('matches',{
                                        where : {id : matchInfo.id},
                                        values : {
                                            winner_id:0,
                                            creator_start:'1',
                                            joiner_start:'1',
                                            creator_result:'0',
                                            joiner_result:'0'
                                        }
                                    }),
                                    querys.insert2vPromise('transactions_history',{
                                        user_id: matchInfo.joiner_id,
                                        type:'1',
                                        way:'1',
                                        matches_id:matchInfo.id,
                                        money:plusMoney
                                    }),
                                ])
                                    .then(()=>{
                                        resolve({error:false})
                                    })
                                    .catch((error)=>{
                                        log.insertLog(JSON.stringify(error) +" errorSelfCode:'59093'","./logs/v1/match.txt");
                                        resolve({error:true,msg : '59093'})
                                    })
                            })
                            .catch(function (error) {
                                log.insertLog(JSON.stringify(error) +" errorSelfCode:'59091'","./logs/v1/match.txt");
                                resolve({error:true,msg : '59091'})
                            })
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error) +" errorSelfCode:'59092'","./logs/v1/match.txt");
                        resolve({error:true,msg : '59092'})
                    })
            })
            .catch((error)=>{
                log.insertLog(JSON.stringify(error) +" errorSelfCode:'59090'","./logs/v1/match.txt");
                resolve({error:true,msg : '59090'})
            })
    })
} // no usage
