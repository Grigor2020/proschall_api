const express = require('express');
const router = express.Router();
const sha1 = require('sha1');
const log = require("../../logs/log");
const querys = require("../../model/querys/querys");
const notificationQuerys = require("../../model/querys/v1/notification");
const profileQuerys = require("../../model/querys/v1/profile");
const checkUser = require('../../object/auth/auth');
const sendmail = require('../../mailer/sendmail');

router.post('/check-info',validateCheckBody(), async function(req, res) {
    try {
        if(req.signUpError) {
            throw {
                msg:"error params",
                reqBody:req.body,
                errorText:req.signUpErrorText,
                date:new Date()
            };
        }

        const email = await querys.findByEmail(req.body.a_email);
        const username = await querys.findByUsername(req.body.a_username);
        if (!email || !username) {
            throw {
                msg:"error findByEmail OR findByUsername",
                reqBody:req.body,
                date:new Date()
            };
        }

        var result = {email : false, username : false};

        if (email.length > 0) {
            result.email = true
        }

        if (username.length > 0) {
            result.username = true
        }

        if(!result.email && !result.username){
            res.json({error: false,params:{}});
        } else{
            res.json({error: true,params:result});
        }

    } catch (err) {
        log.insertLog(JSON.stringify(err),"./logs/v1/signup.txt")
        res.json({error: true, msg: err.msg, data: []});
        res.end();
    }
});

router.post('/sign-up',validateSignUpBody(), async function(req, res) {
    try {
        if(req.signUpError) {
            throw {
                msg:"error params",
                reqBody:req.body,
                errorText:req.signUpErrorText,
                date:new Date()
            }
        }
        const userFind = await querys.findByEmail(req.body.email)
        if(userFind.length > 0) {
            throw {
                msg:"Find user by this email",
                errorText:req.signUpErrorText,
                date:new Date()
            }
        }

        const signUpParam = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            username: req.body.username,
            email: req.body.email,
            phone: req.body.phone,
            password: sha1(req.body.password),
            token: sha1(sha1(req.body.email) + "" + new Date().getTime()),
        };

        const data = await querys.insert2vPromise('users_db',signUpParam);

        const paramsReg = {
            user_id : data.insertId,
            type:'1',
        }
        await notificationQuerys.addNotification(paramsReg);

        const paramsEmailNotVerivied = {
            user_id : data.insertId,
            type:'15',
        }
        await notificationQuerys.addNotification(paramsEmailNotVerivied);

        await querys.insert2vPromise('user_money', {user_id : data.insertId});

        const EmailRes = await sendmail.sendMailAuth(signUpParam);

        res.json({error: false, msg: "1000",user:{token:signUpParam.token}});
        res.end();
    } catch (err) {
        log.insertLog(JSON.stringify(err),"./logs/v1/signup.txt")
        res.json({error: true, msg: err.msg, data: []});
        res.end();
    }
});

router.post('/login', async function(req, res) {
    try {
        const params = {
            login : req.body.a_login,
            password : sha1(req.body.a_password)
        };

        const userFind = await querys.findForLogin(params);
        if (!userFind || userFind.length === 0) {
            throw {
                msg:"no user",
                date:new Date()
            }
        }

        res.json({error: false, msg: "1000",user:userFind[0]});
        res.end();

    } catch (err) {
        log.insertLog(JSON.stringify(err),"./logs/v1/signup.txt")
        res.json({error: true, msg: err.msg, data: []});
        res.end();
    }
});

router.post('/check-user', function(req, res, next) {
    if(typeof req.body.token != "undefined" && req.body.token.length > 0 ) {
        querys.findByToken(req.body.token)
            .then(function (user) {
                if (user.length === 1) {
                    var userId =user[0].id;
                    Promise.all([
                        notificationQuerys.findUserNotifications(userId),
                        notificationQuerys.findUserUnreadCount(userId),
                        querys.checkUserForMsgShow(userId),
                        // profileQuerys.findUserGameUsernames(userId)
                    ])

                        .then(function ([notes,unread,showMsg]) {

                            // console.log('................user',user)
                            // for(let i = 0; i < notes.length; i++){
                            //     // notes[i].disableNote = false
                            //     if(notes[i].type == '7' && notes[i].user_id == user[0].id){
                            //         // notes[i].disableNote = true
                            //         notes.splice(i,1)
                            //     }
                            // }
                            // console.log('................notes',notes)
                            res.json(
                                {
                                    error: false,
                                    msg: "1000",
                                    user: {
                                        id : ''+user[0].id,
                                        email : user[0].email,
                                        firstName : user[0].firstName,
                                        lastName : user[0].lastName,
                                        username : user[0].username,
                                        token : user[0].token,
                                        bio : user[0].bio,
                                        userMoney : user[0].userMoney,
                                        user_image : user[0].user_image,
                                        email_verify : user[0].email_verify,
                                    },
                                    notes : notes,
                                    unreadCount : unread[0].counts,
                                    showMsg : {
                                        show : showMsg.length === 1 ? true : false,
                                        matches_id : showMsg.length === 1 ? showMsg[0].id : null
                                    }
                                });
                            res.end();
                        })
                        .catch(function (error) {
                            log.insertLog(JSON.stringify(errorData), "./logs/v1/signup.txt")
                            res.json({error: true, msg: "12479", data: []});
                            res.end();
                        })
                } else {
                    var errorData = {
                        msgtext: "no user",
                        reqBody: req.body,
                        date: new Date()
                    }
                    log.insertLog(JSON.stringify(errorData), "./logs/v1/signup.txt")
                    res.json({error: true, msg: "123", data: []});
                    res.end();
                }
            })
            .catch(function (error) {
                var errorData = {
                    promiseError: error.toString(),
                    msgtext: "Find user by this email",
                    errorText: req.signUpErrorText,
                    date: new Date()
                }
                log.insertLog(JSON.stringify(errorData), "./logs/v1/signup.txt");
                res.json({error: true, msg: "448", data: []});
                res.end();
            })
    }else{
        res.json({error: true, msg: "450", data: []});
        log.insertLog(JSON.stringify('error token define'), "./logs/v1/signup.txt");
        res.end();
    }
});

router.post('/get-messages',checkUser.checkValidUser(), function(req, res, next) {
    querys.getUserMessages(req.userId,req.body.matches_id)
        .then(function (data){
            res.json({error: false, data: data});
        })
        .catch(function (error){
            res.json({error: true, msg: "00489", data: []});
            log.insertLog(JSON.stringify(error)+' errSelfCode = 00489, ' , "./logs/v1/signup.txt");
            res.end();
        })
});

router.post('/set-messages-viewed',checkUser.checkValidUser(), function(req, res, next) {
    querys.setUserMessagesViewed(req.userId,req.body.matches_id)
        .then(function (data){
            res.json({error: false});
        })
        .catch(function (error){
            res.json({error: true, msg: "00490", data: []});
            log.insertLog(JSON.stringify(error)+' errSelfCode = 00490, ' , "./logs/v1/signup.txt");
            res.end();
        })
});
router.post('/set-messages',checkUser.checkValidUser(), function(req, res, next) {
    if(typeof req.body.message !== "undefined") {
        querys.insert2vPromise('messages',{user_id:req.userId,matches_id:req.body.matches_id,msg:req.body.message})
            .then(function (data) {
                res.json({error: false});
            })
            .catch(function (error) {
                res.json({error: true, msg: "00491", data: []});
                log.insertLog(JSON.stringify(error) + ' errSelfCode = 00491, ', "./logs/v1/signup.txt");
                res.end();
            })
    }
});


module.exports = router;




function validateBody() {
    return function(req, res, next) {
        req.signUpError = false;
        req.signUpErrorText = [];
        let reqQuery = {
            email : req.body.email,
            password : req.body.password
        }
        let checkEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (typeof reqQuery.email == "undefined" || !checkEmail.test(reqQuery.email.toLowerCase())) {
            req.signUpError = true;
            req.signUpErrorText.push("email");
        }


        if(typeof reqQuery.password == "undefined" || reqQuery.password.length < 6) {
            req.signUpError = true;
            req.signUpErrorText.push("password");
        }else if(reqQuery.password.length == 0){
            req.signUpError = true;
            req.signUpErrorText.push("password");
        }

        next();
    }
}

function validateSignUpBody() {
    // typeof req.body.firstName !== "undefined" ||
    // typeof req.body.lastName !== "undefined" ||
    // typeof req.body.username !== "undefined" ||
    // typeof req.body.email !== "undefined" ||
    // typeof req.body.password !== "undefined"
    return function(req, res, next) {
        req.signUpError = false;
        req.signUpErrorText = [];
        let reqQuery = {
            firstName : req.body.firstName,
            lastName : req.body.lastName,
            username : req.body.username,
            email : req.body.email,
            phone : req.body.phone,
            password : req.body.password
        }
        let checkEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (typeof reqQuery.email == "undefined" || !checkEmail.test(reqQuery.email.toLowerCase())) {
            req.signUpError = true;
            req.signUpErrorText.push("email");
        }
        if(typeof reqQuery.firstName == "undefined") {
            req.signUpError = true;
            req.signUpErrorText.push("firstName");
        }
        if(typeof reqQuery.lastName == "undefined") {
            req.signUpError = true;
            req.signUpErrorText.push("lastName");
        }
        if(typeof reqQuery.phone == "undefined") {
            req.signUpError = true;
            req.signUpErrorText.push("phone");
        }
        if(typeof reqQuery.username == "undefined") {
            req.signUpError = true;
            req.signUpErrorText.push("username");
        }

        if(typeof reqQuery.password == "undefined" || reqQuery.password.length < 6) {
            req.signUpError = true;
            req.signUpErrorText.push("password");
        }else if(reqQuery.password.length < 8){
            req.signUpError = true;
            req.signUpErrorText.push("password ength is < 8");
        }

        next();
    }
}

function validateCheckBody() {
    return function(req, res, next) {
        req.signUpError = false;
        req.signUpErrorText = [];
        let reqQuery = {
            username : req.body.a_username,
            email : req.body.a_email
        }
        let checkEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (typeof reqQuery.email == "undefined" || !checkEmail.test(reqQuery.email.toLowerCase())) {
            req.signUpError = true;
            req.signUpErrorText.push("email");
        }
        if(typeof reqQuery.username == "undefined") {
            req.signUpError = true;
            req.signUpErrorText.push("username");
        }
        next();
    }
}
