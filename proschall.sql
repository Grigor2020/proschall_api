-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Окт 23 2020 г., 21:35
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `proschall`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `log` varchar(100) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admin_login`
--

INSERT INTO `admin_login` (`id`, `log`, `pass`, `token`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '85136c79cbf9fe3fgt9d05d0639c70c265c18d37');

-- --------------------------------------------------------

--
-- Структура таблицы `challenge`
--

CREATE TABLE `challenge` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `mode_id` int(11) DEFAULT NULL,
  `server_id` int(11) DEFAULT NULL,
  `console_id` int(11) DEFAULT NULL,
  `prize` decimal(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `started` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - not started, 1 - started , 2 - ended'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `challenge`
--

INSERT INTO `challenge` (`id`, `user_id`, `game_id`, `mode_id`, `server_id`, `console_id`, `prize`, `description`, `create_date`, `started`) VALUES
(112, 26, 19, 9, NULL, 1, '556.00', 'df fg df gdg df', '2020-07-05 20:39:06', '0'),
(113, 26, 19, 9, NULL, 2, '162.00', 'huybjyby gyjgjgj', '2020-07-05 20:40:47', '0'),
(114, 25, 19, 9, NULL, 1, '65.00', 'iuhub   hg gh hg ', '2020-07-05 20:41:10', '0'),
(115, 25, 19, 10, NULL, 2, '5.00', 'kj hj hj h hj h', '2020-07-05 20:41:29', '0'),
(116, 25, 19, 9, NULL, 1, '546.00', 'h h hhjb ', '2020-07-05 20:41:40', '0'),
(117, 28, 19, 9, NULL, 1, '22.00', 'sfsdfdsfdsfds', '2020-07-09 14:50:14', '0'),
(118, 28, 19, 9, NULL, 1, '22.00', 'sfsdfdsfdsfds', '2020-07-09 14:50:18', '0'),
(122, 25, 19, 9, NULL, 3, '1.24', 'sadsad asd as das das ', '2020-07-11 16:25:20', '0'),
(123, 25, 19, 9, NULL, 3, '2.00', 'sadsad asd as das das ', '2020-07-11 16:25:34', '0'),
(124, 25, 19, 9, NULL, 3, '1.99', 'sadsad asd as das das ', '2020-07-11 17:15:21', '0'),
(125, 25, 19, 9, NULL, 1, '95.15', 'ff f f f f ', '2020-07-13 11:50:06', '0'),
(126, 31, 19, 9, NULL, 1, '1000.00', 'testmatch', '2020-07-15 14:19:08', '0'),
(127, 31, 19, 9, NULL, 1, '1000.00', 'testmatch', '2020-07-15 14:19:30', '0'),
(128, 25, 19, 9, NULL, 1, '122.00', 'kjjk yghj', '2020-07-15 14:19:34', '0'),
(129, 25, 19, 10, NULL, 1, '20.00', 'gh g', '2020-07-15 14:20:53', '0'),
(130, 31, 19, 9, NULL, 1, '1000.00', 'testmatch', '2020-07-15 14:21:11', '0'),
(131, 31, 19, 9, NULL, 1, '1000.00', 'testmatch', '2020-07-15 14:21:25', '0'),
(132, 25, 19, 9, NULL, 1, '20000.00', 'b hghjghjg hjg hj ghjghj', '2020-07-15 18:56:39', '0'),
(133, 26, 19, 9, NULL, 1, '50.00', 'aaaa', '2020-07-15 21:46:08', '0'),
(134, 26, 19, 10, NULL, 1, '65.00', 'hjgjh', '2020-07-15 21:46:37', '0'),
(135, 26, 20, 4, NULL, 3, '34.00', 'asdasd', '2020-07-15 21:46:59', '0'),
(137, 26, 19, 9, NULL, 1, '55.00', 'asdasdsad', '2020-07-15 22:58:05', '0'),
(138, 26, 19, 9, NULL, 1, '34.00', 'sdad', '2020-07-15 22:59:40', '0'),
(139, 26, 19, 9, NULL, 1, '345.00', 'werwer', '2020-07-15 23:04:23', '0'),
(140, 26, 19, 10, NULL, 1, '545.00', 'wtewtwet', '2020-07-15 23:07:52', '0'),
(141, 26, 19, 10, NULL, 1, '234.00', 'werwer', '2020-07-15 23:12:31', '0'),
(142, 26, 19, 9, NULL, 2, '652.00', 'werwe', '2020-07-15 23:13:57', '0'),
(143, 26, 19, 9, NULL, 1, '76.00', 'hjvjhv', '2020-07-15 23:15:31', '0'),
(145, 26, 20, 4, NULL, 3, '75.50', 'fggh', '2020-07-16 17:18:56', '0'),
(147, 25, 19, 9, NULL, 3, '10000.00', 'sadsad asd as das das ', '2020-07-16 22:29:44', '0'),
(157, 25, 19, 9, NULL, 3, '10000.00', 'sadsad asd as das das ', '2020-07-30 17:20:03', '0'),
(158, 25, 19, 9, NULL, 1, '10000.00', 'adas asassadasdasdd sa d asdas d', '2020-07-30 19:15:16', '0'),
(159, 25, 19, 9, NULL, 1, '120000.00', 'asd sa dsa das dsad as', '2020-07-30 21:56:56', '0'),
(160, 25, 19, 9, NULL, 1, '456.00', 'hg h g h gh g h h', '2020-07-30 22:01:00', '0'),
(163, 25, 19, 9, NULL, 1, '20000.00', 'hgv hy g yg', '2020-08-02 00:19:53', '0'),
(166, 25, 19, 9, NULL, 1, '5555.00', 'sad fdsdf ds fdsf ds\nwqewqewqe qwe wq ewqewq', '2020-08-02 20:57:28', '0'),
(167, 25, 19, 9, NULL, 1, '6666.00', 'asd asd qwe \r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nzxc zxc \r\n\r\n\r\n\r\nvvvvv', '2020-08-02 20:59:09', '0'),
(168, 25, 19, 9, NULL, 1, '3000.00', ' ds dg dfgfdg\n gretr re\nt re\nt', '2020-08-03 23:56:49', '0'),
(175, 26, 19, 10, NULL, 1, '5000.00', 'xaxanq brat ?', '2020-08-06 22:46:38', '0'),
(176, 26, 19, 9, NULL, 1, '4444.00', '4444', '2020-08-06 22:57:27', '0'),
(177, 25, 19, 9, NULL, 1, '15000.00', 'vhh vjhgvhg  yjg jgjgj', '2020-08-10 15:51:56', '0'),
(178, 25, 19, 9, NULL, 1, '15000.00', 'mhb h hvh\n bh', '2020-08-12 22:30:14', '0'),
(179, 25, 19, 9, NULL, 1, '1000.00', 's  sss', '2020-08-14 12:35:14', '0'),
(180, 25, 19, 9, NULL, 1, '20000.00', 'Dhd fg g g g', '2020-08-14 14:33:44', '0'),
(181, 25, 19, 9, NULL, 1, '32000.00', 'lkj kj jkg kj ', '2020-08-14 15:16:21', '2'),
(182, 25, 19, 9, NULL, 3, '3333.00', 'ssacsa', '2020-09-01 13:27:37', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `consoles`
--

CREATE TABLE `consoles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `consoles`
--

INSERT INTO `consoles` (`id`, `name`, `image`) VALUES
(1, 'PlayStation', 'ps-console.svg'),
(2, 'PC', 'pc-console.svg'),
(3, 'Xbox', 'xbox-console.svg');

-- --------------------------------------------------------

--
-- Структура таблицы `deposit`
--

CREATE TABLE `deposit` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `deposit`
--

INSERT INTO `deposit` (`id`) VALUES
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57);

-- --------------------------------------------------------

--
-- Структура таблицы `deposit_bitcoin`
--

CREATE TABLE `deposit_bitcoin` (
  `id` int(11) NOT NULL,
  `deposit_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `wallet_address` varchar(200) DEFAULT NULL,
  `type` enum('1','2','3','') DEFAULT NULL COMMENT '1 - c2c 2 - bitcoin 3 - perfect money	',
  `admin_check` enum('0','1') DEFAULT NULL COMMENT '0 - not checked, 1 - checked	',
  `admin_accept` enum('0','1') DEFAULT NULL COMMENT '0 - not accept, 1 - accept',
  `insert_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `deposit_bitcoin`
--

INSERT INTO `deposit_bitcoin` (`id`, `deposit_id`, `user_id`, `amount`, `wallet_address`, `type`, `admin_check`, `admin_accept`, `insert_date`) VALUES
(2, 8, 25, '13.44', '3J98t1WpEZ73CNmQviecrnyiWrnqRhWNLy', '2', '1', '0', '2020-07-08 02:39:29'),
(3, 20, 25, '40.00', 'asd sad sa  dsa ds adas d sa', '2', '1', '0', '2020-07-12 18:28:14'),
(4, 21, 25, '40.00', 'kllllllllllllllllllllllllllll', '2', '1', '1', '2020-07-12 18:30:52'),
(5, 23, 26, '522211120.00', 'asdasdasdasdasd', '2', '1', '1', '2020-07-12 18:43:14');

-- --------------------------------------------------------

--
-- Структура таблицы `deposit_c2c`
--

CREATE TABLE `deposit_c2c` (
  `id` int(11) NOT NULL,
  `deposit_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `card_number` varchar(100) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `type` enum('1','2','3') DEFAULT NULL COMMENT '1 - c2c\r\n2 - bitcoin\r\n3 - perfect money',
  `admin_check` enum('0','1') DEFAULT '0' COMMENT '0 - not checked, 1 - checked',
  `admin_accept` enum('0','1') DEFAULT '0' COMMENT '0 - not accept, 1 - accept',
  `insert_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `deposit_c2c`
--

INSERT INTO `deposit_c2c` (`id`, `deposit_id`, `user_id`, `card_number`, `amount`, `type`, `admin_check`, `admin_accept`, `insert_date`) VALUES
(3, 9, 25, '4569874545698741', '13.44', '1', '1', '0', '2020-07-08 02:39:30'),
(4, 10, 25, '4569874545698741', '18.32', '1', '1', '1', '2020-07-08 02:59:01'),
(5, 11, 25, '4569874545698741', '13.44', '1', '1', '0', '2020-07-08 02:59:01'),
(6, 12, 25, '4569874545698741', '13.44', '1', '1', '0', '2020-07-08 03:04:28'),
(7, 14, 25, '4564747474747474', '100.00', '1', '1', '0', '2020-07-12 18:09:37'),
(8, 15, 25, '4564747474747474', '30.00', '1', '1', '1', '2020-07-12 18:10:26'),
(9, 16, 25, '4564747474747474', '30.00', '1', '1', '0', '2020-07-12 18:10:56'),
(10, 17, 25, '4564747474747474', '55.00', '1', '1', '0', '2020-07-12 18:14:40'),
(11, 18, 25, '4564747474747474', '12.00', '1', '1', '0', '2020-07-12 18:18:17'),
(12, 19, 25, '4564747474747474', '47.00', '1', '1', '0', '2020-07-12 18:19:32'),
(13, 22, 26, '4545445454545454', '522211120.00', '1', '1', '1', '2020-07-12 18:37:16'),
(14, 24, 26, '4545445454545454', '23.00', '1', '1', '1', '2020-07-12 18:51:22'),
(15, 25, 26, '4545445454545454', '56464548.00', '1', '1', '1', '2020-07-12 18:52:49'),
(16, 26, 31, '12345678765432235', '100000.00', '1', '1', '1', '2020-07-15 14:17:16'),
(17, 27, 25, '4564747474747474', '100000.00', '1', '1', '1', '2020-07-15 14:22:36'),
(18, 28, 26, '5152516515315315', '300000.00', '1', '1', '1', '2020-07-15 19:01:06'),
(19, 29, 25, '4564747474747474', '450000.00', '1', '1', '1', '2020-07-15 21:19:08'),
(20, 30, 26, '4545445454545454', '5000.00', '1', '1', '0', '2020-07-15 21:23:24'),
(21, 31, 26, '4545445454545454', '5000.00', '1', '1', '0', '2020-07-15 21:23:25'),
(22, 32, 25, '4564747474747474', '600000.00', '1', '1', '1', '2020-07-15 21:27:56'),
(23, 33, 26, '45454545454545454545', '300000.00', '1', '1', '1', '2020-07-15 21:28:28'),
(24, 34, 25, '4489984984', '140000.00', '1', '1', '1', '2020-07-15 21:29:53'),
(25, 35, 30, '87789789789', '15000.00', '1', '1', '1', '2020-07-15 22:46:58'),
(26, 36, 30, '87789789789', '200000000.00', '1', '1', '1', '2020-07-15 22:50:08'),
(27, 37, 26, '45454545454545454545', '200000.00', '1', '1', '1', '2020-07-16 01:39:52'),
(28, 38, 28, '87789789789', '300000.00', '1', '1', '1', '2020-08-05 22:53:16'),
(29, 39, 29, '1231231231231231', '40000.00', '1', '1', '1', '2020-08-05 22:53:38'),
(30, 40, 25, '4564747474747474', '100.00', '1', '1', '1', '2020-08-06 23:39:37'),
(31, 41, 25, '4564747474747474', '150000.00', '1', '1', '1', '2020-08-06 23:39:45'),
(32, 42, 26, '', '0.00', '1', '1', '0', '2020-08-06 23:47:08'),
(33, 43, 26, '', '0.00', '1', '1', '0', '2020-08-06 23:47:37'),
(34, 44, 25, '4564747474747474', '50000.00', '1', '1', '1', '2020-08-06 23:52:27'),
(35, 45, 26, '', '0.00', '1', '0', '0', '2020-08-06 23:56:31'),
(36, 46, 25, '4564747474747474', '200000.00', '1', '1', '1', '2020-08-06 23:56:47'),
(37, 47, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:01:00'),
(38, 48, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:01:27'),
(39, 49, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:02:14'),
(40, 50, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:05:02'),
(41, 51, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:05:53'),
(42, 52, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:06:09'),
(43, 53, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:09:49'),
(44, 54, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:10:10'),
(45, 55, 26, '', '0.00', '1', '0', '0', '2020-08-07 00:14:11'),
(46, 56, 26, '1212121212121212', '0.00', '1', '0', '0', '2020-08-07 00:30:54');

-- --------------------------------------------------------

--
-- Структура таблицы `deposit_pm`
--

CREATE TABLE `deposit_pm` (
  `id` int(11) NOT NULL,
  `deposit_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `e_voucher` varchar(200) DEFAULT NULL,
  `activation_code` varchar(200) DEFAULT NULL,
  `type` enum('1','2','3') DEFAULT NULL COMMENT '1 - c2c 2 - bitcoin 3 - perfect money	',
  `admin_check` enum('0','1') DEFAULT NULL COMMENT '0 - not checked, 1 - checked	',
  `admin_accept` enum('0','1') DEFAULT NULL COMMENT '	0 - not accept, 1 - accept',
  `insert_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `deposit_pm`
--

INSERT INTO `deposit_pm` (`id`, `deposit_id`, `user_id`, `e_voucher`, `activation_code`, `type`, `admin_check`, `admin_accept`, `insert_date`) VALUES
(2, 7, 25, 'asdasdasdasd', 'qweqweqweqweqwewqeeewq', '3', '1', '0', '2020-07-08 02:39:25'),
(3, 13, 25, 'asdasdasdasd', 'qweqweqweqweqwewqeeewq', '3', '1', '0', '2020-07-08 03:05:14'),
(4, 57, 26, '', '', '3', '0', '0', '2020-08-07 00:41:29');

-- --------------------------------------------------------

--
-- Структура таблицы `email_codes`
--

CREATE TABLE `email_codes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `code` varchar(6) DEFAULT NULL,
  `send_data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `email_codes`
--

INSERT INTO `email_codes` (`id`, `user_id`, `code`, `send_data`) VALUES
(38, 26, '307097', '2020-07-26 01:32:14');

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faq`
--

INSERT INTO `faq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6);

-- --------------------------------------------------------

--
-- Структура таблицы `faq_lang`
--

CREATE TABLE `faq_lang` (
  `id` int(11) NOT NULL,
  `faq_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `question` text DEFAULT NULL,
  `answer` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faq_lang`
--

INSERT INTO `faq_lang` (`id`, `faq_id`, `lang_id`, `question`, `answer`) VALUES
(1, 1, 1, 'how much does it cost to play?', 'you can chose how much you wanna wager on a single challenge by creating one, or join the challenge that you are ok with. there is not a limit for the maximum amount but the minimum amount should be at least 2000t.'),
(2, 1, 2, 'هزینه شرکت در بازی چقدر می باشد؟', 'شما می توانید با ایجاد یک چالش اتخاب کنید که با چه مبلغی می خواهید بازی کنید ، یا به چالش ایجاد شده توسط پلیر های دیگه  بپیوندید.\r\nبرای حداکثر مبلغ محدودیتی وجود ندارد، اما حداقل مبلغ برای انجام بازی 2000T می باشد.'),
(3, 2, 1, 'how do I claim my funds?', 'to claim your funds, first you have to verify your account by sending us a photo of valid id and selfie (photo of debit/credit card if needed. then you can choose the desired option to make the withdrawal. money will be sent to your account in less than 24 hours. however we will always try to make it happen as soon as possible.'),
(4, 2, 2, 'چگونه می توانم جایزه خود را برداشت کنم؟', NULL),
(5, 3, 1, 'what happens if I disagree with a match result?', 'you are free to send a dispute right after the match is finished, then you will have to provide us with some files and documents such as screenshots from the game or a proof video. <br/>it will be reviewed by our professional staff and you will be informed about the outcome up to 24 hours.<br/>so keep recording on when you play, it might be helpful.'),
(6, 3, 2, 'اگر با نتیجه مسابقه موافق نباشم چه اتفاقی می افتد؟', NULL),
(7, 4, 1, 'I’m a newbie: how do I arrange a match?', 'well we advise you to get out there and practice to be as good as you can, but if you are a newbie, you have to minimize your risks so play the challenges with the minimum amount, and make your way up as you are getting pro day by day.'),
(8, 4, 2, 'من تازه ثبت نام کردم ، چگونه می توانم یک چالش ایجاد کنم؟', NULL),
(9, 5, 1, 'will I always be matched with players at the same level as me?', 'the answer is no! i mean where’s the fun in that?<br/> you can check 5 previous games of your opponent so you will have a small knowledge of their level.'),
(10, 5, 2, 'آیا من همیشه با بازیکنانی که در سطح خودم هستند بازی می کنم؟', NULL),
(11, 6, 1, 'where can I purchase play vouchers?', 'to purchase vouchers you can use google to buy the relevant voucher or you can refer the online chat support, they will link you to hottest shops out there!'),
(12, 6, 2, 'چگونه می توانم فرم شارژ را تکمیل کنم؟', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `games`
--

CREATE TABLE `games` (
  `id` int(11) NOT NULL,
  `image` text DEFAULT NULL,
  `url` varchar(250) NOT NULL,
  `show_server` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - dont show, 1 - show',
  `data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `games`
--

INSERT INTO `games` (`id`, `image`, `url`, `show_server`, `data`) VALUES
(19, '<svg width=\"35\" height=\"12\" viewBox=\"0 0 35 12\" fill=\"none\"\r\n                                xmlns=\"http://www.w3.org/2000/svg\">\r\n                                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\"\r\n                                    d=\"M0 -1.33119e-05V11.176H3.52998V7.14068H5.98273L6.93625 4.69789H3.52998V2.43019H7.6759L8.58015 -1.33119e-05H0Z\" />\r\n                                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\"\r\n                                    d=\"M9.65931 0.00465588L9.65302 11.1807H13.1421V0.00465588H9.65931Z\" />\r\n                                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\"\r\n                                    d=\"M26.8345 7.53023L28.1785 2.87688L29.5708 7.53023H26.8345ZM30.0468 0.00476768H26.424L22.407 11.1818H25.7792L26.2133 9.67893H30.1443L30.5988 11.1818H34.0742L30.0468 0.00476768Z\" />\r\n                                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\"\r\n                                    d=\"M15.5515 0.00465588V11.1807H19.0794V7.14535H21.5295L22.4899 4.70256H19.0794V2.43486H23.2269L24.138 0.00465588H15.5515Z\" />\r\n                            </svg>', 'fifa', '0', '2020-06-07 22:25:53'),
(20, '<svg width=\"18\" height=\"18\" viewBox=\"0 0 18 18\" fill=\"none\"\r\n                                xmlns=\"http://www.w3.org/2000/svg\">\r\n                                <path\r\n                                    d=\"M18 0H15.7661L14.8359 0.465082L13.9057 0H9.52734L8.47266 0.527344H5.55757L4.50288 0H0V8.47266L0.527344 9.52734V12.4424L0 13.4971V18H9L9.9302 17.5349L10.8604 18H18V10.8604L17.5349 9.9302L18 9V0ZM4.74609 14.8359L3.16406 13.2539V10.0898L7.91016 14.8359H4.74609ZM14.3086 14.8359H12.1992L10.8322 13.2295L10.5503 12.0845L9.46512 11.6231L9 11.0765L3.16406 4.21875L4.21875 3.16406L9 6.75791L14.8359 11.1445L14.3086 14.8359ZM14.8359 7.91016L10.0898 3.16406H13.2539L14.8359 4.74609V7.91016Z\" />\r\n                            </svg>', 'dota', '1', '2020-06-07 22:27:20'),
(21, '<svg width=\"17\" height=\"18\" viewBox=\"0 0 17 18\" fill=\"none\"\r\n                                xmlns=\"http://www.w3.org/2000/svg\">\r\n                                <path\r\n                                    d=\"M2.42966 0C2.35444 0 2.28072 0.021156 2.21673 0.0610974C2.15275 0.101039 2.10105 0.158187 2.06741 0.22614C2.03377 0.294092 2.01953 0.370163 2.02629 0.44583C2.03304 0.521496 2.06052 0.593767 2.10565 0.654545L3.23968 2.18127V15.8187L1.70064 17.3455C1.65551 17.4062 1.62803 17.4785 1.62128 17.5542C1.61452 17.6298 1.62876 17.7059 1.6624 17.7739C1.69604 17.8418 1.74774 17.899 1.81172 17.9389C1.87571 17.9788 1.94943 18 2.02465 18H13.7699C13.8931 18 14.0093 17.9435 14.0863 17.8466L16.1113 15.8011C16.1587 15.7409 16.1883 15.6683 16.1968 15.5919C16.2052 15.5154 16.1922 15.4381 16.1592 15.3687C16.1262 15.2993 16.0746 15.2408 16.0102 15.1997C15.9457 15.1586 15.8712 15.1367 15.795 15.1364H7.28978V0.409091C7.28978 0.300593 7.24711 0.196539 7.17115 0.11982C7.0952 0.0431005 6.99219 0 6.88477 0H2.42966ZM8.0998 1.63636V2.45455C12.1195 2.45455 15.39 5.75795 15.39 9.81818C15.39 11.5135 14.8145 13.0721 13.8578 14.3182H14.8578C15.734 12.9843 16.2009 11.4189 16.2 9.81818C16.2 5.30673 12.5662 1.63636 8.0998 1.63636ZM8.0998 3.27273V14.3182H12.7939C13.8979 13.1441 14.58 11.5617 14.58 9.81818C14.58 6.20918 11.6728 3.27273 8.0998 3.27273ZM2.42966 3.98455C1.02995 5.373 0.123938 7.2675 0.0117502 9.37309C-0.00876879 9.76034 -0.0024081 10.1486 0.0307856 10.5349C0.202915 12.5313 1.09029 14.3231 2.42966 15.6518V14.4356C1.38085 13.13 0.808811 11.4995 0.80962 9.81818C0.808811 8.13683 1.38085 6.50641 2.42966 5.20077V3.98455ZM2.42966 6.65632C1.89772 7.6235 1.61892 8.71178 1.61964 9.81818C1.61964 10.9645 1.91489 12.042 2.42966 12.98V6.65591V6.65632Z\" />\r\n                            </svg>', 'league-of-legends', '1', '2020-06-07 22:30:21'),
(22, '<svg width=\"39\" height=\"11\" viewBox=\"0 0 39 11\" fill=\"none\"\r\n                                xmlns=\"http://www.w3.org/2000/svg\">\r\n                                <path\r\n                                    d=\"M0 5.5C0 8.52562 0.00740038 11 0.0172676 11C0.0320683 11 2.04251 10.6234 2.28918 10.5735L2.41746 10.5485V8.52812V6.5102H3.18216H3.94687V6.14104C3.94687 5.939 3.95427 5.4127 3.96414 4.96871L3.97894 4.16553H3.19943H2.41746V3.28005V2.39456H3.31784C3.81366 2.39456 4.21822 2.38957 4.21822 2.38209C4.21822 2.35465 4.41556 0.224489 4.4279 0.117233L4.4427 -1.63913e-07H2.22011H0V5.5Z\" />\r\n                                <path\r\n                                    d=\"M36.8981 0.321775L34.9296 0.4864V5.45511V10.4238L35.011 10.4363C35.0826 10.4488 38.8937 10.7281 38.9702 10.7256C38.9924 10.7256 38.9998 10.4612 38.9998 9.49094V8.25624H38.1611H37.3224V7.35828V6.46032H38.0378H38.7531V5.38776V4.3152H38.0131H37.2731V3.60432V2.89343L37.3298 2.88345C37.3594 2.87597 37.611 2.86599 37.8898 2.85602C38.1685 2.84853 38.5311 2.83606 38.6989 2.82608L38.9998 2.81112V1.48164V0.149666L38.9332 0.15216C38.8937 0.154655 37.9786 0.229484 36.8981 0.321775Z\" />\r\n                                <path\r\n                                    d=\"M24.2609 0.665862C24.2856 0.915295 24.5668 4.67425 24.5619 4.67924C24.5594 4.68423 24.1968 3.78378 23.7552 2.67879C23.3161 1.5738 22.9461 0.658379 22.9363 0.640919C22.9215 0.613482 22.2406 0.740692 21.1232 0.980148L21.0171 1.0026V5.29035V9.57811H22.1148H23.2125L23.2002 9.39851C23.1755 9.06427 23.0843 6.30056 23.0941 6.1908C23.1015 6.09353 23.1755 6.26314 23.8761 7.97425C24.3004 9.01439 24.6605 9.87493 24.6704 9.8899C24.6852 9.90486 25.0281 9.88241 25.6103 9.83253C26.116 9.78763 26.5304 9.74772 26.5304 9.74523C26.5328 9.74273 26.5205 7.94432 26.5032 5.74931C26.486 3.5543 26.4712 1.48151 26.4712 1.13978L26.4687 0.523685H25.3587H24.2461L24.2609 0.665862Z\" />\r\n                                <path\r\n                                    d=\"M6.89478 0.675984C5.41964 0.775757 4.60807 1.79594 4.35152 3.86623C4.29972 4.29526 4.27751 5.75195 4.31452 6.26079C4.43786 7.91204 4.87941 8.9846 5.66385 9.53086C6.08567 9.8227 6.71964 9.97984 7.34621 9.94243C8.05911 9.90252 8.54507 9.69798 8.98169 9.25648C9.33937 8.89481 9.53425 8.55558 9.72173 7.97689C9.98321 7.16873 10.1016 5.96147 10.0424 4.70433C9.97581 3.29753 9.76613 2.42202 9.33444 1.75104C8.92249 1.11249 8.27125 0.730859 7.51148 0.683467C7.39061 0.675984 7.26233 0.668501 7.2278 0.663513C7.19326 0.661018 7.04526 0.666007 6.89478 0.675984ZM7.2426 2.63653C7.42268 2.83358 7.49915 3.6193 7.49915 5.30047C7.49915 7.37077 7.37087 8.23131 7.08966 8.04673C6.96879 7.96442 6.87752 7.56283 6.83311 6.89685C6.80104 6.41045 6.80104 4.18301 6.83311 3.71657C6.87998 3.05807 6.96385 2.6939 7.08966 2.6066C7.15873 2.55671 7.17106 2.5592 7.2426 2.63653Z\" />\r\n                                <path\r\n                                    d=\"M10.3607 5.21069V9.75286H11.493H12.6228L12.6376 9.43608C12.6474 9.25898 12.6672 8.68529 12.682 8.15649C12.6968 7.62769 12.7165 7.16375 12.7264 7.12633C12.7387 7.07395 12.8448 7.41069 13.1704 8.5481C13.4072 9.36874 13.607 10.0497 13.6169 10.0597C13.6366 10.0821 15.9505 9.89753 15.9751 9.87259C15.9825 9.86261 15.726 9.04697 15.4053 8.05422L14.8231 6.25332L14.9465 6.14356C15.356 5.78189 15.6939 5.08347 15.7901 4.39254C15.8321 4.09073 15.8049 3.34243 15.7408 3.06556C15.4941 1.98053 14.8404 1.13744 14.0066 0.823156C13.644 0.688462 13.6736 0.690957 11.9592 0.680979L10.3607 0.671002V5.21069ZM12.904 2.82112C13.0791 2.90842 13.2222 3.13291 13.2987 3.4422C13.3505 3.64923 13.3431 4.30524 13.2863 4.51477C13.1803 4.90887 12.9928 5.13835 12.7116 5.22565L12.6548 5.24062V3.99345V2.74379L12.7362 2.76125C12.7782 2.76873 12.8546 2.79617 12.904 2.82112Z\" />\r\n                                <path\r\n                                    d=\"M15.8616 1.85827V3.01813H16.4907H17.1197V6.36054V9.70294H18.3038H19.4878V6.56008V3.41723H20.1292H20.7706V2.25736V1.0975L20.7163 1.08752C20.6645 1.07505 15.9603 0.695912 15.8937 0.698407C15.869 0.698407 15.8616 0.94784 15.8616 1.85827Z\" />\r\n                                <path\r\n                                    d=\"M26.9869 5.47505V10.0023H28.208H29.4291V5.47505V0.947837H28.208H26.9869V5.47505Z\" />\r\n                                <path\r\n                                    d=\"M32.2162 1.26212C30.8792 1.33445 29.7667 1.39432 29.7445 1.39432C29.7025 1.39681 29.7001 1.46915 29.7001 2.56915V3.74148H30.3291H30.9581V7.02153V10.3016H32.1545H33.3509V6.88434V3.46711H34.017H34.683V2.29477C34.683 1.64874 34.6756 1.12493 34.6657 1.12493C34.6534 1.12743 33.5532 1.18729 32.2162 1.26212Z\" />\r\n                            </svg>', 'fortnite', '1', '2020-06-07 22:35:14');

-- --------------------------------------------------------

--
-- Структура таблицы `games_language`
--

CREATE TABLE `games_language` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `games_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `games_language`
--

INSERT INTO `games_language` (`id`, `lang_id`, `games_id`, `name`) VALUES
(11, 1, 19, 'Fifa'),
(12, 2, 19, 'Fifa ir'),
(13, 1, 20, 'Dota'),
(14, 2, 20, 'Dota ir'),
(15, 1, 21, 'League Of Legends'),
(16, 2, 21, 'League Of Legen ir'),
(17, 1, 22, 'Fortnite'),
(18, 2, 22, 'Fortnite ir');

-- --------------------------------------------------------

--
-- Структура таблицы `game_consoles`
--

CREATE TABLE `game_consoles` (
  `id` int(11) NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  `console_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `game_consoles`
--

INSERT INTO `game_consoles` (`id`, `game_id`, `console_id`) VALUES
(1, 19, 1),
(2, 19, 2),
(3, 19, 3),
(4, 20, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `game_mode`
--

CREATE TABLE `game_mode` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `from_admin` enum('0','1') NOT NULL COMMENT '0 - not admin, 1 - admin',
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `game_mode`
--

INSERT INTO `game_mode` (`id`, `game_id`, `user_id`, `from_admin`, `name`) VALUES
(4, 20, NULL, '1', 'mode 2'),
(9, 19, NULL, '1', 'FUT'),
(10, 19, NULL, '1', 'Online game');

-- --------------------------------------------------------

--
-- Структура таблицы `game_server`
--

CREATE TABLE `game_server` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `game_server`
--

INSERT INTO `game_server` (`id`, `game_id`, `name`) VALUES
(1, 21, 'server 3');

-- --------------------------------------------------------

--
-- Структура таблицы `game_usernames`
--

CREATE TABLE `game_usernames` (
  `id` int(11) NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `game_usernames`
--

INSERT INTO `game_usernames` (`id`, `game_id`, `user_id`, `name`) VALUES
(3, 19, 20, 'fifa_2_usname'),
(4, 19, 25, 'mov_fdr_5'),
(17, 20, 25, 'dotaGrig'),
(18, 21, 25, 'LolGrig'),
(19, 20, 26, 'aram'),
(20, 21, 26, 'aram'),
(25, 19, 26, 'aram_fifa'),
(26, 19, 28, 'test1Fifa'),
(27, 19, 31, 'Fifauser'),
(28, 19, 29, '2sadFifa');

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `lang_code` varchar(50) DEFAULT NULL,
  `lang_name` varchar(50) DEFAULT NULL,
  `lang_img` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`id`, `lang_code`, `lang_name`, `lang_img`) VALUES
(1, 'en', 'English', 'uk.svg'),
(2, 'ir', 'Pharsi', 'iran.svg');

-- --------------------------------------------------------

--
-- Структура таблицы `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `winner_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `joiner_id` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `creator_accept` enum('0','1') NOT NULL COMMENT '0 - not accpept, 1 - accept',
  `joiner_accept` enum('0','1') NOT NULL COMMENT '0 - not accpept, 1 - accept',
  `creator_start` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - not started, 1 - started',
  `joiner_start` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - not started, 1 - started',
  `creator_result` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - no action\r\n1 - won\r\n2 - lost',
  `joiner_result` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - no action\r\n1 - won\r\n2 - lost',
  `start_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `matches`
--

INSERT INTO `matches` (`id`, `challenge_id`, `winner_id`, `creator_id`, `joiner_id`, `game_id`, `creator_accept`, `joiner_accept`, `creator_start`, `joiner_start`, `creator_result`, `joiner_result`, `start_date`) VALUES
(100, 181, 25, 25, 26, 19, '1', '1', '1', '1', '1', '2', '2020-08-14 15:16:30'),
(101, 181, 28, 22, 28, 19, '1', '1', '1', '1', '1', '2', '2020-08-14 15:16:30');

-- --------------------------------------------------------

--
-- Структура таблицы `matches_dispute`
--

CREATE TABLE `matches_dispute` (
  `id` int(11) NOT NULL,
  `matches_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `message` varchar(250) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `matches_dispute`
--

INSERT INTO `matches_dispute` (`id`, `matches_id`, `user_id`, `filename`, `message`, `description`, `data`) VALUES
(21, 97, 26, '262267-1597258978100.png', 'Wrong Result 2', 'jhh gh ghghjghjt\r\n h\r\n\r\ngh \r\ngf h\r\nfg', '2020-08-12 23:02:58');

-- --------------------------------------------------------

--
-- Структура таблицы `matches_users`
--

CREATE TABLE `matches_users` (
  `id` int(11) NOT NULL,
  `matches_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_accept` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `matches_id` int(11) NOT NULL,
  `msg` text DEFAULT NULL,
  `view` enum('0','1') NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `messages_archive`
--

CREATE TABLE `messages_archive` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `matches_id` int(11) NOT NULL,
  `msg` text DEFAULT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `s_user_id` int(11) DEFAULT NULL,
  `view` enum('0','1') DEFAULT '0' COMMENT '0 - not viewed, 1 - viewed',
  `challenge_id` int(11) DEFAULT NULL,
  `money` decimal(12,2) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `s_user_id`, `view`, `challenge_id`, `money`, `type`) VALUES
(341, 25, 26, '1', 178, NULL, 4),
(342, 25, 26, '1', 178, NULL, 7),
(343, 26, 25, '1', 178, NULL, 0),
(344, 26, 25, '1', 178, NULL, 7),
(345, 25, NULL, '1', NULL, NULL, 8),
(346, 26, NULL, '1', NULL, NULL, 8),
(347, 26, NULL, '1', NULL, NULL, 11),
(348, 25, 26, '1', 179, NULL, 4),
(349, 26, 25, '1', 179, NULL, 0),
(350, 26, 25, '1', 179, NULL, 7),
(351, 25, 26, '1', 179, NULL, 7),
(352, 26, NULL, '1', NULL, NULL, 8),
(353, 25, NULL, '1', NULL, NULL, 8),
(354, 25, 26, '1', 180, NULL, 4),
(355, 25, 26, '1', 180, NULL, 7),
(356, 26, 25, '1', 180, NULL, 0),
(357, 26, 25, '1', 180, NULL, 7),
(358, 25, NULL, '1', NULL, NULL, 8),
(359, 26, NULL, '1', NULL, NULL, 8),
(360, 25, 26, '1', 181, NULL, 4),
(361, 26, 25, '1', 181, NULL, 0),
(362, 26, 25, '1', 181, NULL, 7),
(363, 25, 26, '1', 181, NULL, 7),
(364, 25, NULL, '1', NULL, NULL, 8),
(365, 26, NULL, '1', NULL, NULL, 8),
(366, 25, NULL, '1', NULL, NULL, 9),
(367, 25, NULL, '1', NULL, '57600.00', 13),
(368, 26, NULL, '1', NULL, NULL, 17),
(369, 33, NULL, '1', NULL, NULL, 1),
(370, 33, NULL, '1', NULL, NULL, 15),
(371, 34, NULL, '0', NULL, NULL, 1),
(372, 34, NULL, '0', NULL, NULL, 15),
(373, 25, NULL, '1', NULL, NULL, 14),
(374, 25, NULL, '1', NULL, NULL, 18),
(375, 35, NULL, '0', NULL, NULL, 1),
(376, 35, NULL, '0', NULL, NULL, 15),
(377, 36, NULL, '0', NULL, NULL, 15),
(378, 36, NULL, '0', NULL, NULL, 1),
(379, 37, NULL, '0', NULL, NULL, 1),
(380, 37, NULL, '0', NULL, NULL, 15),
(381, 38, NULL, '0', NULL, NULL, 1),
(382, 38, NULL, '0', NULL, NULL, 15),
(383, 39, NULL, '0', NULL, NULL, 1),
(384, 39, NULL, '0', NULL, NULL, 15),
(385, 40, NULL, '0', NULL, NULL, 1),
(386, 40, NULL, '0', NULL, NULL, 15);

-- --------------------------------------------------------

--
-- Структура таблицы `tournament`
--

CREATE TABLE `tournament` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `game_mode_id` int(11) DEFAULT NULL,
  `members_count` int(11) DEFAULT NULL,
  `entry` decimal(10,2) DEFAULT NULL,
  `prize` decimal(10,2) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `check_in_start_date` date DEFAULT NULL,
  `check_in_start_time` time DEFAULT NULL,
  `create_data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tournament`
--

INSERT INTO `tournament` (`id`, `name`, `image`, `game_id`, `game_mode_id`, `members_count`, `entry`, `prize`, `start_date`, `start_time`, `check_in_start_date`, `check_in_start_time`, `create_data`) VALUES
(1, 'tr 1', NULL, 19, 10, 16, '5000.00', '72000.00', '2020-10-25', '17:00:00', '2020-10-25', '14:00:00', '2020-10-22 02:16:13'),
(8, 'tr 2', NULL, 19, 9, 32, '1000.00', '28800.00', '2020-10-26', '16:00:00', '2020-10-26', '13:00:00', '2020-10-22 12:48:50');

-- --------------------------------------------------------

--
-- Структура таблицы `tournament_members`
--

CREATE TABLE `tournament_members` (
  `id` int(11) NOT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `join_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `transactions_history`
--

CREATE TABLE `transactions_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` enum('1','2','3') DEFAULT NULL COMMENT '1 - gaming\r\n2 - deposit\r\n3 - withdraw',
  `way` enum('0','1','2') DEFAULT NULL COMMENT '0 - out\r\n1 - in\r\n2 - freeze',
  `matches_id` int(11) DEFAULT NULL,
  `deposite_id` int(11) DEFAULT NULL,
  `withdraw_id` int(11) DEFAULT NULL,
  `money` decimal(12,2) DEFAULT NULL,
  `transaction_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transactions_history`
--

INSERT INTO `transactions_history` (`id`, `user_id`, `type`, `way`, `matches_id`, `deposite_id`, `withdraw_id`, `money`, `transaction_date`) VALUES
(62, 26, '1', '2', 93, NULL, NULL, '5000.00', '2020-08-06 22:56:13'),
(63, 26, '1', '0', 94, NULL, NULL, '4444.00', '2020-08-06 22:59:40'),
(64, 25, '1', '0', 94, NULL, NULL, '4444.00', '2020-08-06 23:03:27'),
(65, 26, '1', '1', 94, NULL, NULL, '7999.20', '2020-08-06 23:06:41'),
(66, 0, '1', '1', 94, NULL, NULL, '888.80', '2020-08-06 23:06:41'),
(67, 0, NULL, NULL, 93, NULL, NULL, '5000.00', '2020-08-06 23:15:40'),
(68, 0, NULL, NULL, 93, NULL, NULL, '7000.00', '2020-08-06 23:15:40'),
(69, 25, '3', '0', NULL, NULL, 5, '10000.00', '2020-08-06 23:41:43'),
(70, 25, '2', '1', NULL, 34, NULL, '50000.00', '2020-08-06 23:52:27'),
(71, 26, '2', '1', NULL, 35, NULL, '0.00', '2020-08-06 23:56:31'),
(72, 25, '2', '1', NULL, 36, NULL, '200000.00', '2020-08-06 23:56:47'),
(73, 25, '3', '2', NULL, NULL, 6, '1050.00', '2020-08-06 23:57:42'),
(74, 26, '2', '1', NULL, 37, NULL, '0.00', '2020-08-07 00:01:00'),
(75, 26, '2', '1', NULL, 38, NULL, '0.00', '2020-08-07 00:01:27'),
(76, 26, '2', '1', NULL, 39, NULL, '0.00', '2020-08-07 00:02:14'),
(77, 26, '2', '1', NULL, 40, NULL, '0.00', '2020-08-07 00:05:02'),
(78, 26, '2', '1', NULL, 41, NULL, '0.00', '2020-08-07 00:05:53'),
(79, 26, '2', '1', NULL, 42, NULL, '0.00', '2020-08-07 00:06:09'),
(80, 26, '2', '1', NULL, 43, NULL, '0.00', '2020-08-07 00:09:49'),
(81, 26, '2', '1', NULL, 44, NULL, '0.00', '2020-08-07 00:10:10'),
(82, 26, '2', '1', NULL, 45, NULL, '0.00', '2020-08-07 00:14:11'),
(83, 26, '2', '1', NULL, 46, NULL, '0.00', '2020-08-07 00:30:54'),
(84, 25, '1', '2', 96, NULL, NULL, '15000.00', '2020-08-10 15:52:14'),
(85, 26, '1', '2', 96, NULL, NULL, '15000.00', '2020-08-10 21:14:48'),
(86, 25, '1', '2', 97, NULL, NULL, '15000.00', '2020-08-12 22:30:34'),
(87, 26, '1', '2', 97, NULL, NULL, '15000.00', '2020-08-12 22:38:13'),
(88, 25, '1', '2', 98, NULL, NULL, '1000.00', '2020-08-14 12:35:25'),
(89, 26, '1', '2', 98, NULL, NULL, '1000.00', '2020-08-14 12:35:38'),
(90, 25, '1', '2', 99, NULL, NULL, '20000.00', '2020-08-14 14:34:20'),
(91, 26, '1', '2', 99, NULL, NULL, '20000.00', '2020-08-14 14:34:39'),
(92, 25, '1', '0', 100, NULL, NULL, '32000.00', '2020-08-14 15:16:36'),
(93, 26, '1', '0', 100, NULL, NULL, '32000.00', '2020-08-14 15:16:41'),
(94, 25, '1', '1', 100, NULL, NULL, '57600.00', '2020-08-14 15:19:38'),
(95, 0, '1', '1', 100, NULL, NULL, '6400.00', '2020-08-14 15:19:38');

-- --------------------------------------------------------

--
-- Структура таблицы `users_db`
--

CREATE TABLE `users_db` (
  `id` int(11) NOT NULL,
  `firstName` varchar(250) DEFAULT NULL,
  `lastName` varchar(250) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `password` varchar(250) NOT NULL,
  `token` varchar(250) NOT NULL,
  `e_verify` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - unverifyed\r\n1-verifyed',
  `data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_db`
--

INSERT INTO `users_db` (`id`, `firstName`, `lastName`, `username`, `email`, `phone`, `image`, `bio`, `password`, `token`, `e_verify`, `data`) VALUES
(19, 'firstname 1', 'lastname 1', 'firstname_1', 'firstname1@gmail.com', NULL, NULL, NULL, '141f87be1330a105a87923f4ee6383bd7de46541', '00d35eaff9c5b45c2c3a51e7902c1ee34437ac52', '0', '2020-06-23 21:55:14'),
(20, 'firstname 2', 'lastname 2', 'firstname_2', 'firstname2@gmail.com', NULL, NULL, NULL, '141f87be1330a105a87923f4ee6383bd7de46541', '30406f624cd2e7f6a4449c14ad83fb2431971c94', '0', '2020-06-23 21:56:13'),
(25, 'Grigor', 'Sadoyan', 'GrigUsername', 'sadoyangrigor@gmail.com', NULL, NULL, 'aLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'a614cf3d5c836171fda6b603ff8825900c677cf6', '1', '2020-07-04 16:30:41'),
(26, 'Aram', 'Kirakosyan', 'Aram', 'ama12.06.92@gmail.com', NULL, NULL, NULL, '141f87be1330a105a87923f4ee6383bd7de46541', '5ef8e157ba6dabd628e560e80639bd3a94e81f24', '0', '2020-07-04 16:36:03'),
(28, 'aGrigor', 'Sadoyan', '1sadUsName', 'asadoyangrigor@gmail.com', NULL, NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', '6e42176175a4f818c93fa96f8366773d642e32ec', '0', '2020-07-09 14:44:51'),
(29, 'bGrigor', 'Sadoyan', '2sadUsName', 'bsadoyangrigor@gmail.com', NULL, NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'fcfb0a9bbab3f38535860c135d05cf66b2830c51', '0', '2020-07-09 14:45:01'),
(30, 'cGrigor', 'Sadoyan', '3sadUsName', 'csadoyangrigor@gmail.com', NULL, NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'e3d472545d958d56c7e692069b1947d6b70a8077', '0', '2020-07-09 14:45:18'),
(31, 'Sup', 'Cup', 'Supacupa', 'Daniel.hambik@gmail.com', NULL, NULL, NULL, 'ae4b942ac9ab83e7082ec83ed867bd013067a6aa', '90c42edb6485c80ad6b170a2ae64e7827bf4124d', '0', '2020-07-15 14:13:00'),
(32, 'Grigor', 'Sadoyan', '4sadUsName', 'dsadoyangrigor@gmail.com', NULL, NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'fe01ca573e71812a2afe9504dbd902e4f8c27631', '0', '2020-07-17 17:58:53'),
(34, 'sads', 's', 'sss', 'karine@gmail.com', '12321312', NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'e195f9585037411b251559edf6e502cbddc0edb3', '0', '2020-09-01 12:45:46'),
(37, 'Grigor', 'Sadoyan', 'GrigUsername', 'mosarmcinema@gmail.com', '12321312', NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'db4d010012ca0fcfce5b676865ae023977fe7de1', '0', '2020-09-15 01:04:21'),
(38, 'Grigor', 'Sadoyan', 'aGrigUsername', 'osadoyangrigor@gmail.com', NULL, NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', '2352f3c9bf4ec30b3ead79fd6305e74f7d06079c', '0', '2020-09-15 01:27:17'),
(39, 'Grigor', 'Sadoyan', 'GrisgUsername', 'sdadoyangrigor@gmail.com', NULL, NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'dac82f4675dcaab9b615b24c66f80131a32cdd85', '0', '2020-09-15 01:27:56'),
(40, 'Grigor', 'Sadoyan', 'Gr77igUsername', 'sadoy8angrigor@gmail.com', NULL, NULL, NULL, '00ea1da4192a2030f9ae023de3b3143ed647bbab', '2fbaad7d44429e22f5ceba859c826d63894f16b3', '0', '2020-09-15 01:28:50');

-- --------------------------------------------------------

--
-- Структура таблицы `user_money`
--

CREATE TABLE `user_money` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_money`
--

INSERT INTO `user_money` (`id`, `user_id`, `money`) VALUES
(10, 19, '0.00'),
(11, 20, '0.00'),
(12, 25, '759206.00'),
(13, 26, '115555.20'),
(14, 28, '290000.00'),
(15, 29, '30000.00'),
(16, 30, '200015000.00'),
(17, 31, '0.00'),
(18, 32, '0.00'),
(20, 34, '0.00'),
(23, 37, '0.00'),
(24, 38, '0.00'),
(25, 39, '0.00'),
(26, 40, '0.00');

-- --------------------------------------------------------

--
-- Структура таблицы `verify_tokens`
--

CREATE TABLE `verify_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `v_token` varchar(200) DEFAULT NULL,
  `verify_data` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `verify_tokens`
--

INSERT INTO `verify_tokens` (`id`, `user_id`, `v_token`, `verify_data`) VALUES
(8, 26, '89af1e64c29b4d7e5f592b7b99e79ff6912bb232-741051d63ae55b682c06e7107e47584037768e72', '2020-08-06 23:43:19');

-- --------------------------------------------------------

--
-- Структура таблицы `withdraws`
--

CREATE TABLE `withdraws` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bank_name` varchar(250) NOT NULL,
  `bank_account_number` varchar(250) NOT NULL,
  `card_number` varchar(250) NOT NULL,
  `amount` varchar(250) NOT NULL,
  `accept` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - no action\r\n1 - no accept\r\n2 - accept',
  `insert_data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `withdraws`
--

INSERT INTO `withdraws` (`id`, `user_id`, `bank_name`, `bank_account_number`, `card_number`, `amount`, `accept`, `insert_data`) VALUES
(2, 25, 'Acba Credit Bank', '123456789456', '458796325896', '100000', '0', '2020-07-24 02:22:43'),
(3, 25, 'Ineco Bank', '456965414563', '458796325896', '100000', '0', '2020-07-25 21:52:49'),
(4, 25, 'Acba Credit Bank', '123456789456', '458796325896', '100000', '2', '2020-07-26 00:10:29'),
(5, 25, 'Acba Credit Bank', '123456789456', '458796325896', '10000', '2', '2020-08-06 23:41:43'),
(6, 25, 'Acba Credit Bank', '123456789456', '458796325896', '1050', '0', '2020-08-06 23:57:42');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `challenge`
--
ALTER TABLE `challenge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `console_id` (`console_id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `mode_id` (`mode_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `server_id` (`server_id`);

--
-- Индексы таблицы `consoles`
--
ALTER TABLE `consoles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `deposit_bitcoin`
--
ALTER TABLE `deposit_bitcoin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `deposit_c2c`
--
ALTER TABLE `deposit_c2c`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `deposit_pm`
--
ALTER TABLE `deposit_pm`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `email_codes`
--
ALTER TABLE `email_codes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `faq_lang`
--
ALTER TABLE `faq_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faq_id` (`faq_id`);

--
-- Индексы таблицы `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `games_language`
--
ALTER TABLE `games_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `games_id` (`games_id`);

--
-- Индексы таблицы `game_consoles`
--
ALTER TABLE `game_consoles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `game_mode`
--
ALTER TABLE `game_mode`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `game_server`
--
ALTER TABLE `game_server`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `game_usernames`
--
ALTER TABLE `game_usernames`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `game_id_2` (`game_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `matches_dispute`
--
ALTER TABLE `matches_dispute`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `matches_users`
--
ALTER TABLE `matches_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matches_id` (`matches_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages_archive`
--
ALTER TABLE `messages_archive`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tournament`
--
ALTER TABLE `tournament`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tournament_members`
--
ALTER TABLE `tournament_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tournament_id` (`tournament_id`);

--
-- Индексы таблицы `transactions_history`
--
ALTER TABLE `transactions_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users_db`
--
ALTER TABLE `users_db`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_money`
--
ALTER TABLE `user_money`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `verify_tokens`
--
ALTER TABLE `verify_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `withdraws`
--
ALTER TABLE `withdraws`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `challenge`
--
ALTER TABLE `challenge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT для таблицы `consoles`
--
ALTER TABLE `consoles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT для таблицы `deposit_bitcoin`
--
ALTER TABLE `deposit_bitcoin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `deposit_c2c`
--
ALTER TABLE `deposit_c2c`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT для таблицы `deposit_pm`
--
ALTER TABLE `deposit_pm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `email_codes`
--
ALTER TABLE `email_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `faq_lang`
--
ALTER TABLE `faq_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `games_language`
--
ALTER TABLE `games_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `game_consoles`
--
ALTER TABLE `game_consoles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `game_mode`
--
ALTER TABLE `game_mode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `game_server`
--
ALTER TABLE `game_server`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `game_usernames`
--
ALTER TABLE `game_usernames`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT для таблицы `matches_dispute`
--
ALTER TABLE `matches_dispute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `matches_users`
--
ALTER TABLE `matches_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT для таблицы `messages_archive`
--
ALTER TABLE `messages_archive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;

--
-- AUTO_INCREMENT для таблицы `tournament`
--
ALTER TABLE `tournament`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `tournament_members`
--
ALTER TABLE `tournament_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `transactions_history`
--
ALTER TABLE `transactions_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT для таблицы `users_db`
--
ALTER TABLE `users_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `user_money`
--
ALTER TABLE `user_money`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `verify_tokens`
--
ALTER TABLE `verify_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `withdraws`
--
ALTER TABLE `withdraws`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `challenge`
--
ALTER TABLE `challenge`
  ADD CONSTRAINT `challenge_ibfk_1` FOREIGN KEY (`console_id`) REFERENCES `consoles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `challenge_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `challenge_ibfk_3` FOREIGN KEY (`mode_id`) REFERENCES `game_mode` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `challenge_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `users_db` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `deposit_bitcoin`
--
ALTER TABLE `deposit_bitcoin`
  ADD CONSTRAINT `deposit_bitcoin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users_db` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `deposit_c2c`
--
ALTER TABLE `deposit_c2c`
  ADD CONSTRAINT `deposit_c2c_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users_db` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `faq_lang`
--
ALTER TABLE `faq_lang`
  ADD CONSTRAINT `faq_lang_ibfk_1` FOREIGN KEY (`faq_id`) REFERENCES `faq` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `games_language`
--
ALTER TABLE `games_language`
  ADD CONSTRAINT `games_language_ibfk_1` FOREIGN KEY (`games_id`) REFERENCES `games` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `game_usernames`
--
ALTER TABLE `game_usernames`
  ADD CONSTRAINT `game_usernames_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `game_usernames_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users_db` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `matches_users`
--
ALTER TABLE `matches_users`
  ADD CONSTRAINT `matches_users_ibfk_1` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matches_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users_db` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tournament_members`
--
ALTER TABLE `tournament_members`
  ADD CONSTRAINT `tournament_members_ibfk_1` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_money`
--
ALTER TABLE `user_money`
  ADD CONSTRAINT `user_money_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users_db` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
