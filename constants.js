const matchStatuses = {
    joined: 0,
    accepted : 1,
    started: 2,
    ended: 3,
    canceled: 4
}

module.exports.matchStatuses = matchStatuses;