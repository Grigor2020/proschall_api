const mailjet = require ('node-mailjet').connect("34389fc6d0fab6262a98be004049b871", "d568c392cda79d1427e1d11657ddc6ee")
let statics = require('../static');


var sendMailAuth = (params) => {
    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "no-reply@prizchall.com",
                        "Name": "Welcome To Prizchall.com"
                    },
                    "To": [{
                        "Email": params.email,
                        "Name": params.firstName + " " + params.lastName
                    }],
                    "Subject": "Prizchall.com",
                    "TextPart": "",
                    "HTMLPart": welcomeMessage(params)
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}

var sendMembersCountToAdmin = (tid,count) => {
    // console.log('count',count)
    // console.log('tid',tid)
    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "no-reply@prizchall.com",
                        "Name": "prizchall"
                    },
                    "To": [{
                        "Email": 'sadoyangrigor@gmail.com',
                        "Name": 'Prizchall  - #'+ tid +' tournament members count'
                    }],
                    "Subject": "Prizchall.com",
                    "TextPart": "",
                    "HTMLPart": sendMembersCountToAdminTemplate(tid,count)
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}


var sendMailVerification = (params) => {
    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "no-reply@prizchall.com",
                        "Name": "Welcome To Prizchall.com"
                    },
                    "To": [{
                        "Email": params.email,
                        "Name": params.email
                    }],
                    "Subject": "Prizchall.com",
                    "TextPart": "",
                    "HTMLPart": emailVerificationTemplate(params)
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}

var welcomeMessage = function (params) {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="width=device-width, initial-scale=1" name="viewport">
<title>Contact Message</title>
<style type="text/css">
*{-webkit-box-sizing: border-box;box-sizing: border-box;}
img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
a{text-decoration:none;border:0;outline:none;color:#bbb}
a img{border:none}
td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
td{text-align:center}
body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
table{border-collapse:collapse!important}
</style>
<style media="only screen and (max-width: 480px)" type="text/css">
/* Mobile styles */
@media only screen and (max-width: 480px) {
table[class="w320"] {
width: 320px !important;
}
}
</style>
<style type="text/css"></style>
</head>
<body bgcolor="#ffffff" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
<table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tbody>
<tr>
<td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
<table cellpadding="0" cellspacing="0" class="w320" width="580" style="border: 1px solid #D1D1D1;margin: 0 auto;background-image: url(http://prizchall.com/images/email-bg.png);background-repeat: no-repeat;background-position: center;background-size: 100% 100%">
<tbody>
<tr>
<td align="center" class="" valign="top" style="">
<img src="http://prizchall.com/images/logo-orange.png" alt="" width="200" style="margin-top: 40px">
</td>
</tr>
<tr>
<td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
<h1 style="margin-top: 35px;margin-bottom: 35px;font-size: 26px;line-height: 44px;text-align: center;text-transform: uppercase;color: #000000;">Hi ${params.firstName + " " + params.lastName}
</td>
</tr>
<tr>
<td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
<h1 style="margin-top: 35px;margin-bottom: 35px;font-size: 26px;line-height: 44px;text-align: center;text-transform: uppercase;color: #000000;">You have successfully registered in
<a href="http://prizchall.com" target="_blank" style="color: #023466;text-decoration: underline">prizchall.com</a></h1>
</td>
</tr>
<tr>
<td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
<p style="font-weight: normal;font-size: 24px;line-height: 34px;text-align: center;color: #7C7C7C;max-width: 369px;margin-top: 117px;margin-bottom: 30px;display: block;margin-left: auto;margin-right: auto">
If you did not send this request, please ignore this letter!
</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>`
}

var emailVerificationTemplate = function (params) {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
                    <meta content="width=device-width, initial-scale=1" name="viewport">
                    <title>Contact Message</title>
                    <style type="text/css">
                        *{-webkit-box-sizing: border-box;box-sizing: border-box;}
                        img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
                        a{text-decoration:none;border:0;outline:none;color:#bbb}
                        a img{border:none}
                        td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
                        td{text-align:center}
                        body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
                        table{border-collapse:collapse!important}
                    </style>
                    <style media="only screen and (max-width: 480px)" type="text/css">
                        /* Mobile styles */
                        @media only screen and (max-width: 480px) {
                            table[class="w320"] {
                                width: 320px !important;
                            }
                        }
                    </style>
                    <style type="text/css"></style>
                </head>
                <body bgcolor="#ffffff" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
                    <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                                    <table cellpadding="0" cellspacing="0" class="w320" width="580" style="border: 1px solid #D1D1D1;margin: 0 auto;background-image: url(http://prizchall.com/images/email-bg.png);background-repeat: no-repeat;background-position: center;background-size: 100% 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="" valign="top" style="">
                                                    <img src="http://prizchall.com/images/logo-orange.png" alt="" width="237" style="margin-top: 40px">
                                                </td>
                                            </tr>
                                             <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="font-weight: normal;font-size: 16px;line-height: 24px;color: #7C7C7C;text-align: left">
                                                        To get access to all the features of your personal account, please confirm your e-mail by clicking on the button below, by this you confirm your consent to receive mailings:
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <a href="${params.backUrl}" target="_blank" style="display:inline-block;width: 100%;background: #FFB608;border: 1px solid #866601;padding: 28px;text-decoration: none;font-weight: normal;font-size: 14px;line-height: 16px;letter-spacing: 0.3em;text-transform: uppercase;color: #161616;margin-bottom: 38px">Confirm MY email address</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>`
}

var sendMembersCountToAdminTemplate = function (tid,count) {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
                    <meta content="width=device-width, initial-scale=1" name="viewport">
                    <title>Contact Message</title>
                    <style type="text/css">
                        *{-webkit-box-sizing: border-box;box-sizing: border-box;}
                        img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
                        a{text-decoration:none;border:0;outline:none;color:#bbb}
                        a img{border:none}
                        td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
                        td{text-align:center}
                        body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
                        table{border-collapse:collapse!important}
                    </style>
                    <style media="only screen and (max-width: 480px)" type="text/css">
                        /* Mobile styles */
                        @media only screen and (max-width: 480px) {
                            table[class="w320"] {
                                width: 320px !important;
                            }
                        }
                    </style>
                    <style type="text/css"></style>
                </head>
                <body bgcolor="#ffffff" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
                    <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                                    <table cellpadding="0" cellspacing="0" class="w320" width="580" style="border: 1px solid #D1D1D1;margin: 0 auto;background-image: url(http://prizchall.com/images/email-bg.png);background-repeat: no-repeat;background-position: center;background-size: 100% 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="" valign="top" style="">
                                                    <img src="http://prizchall.com/images/logo-orange.png" alt="" width="237" style="margin-top: 40px">
                                                </td>
                                            </tr>
                                             <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="font-weight: normal;font-size: 16px;line-height: 24px;color: #7C7C7C;text-align: left">
                                                        Prizchall  - #${tid} tournament members count id ${count}
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>`
}


module.exports.sendMailAuth = sendMailAuth;
module.exports.sendMailVerification = sendMailVerification;
module.exports.sendMembersCountToAdmin = sendMembersCountToAdmin;