const statics = require("../static")
const request = require('request');
const axios = require("axios");

const schedulerService = {
    async create(matchId, callbackDateTime, callbackUrl) {
        try {
            const url = statics.SCHEDULER_URL + '/scheduler-service';
            const data = {
                data: { matchId },
                repeat: false,
                date: callbackDateTime,
                callbackUrl: callbackUrl,
                timeZone: statics.TIMEZONE,
            }
            return axios.post(
                url,
                data,
                {
                    headers: {
                        authorization: statics.SCHEDULER_AUTH
                    },
                }
            )
        } catch (err) {
            return err
        }
    },

    async destroy(scheduleId) {
        try {
            const url =  statics.SCHEDULER_URL + '/scheduler-service/' + scheduleId;
            return axios.delete(
                url,
                {
                    headers: {
                        authorization: statics.SCHEDULER_AUTH
                    },
                }
            )
        } catch (err) {
            return err
        }
    },

    get(scheduleId) {
        return axios.get(
            statics.SCHEDULER_URL + '/scheduler-service/' + scheduleId,
            {
                headers: {
                    authorization: statics.SCHEDULER_AUTH
                }
            }

        )
    },

    async healthCheck() {
        try {
            return await axios.get(
                statics.SCHEDULER_URL + '/health-check',
                {
                    headers: {
                        authorization: statics.SCHEDULER_AUTH
                    }
                }

            )
        } catch (err) {
            return err
        }
    }
}

module.exports.schedulerService = schedulerService;