var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth/auth');
var gamesRouter = require('./routes/games/games');
var challengeRouter = require('./routes/challenge/challenge');
var matchRouter = require('./routes/match/match');
var languageRouter = require('./routes/language/language');
var notificationRouter = require('./routes/notification/notification');
var profileRouter = require('./routes/profile/profile');
var forgotRouter = require('./routes/forgot/forgot');
var faqRouter = require('./routes/faq/faq');
var tournamentRouter = require('./routes/tournament/tournament');
var schedulerCallbackRouter = require('./routes/scheduler-callback/router');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const mongoose = require('mongoose')
mongoose.connect('mongodb://127.0.0.1:27017/prizchall-scheduler', { useNewUrlParser: true, useUnifiedTopology: true });

app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Headers", "x-access-token,void,authorization,guest");
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT'); // OPTIONS, PATCH,
    res.setHeader('Cache-Control', 'no-cache'); // OPTIONS, PATCH,
    res.setHeader('X-Frame-Options', 'deny'); // OPTIONS, PATCH,
    res.removeHeader('Server');
    res.removeHeader('X-Powered-By');
    next()
})

app.use(function (req, res, next) {
    if (req.headers['authorization'] == "5b99975c1a84dc272e7beb8ac35f02e7a861615c") {
        next();
    } else {
        res.json({error: true, msg: "Authorization wrong", data: null});
        res.end();
    }
});

app.use('/v1/', indexRouter);
app.use('/v1/users', usersRouter);
app.use('/v1/auth', authRouter);
app.use('/v1/games', gamesRouter);
app.use('/v1/challenge', challengeRouter);
app.use('/v1/match', matchRouter);
app.use('/v1/language', languageRouter);
app.use('/v1/notification', notificationRouter);
app.use('/v1/profile', profileRouter);
app.use('/v1/forgot', forgotRouter);
app.use('/v1/faq', faqRouter);
app.use('/v1/tournament', tournamentRouter);
app.use('/v1/scheduler-callback', schedulerCallbackRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
